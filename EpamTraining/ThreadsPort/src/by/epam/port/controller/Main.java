package by.epam.port.controller;

import by.epam.port.model.businesslogic.PortManager;
import by.epam.port.model.businesslogic.ShipThread;



public class Main {

    public static void main(String[] args) {

        PortManager portManager = PortManager.createPort(); // создание порта

        Thread ship1 = new Thread(new ShipThread("Moskow", 5, portManager, 5));
        Thread ship2 = new Thread(new ShipThread("Minsk", 5, portManager, 0));
        Thread ship3 = new Thread(new ShipThread("Kiev", 5, portManager, 3));
        Thread ship4 = new Thread(new ShipThread("Viena", 5, portManager, 5));
        Thread ship5 = new Thread(new ShipThread("Singapore", 5, portManager, 0));
        Thread ship6 = new Thread(new ShipThread("Lieba", 5, portManager, 3));
        Thread ship7 = new Thread(new ShipThread("Moskow1", 5, portManager, 5));
        Thread ship8 = new Thread(new ShipThread("Minsk1", 5, portManager, 0));
        Thread ship9 = new Thread(new ShipThread("Kiev1", 5, portManager, 3));
        Thread ship10 = new Thread(new ShipThread("Moskow2", 5, portManager, 5));
        Thread ship11 = new Thread(new ShipThread("Minsk2", 5, portManager, 0));
        Thread ship12 = new Thread(new ShipThread("Kiev2", 5, portManager, 3));
        Thread ship13 = new Thread(new ShipThread("Moskow3", 5, portManager, 5));
        Thread ship14 = new Thread(new ShipThread("Minsk3", 5, portManager, 0));
        Thread ship15 = new Thread(new ShipThread("Kiev3", 5, portManager, 3));
        Thread ship16 = new Thread(new ShipThread("Moskow4", 5, portManager, 5));
        Thread ship18 = new Thread(new ShipThread("Minsk4", 5, portManager, 0));
        Thread ship17 = new Thread(new ShipThread("Kiev4", 5, portManager, 3));
        ship1.start();
        ship2.start();
        ship3.start();
        ship4.start();
        ship5.start();
        ship6.start();
        ship7.start();
        ship8.start();
        ship9.start();
        ship10.start();
        ship11.start();
        ship12.start();
        ship13.start();
        ship14.start();
        ship15.start();
        ship16.start();
        ship17.start();
        ship18.start();


    }

}
