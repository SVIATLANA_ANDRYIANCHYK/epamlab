package by.epam.port.exception;

public class ResourceException extends Exception {

    private static final long serialVersionUID = -6212184773700761249L;

    public ResourceException() {
        super();
    }

    public ResourceException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ResourceException(String arg0) {
        super(arg0);
    }

    public ResourceException(Throwable arg0) {
        super(arg0);
    }

}
