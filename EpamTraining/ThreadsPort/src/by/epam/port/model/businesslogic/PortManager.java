package by.epam.port.model.businesslogic;


import by.epam.port.exception.ResourceException;
import org.apache.log4j.Logger;

import javax.sound.sampled.Port;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PortManager {
    public static final Logger LOG = Logger.getLogger(PortManager.class);
    private static final int BERTH_NUMBER = 10;
    private Queue<BerthManager> berthManagerList = new LinkedList<BerthManager>();
    private static PortManager portManager;
    private static volatile boolean portCreated = false;
    private static Lock lock = new ReentrantLock();


    private PortManager() {
        LOG.info("Create 'PortManager' object.");
    }

    public static PortManager createPort() {
        if(!portCreated){
            lock.lock();
            try{
                if(!portCreated){
                    portManager = new PortManager();
                    portManager.berthManagerList.addAll(generateBerth());
                    portCreated = true;
                }
            }

            finally {
                lock.unlock();
            }
        }
        return portManager;
    }


    public void releaseResource(BerthManager berthManager) {
        berthManagerList.add(berthManager);
    }

    public BerthManager findBerth() throws ResourceException {

        while (true) {
            try {
                lock.lock();
                //semaphore.acquire();
                BerthManager berthManager = berthManagerList.poll();
                if (berthManager != null) {
                    System.out.println("Причал №" + berthManager.getNumber()
                            + " зарезервирован.");
                    LOG.info("Причал №" + berthManager.getNumber()
                            + " зарезервирован.");
                }
                return berthManager;

            } finally {
                lock.unlock();
            }

        }

    }

    private static Collection<BerthManager> generateBerth() {
        //generates list of berths
        LinkedList<BerthManager> list = new LinkedList<BerthManager>();
        for (int i = 1; i <= BERTH_NUMBER; i++) {
            list.add(new BerthManager(i));
        }
        return list;
    }
}
