package by.epam.port.model.businesslogic;

import by.epam.port.model.domain.Berth;
import by.epam.port.model.domain.Container;

import java.util.*;


public class BerthManager {

    private static final int BERTH_CAPACITY = 30;
    private Queue<Container> containers = null;
    private Berth berth;


    public BerthManager(int number) {

        berth = new Berth(number);
        containers = new ArrayDeque<Container>(BERTH_CAPACITY);
        List<Container> containerList = generateContainers();
        containers.addAll(containerList);
    }

    public Container pollContainer() {
        if (!containers.isEmpty()) {
            return containers.poll();
        } else
            return null;
    }

    public boolean offerContainer(Container container) {
        return containers.offer(container);
    }

    public int getNumber() {
        return berth.getBerthNumber();
    }

    private static List<Container> generateContainers() {
        //generates cargo on berth
        Random generator = new Random();
        int length = generator.nextInt(30);
        Container[] containers = new Container[length];
        for (int i = 0; i < length; i++) {
            containers[i] = new Container(generator.nextInt(500));
        }
        return Arrays.asList(containers);
    }

}