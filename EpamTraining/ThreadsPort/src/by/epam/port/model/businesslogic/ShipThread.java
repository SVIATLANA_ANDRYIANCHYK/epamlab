package by.epam.port.model.businesslogic;

import by.epam.port.exception.ResourceException;
import by.epam.port.model.domain.Container;
import by.epam.port.model.domain.Ship;
import org.apache.log4j.Logger;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ShipThread implements Runnable {

    public static final Logger LOG = Logger.getLogger(ShipThread.class);
    private Queue<Container> shipStorage;
    private Ship ship;
    private PortManager portManager;
    private BerthManager berthManager;
    private Lock lock = new ReentrantLock();

    public ShipThread(String name, int capacity, PortManager portManager, int amountContainer) {
        ship = new Ship(name, capacity, amountContainer);
        this.portManager = portManager;
        this.shipStorage = new ArrayDeque<Container>(capacity);
        generateShipСargo();
    }

    @Override
    public void run() {
        goToPort();
        if (ship.getAmountContainer() == 0) {
            loadShip();
        } else {
            if (ship.getAmountContainer() < ship.getCapacity() && ship.getAmountContainer() != 0) {
                unloadShip();
                loadShip();
            }
        }
        if (ship.getAmountContainer() == ship.getCapacity()) {
            unloadShip();
        }
        goAwayFromPort();
    }


    private void loadShip(){
        while (!this.tryLoadShip()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                LOG.error(e.getMessage());
            }
        }
    }

    private void unloadShip(){
        while (!this.tryUnloadShip()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                LOG.error(e.getMessage());
            }
        }
    }

    private boolean tryLoadShip() {
        lock.lock();
        try {
            for (int i = 0; i < ship.getCapacity() - ship.getAmountContainer(); i++) {
                Container item = berthManager.pollContainer();
                if (item == null) { // если в порту нет товаров, загрузка
                    System.out.println("В порту закончились товары!"); // прекращается
                    LOG.info("В порту закончились товары!");
                    goAwayFromPort();
                    return true;
                }
                if (shipStorage.offer(item)) {
                    System.out.println("Корабль " + ship.getName() + " загрузил контейнер №"
                            + item.getRegistrationNumber());
                    LOG.info("Корабль " + ship.getName() + " загрузил контейнер №"
                            + item.getRegistrationNumber());
                    return true;
                } else {
                    System.out.println("Не хватает места на складе корабля!");
                    LOG.info("Не хватает места на складе корабля!");
                    goAwayFromPort();
                    return true;
                }

            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                LOG.error("Exception: interrupted. ", e);
            }
            return false;
        } finally {
            lock.unlock();
        }

    }

    private boolean tryUnloadShip() {
        lock.lock();
        try {
            if (ship.getAmountContainer() == 0) {
                System.out.println("Корабль пустой!");
                LOG.info("Корабль пустой!");
                return false; // если корабль пустой выгрузка прекращается
            }
            for (int i = 0; i < ship.getAmountContainer(); i++) {
                Container item = shipStorage.poll();
                if (berthManager.offerContainer(item)) {
                    System.out.println("Корабль " + ship.getName() + " разгрузил контейнер №"
                            + item.getRegistrationNumber());
                    LOG.info("Корабль " + ship.getName() + " разгрузил контейнер №"
                            + item.getRegistrationNumber());
                    return true;
                } else {
                    System.out.println("Нету места на складе в порту!");
                    LOG.error("Нету места на складе в порту!");
                    goToPort();
                    return true;
                }
            }
            try {
                Thread.sleep(50);

            } catch (InterruptedException e) {
                LOG.error("Exception: interrupted. ", e);
            }
            ship.setAmountContainer(0);
            return false;

        } finally {
            lock.unlock();
        }
    }


    private void goToPort() {
        lock.lock();
        try {
            tryToMoor();
            Thread.sleep(new Random(100).nextInt(1000));
            System.out.println("Корабль " + ship.getName() + " причалил на "
                    + berthManager.getNumber() + " причал.");
        } catch (InterruptedException e) {
            LOG.error("Exception: interrupted. ", e);
        }
        finally {
            lock.unlock();
        }
    }

    private void goAwayFromPort() {
        lock.lock();
        try {
            Thread.sleep(new Random(100).nextInt(500));
            System.out.println("Корабль " + ship.getName() + " отчалил с "
                    + berthManager.getNumber() + " причала.");
            LOG.info("Корабль " + ship.getName() + " отчалил с " + berthManager.getNumber()
                    + " причала.");
        } catch (InterruptedException e) {
            LOG.error("Exception: interrupted. ", e);
        } finally {
            portManager.releaseResource(berthManager);
            System.out.println("Причал #" + berthManager.getNumber() + " свободен.");
            LOG.info("Причал #" + berthManager.getNumber() + " свободен.");
            lock.unlock();
        }

    }

    private void tryToMoor() {
        do {
            try {
                berthManager = portManager.findBerth();
            } catch (ResourceException e) {
                LOG.error(e.getMessage());
            }
        } while (berthManager == null);
    }

    private void generateShipСargo() {
        //generates containers on ship
        if (ship.getAmountContainer() > 0) {
            Container[] containers = new Container[ship.getCapacity()];
            for (int i = 0; i < ship.getAmountContainer(); i++) {
                containers[i] = new Container(i + 100);
                shipStorage.add(containers[i]);
            }
        }
    }
}