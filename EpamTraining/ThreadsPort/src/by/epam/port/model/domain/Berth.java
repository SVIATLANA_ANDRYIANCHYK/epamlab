package by.epam.port.model.domain;


public class Berth {
    private int berthNumber;

    public Berth(int berthNumber) {
        this.berthNumber = berthNumber;
    }

    public int getBerthNumber() {
        return berthNumber;
    }

    public void setBerthNumber(int berthNumber) {
        this.berthNumber = berthNumber;
    }
}
