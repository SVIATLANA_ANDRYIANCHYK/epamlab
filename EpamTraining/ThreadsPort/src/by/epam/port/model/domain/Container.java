package by.epam.port.model.domain;


public class Container implements Comparable<Container> {
    private int registrationNumber;

    public Container(int registrationNumber) {
        this.registrationNumber = registrationNumber;
    }


    public int getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(int registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @Override
    public int compareTo(Container o) {
        return o.getRegistrationNumber() - this.getRegistrationNumber();
    }
}
