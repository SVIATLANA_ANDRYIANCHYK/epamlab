package by.epam.port.model.domain;


public class Ship {
    private String name;
    private int amountContainer;
    private int capacity;

    public Ship(String name, int capacity, int amountContainer) {
        this.name = name;
        this.capacity = capacity;
        this.amountContainer = amountContainer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmountContainer() {
        return amountContainer;
    }

    public void setAmountContainer(int amountContainer) {
        this.amountContainer = amountContainer;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
