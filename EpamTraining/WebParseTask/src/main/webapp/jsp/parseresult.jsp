<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Parse result</title>
    <style>
        .beautifulTable {
            margin: 0px;
            padding: 0px;
            width: 100%;
            border: 1px solid #000000;

            -moz-border-radius-bottomleft: 7px;
            -webkit-border-bottom-left-radius: 7px;
            border-bottom-left-radius: 7px;

            -moz-border-radius-bottomright: 7px;
            -webkit-border-bottom-right-radius: 7px;
            border-bottom-right-radius: 7px;

            -moz-border-radius-topright: 7px;
            -webkit-border-top-right-radius: 7px;
            border-top-right-radius: 7px;

            -moz-border-radius-topleft: 7px;
            -webkit-border-top-left-radius: 7px;
            border-top-left-radius: 7px;
        }

        .beautifulTable table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            height: 100%;
            margin: 0px;
            padding: 0px;
        }

        .beautifulTable tr:last-child td:last-child {
            -moz-border-radius-bottomright: 7px;
            -webkit-border-bottom-right-radius: 7px;
            border-bottom-right-radius: 7px;
        }

        .beautifulTable table tr:first-child td:first-child {
            -moz-border-radius-topleft: 7px;
            -webkit-border-top-left-radius: 7px;
            border-top-left-radius: 7px;
        }

        .beautifulTable table tr:first-child td:last-child {
            -moz-border-radius-topright: 7px;
            -webkit-border-top-right-radius: 7px;
            border-top-right-radius: 7px;
        }

        .beautifulTable tr:last-child td:first-child {
            -moz-border-radius-bottomleft: 7px;
            -webkit-border-bottom-left-radius: 7px;
            border-bottom-left-radius: 7px;
        }

        .beautifulTable tr:hover td {

        }

        .beautifulTable tr:nth-child(odd) {
            background-color: #e3effc;
        }

        .beautifulTable tr:nth-child(even) {
            background-color: #ffffff;
        }

        .beautifulTable td {
            vertical-align: middle;

            border: 1px solid #000000;
            border-width: 0px 1px 1px 0px;
            text-align: left;
            padding: 7px;
            font-size: 10px;
            font-family: Arial;
            font-weight: normal;
            color: #000000;
        }

        .beautifulTable tr:last-child td {
            border-width: 0px 1px 0px 0px;
        }

        .beautifulTable tr td:last-child {
            border-width: 0px 0px 1px 0px;
        }

        .beautifulTable tr:last-child td:last-child {
            border-width: 0px 0px 0px 0px;
        }

        .beautifulTable tr:first-child td {
            background: -o-linear-gradient(bottom, #3e7dbc 5%, #2f577f 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #3e7dbc), color-stop(1, #2f577f));
            background: -moz-linear-gradient(center top, #3e7dbc 5%, #2f577f 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3e7dbc", endColorstr="#2f577f");
            background: -o-linear-gradient(top, #3e7dbc, 2 f577f);

            background-color: #3e7dbc;
            border: 0px solid #000000;
            text-align: center;
            border-width: 0px 0px 1px 1px;
            font-size: 14px;
            font-family: Arial;
            font-weight: bold;
            color: #ffffff;
        }

        .beautifulTable tr:first-child:hover td {
            background: -o-linear-gradient(bottom, #3e7dbc 5%, #2f577f 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #3e7dbc), color-stop(1, #2f577f));
            background: -moz-linear-gradient(center top, #3e7dbc 5%, #2f577f 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3e7dbc", endColorstr="#2f577f");
            background: -o-linear-gradient(top, #3e7dbc, 2 f577f);

            background-color: #3e7dbc;
        }

        .beautifulTable tr:first-child td:first-child {
            border-width: 0px 0px 1px 0px;
        }

        .beautifulTable tr:first-child td:last-child {
            border-width: 0px 0px 1px 1px;
        }
    </style>
</head>
<body>
<table class="beautifulTable">
    <tr>
        <td>Name</td>
        <td>Operator</td>
        <td>Payroll</td>
        <td>SMS price</td>
        <td>Billing</td>
        <td>Favourite numbers</td>
        <td>Tariff select price</td>
        <td>Inner calls price</td>
        <td>Outer calls price</td>
        <td>Static calls price</td>
    </tr>
    <c:forEach items="${list}" var="element">
        <tr>
            <td>${element.name}</td>
            <td>${element.operator}</td>
            <td>${element.payroll}</td>
            <td>${element.smsPrice}</td>
            <td>${element.billing}</td>
            <td>${element.favouriteNumbers}</td>
            <td>${element.tariffSelectPrice}</td>
            <td>${element.innerCallsPrice}</td>
            <td>${element.outCallsPrice}</td>
            <td>${element.staticCallPrice}</td>
        </tr>
    </c:forEach>
</table>
<br/>

<form method="get" action="../index.jsp">
    <input type="submit" value="back"/>
</form>
</body>
</html>
