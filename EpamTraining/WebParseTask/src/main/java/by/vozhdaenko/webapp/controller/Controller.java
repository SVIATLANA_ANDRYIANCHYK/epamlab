package by.vozhdaenko.webapp.controller;

import by.vozhdaenko.tariffs.builder.AbstractTariffBuilder;
import by.vozhdaenko.tariffs.entity.Tariff;
import by.vozhdaenko.tariffs.exception.LogicException;
import by.vozhdaenko.tariffs.exception.TechnicalException;
import by.vozhdaenko.tariffs.factory.TariffBuilderFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.lf5.viewer.configure.ConfigurationManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;


@WebServlet(name = "act", urlPatterns = "/act")
public class Controller extends HttpServlet
{
    private static final Logger LOG = Logger.getLogger(Controller.class);

    static
    {
        Properties logProperties = new Properties();
        try
        {
            logProperties.load(ConfigurationManager.class.getResourceAsStream("/conf/log.properties"));
        }
        catch (IOException e)
        {
            throw new ExceptionInInitializerError(e);
        }
        PropertyConfigurator.configure(logProperties);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String page;
        try
        {
            String parserName = request.getParameter("parser");
            TariffBuilderFactory factory = TariffBuilderFactory.getInstance();
            AbstractTariffBuilder.setSchemaPath(getServletContext().getRealPath("/data/schema.xsd"));
            AbstractTariffBuilder builder = factory.createTariffBuilder(parserName);
            builder.buildTariffs(new File(getServletContext().getRealPath("/data/in.xml")));
            ArrayList<Tariff> list = new ArrayList<>(builder.getTariffs());
            request.setAttribute("list", list);
            page = ResourceBundle.getBundle("conf.paths").getString("path.resultpage");
        }
        catch (TechnicalException | LogicException e)
        {
            LOG.error(e);
            request.setAttribute("errormes", e.getMessage());
            page = ResourceBundle.getBundle("conf.paths").getString("path.errorpage");
        }
        RequestDispatcher rd = request.getServletContext().getRequestDispatcher(page);
        rd.forward(request, response);
    }
}
