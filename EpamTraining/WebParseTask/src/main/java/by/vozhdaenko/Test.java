package by.vozhdaenko;

/**
 * @author Artyom
 */
public class Test
{
    private Test m()
    {
        System.out.println("lol");
        return this;
    }

    public static void main(String[] args)
    {
        new Test().m().m();
        Test t = new SubTest();
        t.m().m();
    }
}

class SubTest extends Test
{
    public SubTest m()
    {
        System.out.println("sublol");
        return this;
    }
}
