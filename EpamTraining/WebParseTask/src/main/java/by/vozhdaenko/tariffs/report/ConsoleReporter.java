package by.vozhdaenko.tariffs.report;

import by.vozhdaenko.tariffs.exception.TechnicalException;

/**
 * Reports given information to system out stream.
 */
public class ConsoleReporter implements Reporter
{
    /**
     * Reports the selected object to console.
     * Inserts the new line after report.
     *
     * @param object target to reporting
     * @throws TechnicalException if some technical problems occurred while reporting
     */
    @Override
    public void report(Object object) throws TechnicalException
    {
        System.out.println(object);
        if (System.out.checkError())
        {
            throw new TechnicalException("problems occurred while reporting to console.");
        }
    }
}
