package by.vozhdaenko.tariffs.factory;

import by.vozhdaenko.tariffs.builder.AbstractTariffBuilder;
import by.vozhdaenko.tariffs.builder.TariffDOMBuilder;
import by.vozhdaenko.tariffs.builder.TariffSAXBuilder;
import by.vozhdaenko.tariffs.builder.TariffStAXBuilder;
import by.vozhdaenko.tariffs.exception.LogicException;
import by.vozhdaenko.tariffs.exception.TechnicalException;

/**
 * Factory that creates builder with given type.
 */
public class TariffBuilderFactory
{
    private static TariffBuilderFactory instance = new TariffBuilderFactory();

    private enum ParserType
    {
        DOM, SAX, STAX
    }

    private TariffBuilderFactory()
    {
    }

    public static TariffBuilderFactory getInstance()
    {
        return instance;
    }

    /**
     * Creates tariff builder that builds tariffs from XML file.
     *
     * @param type xml-parser type, not null. Can be DOM, SAX or StAX.
     * @return concrete tariff builder according to given parser type
     * @throws by.vozhdaenko.tariffs.exception.TechnicalException if technical problems occur
     * @throws by.vozhdaenko.tariffs.exception.LogicException     if null xml parser type argument is given
     */
    public AbstractTariffBuilder createTariffBuilder(String type) throws TechnicalException, LogicException
    {
        if (type == null)
        {
            throw new LogicException("null xml parser type argument");
        }
        ParserType parserType;
        try
        {
            parserType = ParserType.valueOf(type.toUpperCase());
        }
        catch (IllegalArgumentException e)
        {
            throw new LogicException("unknown xml parser type argument");
        }
        switch (parserType)
        {
            case DOM:
                return new TariffDOMBuilder();
            case SAX:
                return new TariffSAXBuilder();
            case STAX:
                return new TariffStAXBuilder();
            default:
                throw new LogicException("unknown xml parser type argument");
        }
    }
}
