package by.vozhdaenko.tariffs.validator;

import by.vozhdaenko.tariffs.exception.LogicException;

/**
 * Defines interface and functions for validating data
 * for {@link by.vozhdaenko.tariffs.entity.Tariff} objects.
 *
 * @author - Artyom
 *         Created on 11.03.2015.
 */
public class TariffValidator
{
    public static void validateId(int id) throws LogicException
    {
        if (id < 10 || id >= 100000)
        {
            throw new LogicException("incorrect tariff id");
        }
    }

    public static void validateName(String name) throws LogicException
    {
        if (name == null || name.length() == 0)
        {
            throw new LogicException("incorrect tariff name");
        }
    }

    public static void validateNull(Object value) throws LogicException
    {
        if (value == null)
        {
            throw new LogicException("incorrect tariff value (null)");
        }
    }

    public static void validatePositive(int value) throws LogicException
    {
        if (value < 0)
        {
            throw new LogicException("incorrect price value");
        }
    }

    public static void validateDiscount(double discount) throws LogicException
    {
        if (discount < 0 && discount > 1)
        {
            throw new LogicException("incorrect tariff discount");
        }
    }
}
