package by.vozhdaenko.tariffs.entity;

import by.vozhdaenko.tariffs.exception.LogicException;
import by.vozhdaenko.tariffs.validator.TariffValidator;

/**
 * Tariff abstract entity.
 */
public abstract class Tariff
{
    private int id;
    private String name;
    private Operator operator;
    private int payroll;
    private CallPrices prices;
    private int smsPrice;
    private Parameters parameters;

    /**
     * Default constructor.
     */
    public Tariff()
    {
        prices = new CallPrices();
        parameters = new Parameters();
    }

    /**
     * Constructor to pack data.
     * Its recommended to validate data before creating.
     *
     * @param id                id of the tariff
     * @param name              name of the tariff
     * @param operator          name of the operator
     * @param payroll           payroll of the tariff in money values
     * @param innerCallsPrice   inner call price
     * @param outCallsPrice     outer call price
     * @param staticCallPrice   static call price
     * @param smsPrice          SMS_PRICE price
     * @param favouriteNumbers  number of favourite phone numbers
     * @param billing           billing of the tariff
     * @param tariffSelectPrice money value to pay for tariff connection
     */
    public Tariff(int id, String name, Operator operator, int payroll, int innerCallsPrice, int outCallsPrice,
                  int staticCallPrice, int smsPrice, int favouriteNumbers, Billing billing,
                  int tariffSelectPrice) throws LogicException
    {
        setId(id);
        setName(name);
        setOperator(operator);
        setPayroll(payroll);
        this.prices = new CallPrices(innerCallsPrice, outCallsPrice, staticCallPrice);
        setSmsPrice(smsPrice);
        this.parameters = new Parameters(favouriteNumbers, billing, tariffSelectPrice);
    }

    public void setId(int id) throws LogicException
    {
        TariffValidator.validateId(id);
        this.id = id;
    }

    public void setName(String name) throws LogicException
    {
        TariffValidator.validateName(name);
        this.name = name;
    }

    public int getPayroll()
    {
        return payroll;
    }

    public void setPayroll(int payroll) throws LogicException
    {
        TariffValidator.validatePositive(payroll);
        this.payroll = payroll;
    }

    public void setOperator(Operator operator) throws LogicException
    {
        TariffValidator.validateNull(operator);
        this.operator = operator;
    }

    public void setSmsPrice(int smsPrice) throws LogicException
    {
        TariffValidator.validatePositive(smsPrice);
        this.smsPrice = smsPrice;
    }

    public void setStaticCallPrice(int staticCallPrice) throws LogicException
    {
        TariffValidator.validatePositive(staticCallPrice);
        prices.setStaticCallPrice(staticCallPrice);
    }

    public void setOutCallsPrice(int outCallsPrice) throws LogicException
    {
        TariffValidator.validatePositive(outCallsPrice);
        prices.setOutCallsPrice(outCallsPrice);
    }

    public void setInnerCallsPrice(int innerCallsPrice) throws LogicException
    {
        TariffValidator.validatePositive(innerCallsPrice);
        prices.setInnerCallsPrice(innerCallsPrice);
    }

    public void setFavouriteNumbers(int favouriteNumbers) throws LogicException
    {
        TariffValidator.validatePositive(favouriteNumbers);
        parameters.setFavouriteNumbers(favouriteNumbers);
    }

    public void setBilling(Billing billing) throws LogicException
    {
        TariffValidator.validateNull(billing);
        parameters.setBilling(billing);
    }

    public void setTariffSelectPrice(int tariffSelectPrice) throws LogicException
    {
        TariffValidator.validatePositive(tariffSelectPrice);
        parameters.setTariffSelectPrice(tariffSelectPrice);
    }

    /**
     * String representation of content of
     * {@link by.vozhdaenko.tariffs.entity.Tariff} object.
     */
    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();
        sb.append("name='").append(name).append('\'');
        sb.append(", operator=").append(operator);
        sb.append(", payroll=").append(payroll);
        sb.append(", prices=").append(prices);
        sb.append(", smsPrice=").append(smsPrice);
        sb.append(", parameters=").append(parameters);
        sb.append('}');
        return sb.toString();
    }

    /**
     * Describes the type of billing.
     * May be SECOND or MINUTE (per-second or per-minute billing).
     */
    public enum Billing
    {
        SECOND, MINUTE
    }

    /**
     * Describes prices for different call types.
     */
    private class CallPrices
    {
        private int innerCallsPrice;
        private int outCallsPrice;
        private int staticCallPrice;

        public CallPrices()
        {
        }

        public CallPrices(int innerCallsPrice, int outCallsPrice, int staticCallPrice) throws LogicException
        {
            setInnerCallsPrice(innerCallsPrice);
            setOutCallsPrice(outCallsPrice);
            setStaticCallPrice(staticCallPrice);
        }

        public void setStaticCallPrice(int staticCallPrice) throws LogicException
        {
            TariffValidator.validatePositive(staticCallPrice);
            this.staticCallPrice = staticCallPrice;
        }

        public void setOutCallsPrice(int outCallsPrice) throws LogicException
        {
            TariffValidator.validatePositive(outCallsPrice);
            this.outCallsPrice = outCallsPrice;
        }

        public void setInnerCallsPrice(int innerCallsPrice) throws LogicException
        {
            TariffValidator.validatePositive(innerCallsPrice);
            this.innerCallsPrice = innerCallsPrice;
        }

        public int getInnerCallsPrice()
        {
            return innerCallsPrice;
        }

        public int getOutCallsPrice()
        {
            return outCallsPrice;
        }

        public int getStaticCallPrice()
        {
            return staticCallPrice;
        }

        @Override
        public String toString()
        {
            final StringBuilder sb = new StringBuilder("CallPrices{");
            sb.append("innerCallsPrice=").append(innerCallsPrice);
            sb.append(", outCallsPrice=").append(outCallsPrice);
            sb.append(", staticCallPrice=").append(staticCallPrice);
            sb.append('}');
            return sb.toString();
        }
    }

    /**
     * Some additional tariff parameters.
     */
    private class Parameters
    {
        private int favouriteNumbers;
        private Billing billing;
        private int tariffSelectPrice;

        public Parameters()
        {
        }

        public Parameters(int favouriteNumbers, Billing billing, int tariffSelectPrice) throws LogicException
        {
            setFavouriteNumbers(favouriteNumbers);
            setBilling(billing);
            setTariffSelectPrice(tariffSelectPrice);
        }

        public void setFavouriteNumbers(int favouriteNumbers) throws LogicException
        {
            TariffValidator.validatePositive(favouriteNumbers);
            this.favouriteNumbers = favouriteNumbers;
        }

        public void setBilling(Billing billing) throws LogicException
        {
            TariffValidator.validateNull(billing);
            this.billing = billing;
        }

        public void setTariffSelectPrice(int tariffSelectPrice) throws LogicException
        {
            TariffValidator.validatePositive(tariffSelectPrice);
            this.tariffSelectPrice = tariffSelectPrice;
        }

        public int getFavouriteNumbers()
        {
            return favouriteNumbers;
        }

        public Billing getBilling()
        {
            return billing;
        }

        public int getTariffSelectPrice()
        {
            return tariffSelectPrice;
        }

        @Override
        public String toString()
        {
            final StringBuilder sb = new StringBuilder("Parameters{");
            sb.append("favouriteNumbers=").append(favouriteNumbers);
            sb.append(", billing=").append(billing);
            sb.append(", tariffSelectPrice=").append(tariffSelectPrice);
            sb.append('}');
            return sb.toString();
        }
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public Operator getOperator()
    {
        return operator;
    }

    public int getSmsPrice()
    {
        return smsPrice;
    }

    public int getInnerCallsPrice()
    {
        return prices.getInnerCallsPrice();
    }

    public int getOutCallsPrice()
    {
        return prices.getOutCallsPrice();
    }

    public int getStaticCallPrice()
    {
        return prices.getStaticCallPrice();
    }

    public Billing getBilling()
    {
        return parameters.getBilling();
    }

    public int getFavouriteNumbers()
    {
        return parameters.getFavouriteNumbers();
    }

    public int getTariffSelectPrice()
    {
        return parameters.getTariffSelectPrice();
    }
}
