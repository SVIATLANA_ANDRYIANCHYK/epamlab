package by.vozhdaenko.tariffs.report;

import by.vozhdaenko.tariffs.exception.TechnicalException;

/**
 * Tells that object can report some information.
 */
public interface Reporter
{
    /**
     * Reports the selected object to destination place.
     *
     * @param object target to reporting
     * @throws TechnicalException if some technical problems occurred while reporting
     */
    public void report(Object object) throws TechnicalException;
}
