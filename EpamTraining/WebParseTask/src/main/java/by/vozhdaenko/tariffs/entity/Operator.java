package by.vozhdaenko.tariffs.entity;

/**
 * Operators of mobile connection enumeration
 */
public enum Operator
{
    MTS, VELCOM, LIFE, BEELINE, VODAFONE;
}
