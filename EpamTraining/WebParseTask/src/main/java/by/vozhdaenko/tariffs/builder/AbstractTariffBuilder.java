package by.vozhdaenko.tariffs.builder;

import by.vozhdaenko.tariffs.entity.Tariff;
import by.vozhdaenko.tariffs.exception.LogicException;
import by.vozhdaenko.tariffs.exception.TechnicalException;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Abstract class.
 * Defines interface to tariff builder classes.
 */
public abstract class AbstractTariffBuilder
{
    protected Set<Tariff> tariffs = new HashSet<>();
    protected static String schemaPath = "/src/main/resources/data/schema.xsd";

    /**
     * Default constructor.
     */
    public AbstractTariffBuilder()
    {
    }

    /**
     * Creates builder using existing set of tariffs.
     *
     * @param tariffs set of tariffs builder will be based on
     */
    public AbstractTariffBuilder(Set<Tariff> tariffs) throws LogicException
    {
        if (tariffs == null)
        {
            throw new LogicException("null tariffs");
        }
        this.tariffs = tariffs;
    }

    /**
     * Returns modifiable set of tariffs.
     *
     * @return modifiable set of tariffs
     */
    public Set<Tariff> getTariffs()
    {
        return tariffs;
    }

    /**
     * Builds tariffs from given xml file.
     *
     * @param file source that is an xml file
     * @throws by.vozhdaenko.tariffs.exception.TechnicalException if some technical problems occur
     * @throws by.vozhdaenko.tariffs.exception.LogicException     if null file is given
     */
    abstract public void buildTariffs(File file) throws TechnicalException, LogicException;

    protected void validate(Source source) throws TechnicalException
    {
        try
        {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(schemaPath));
            Validator validator = schema.newValidator();
            validator.validate(source);
        }
        catch (IOException | SAXException e)
        {
            throw new TechnicalException("validation problems");
        }
    }

    public static String getSchemaPath()
    {
        return schemaPath;
    }

    public static void setSchemaPath(String schemaPath)
    {
        AbstractTariffBuilder.schemaPath = schemaPath;
    }
}
