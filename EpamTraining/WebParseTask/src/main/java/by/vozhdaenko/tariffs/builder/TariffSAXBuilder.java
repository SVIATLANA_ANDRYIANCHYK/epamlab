package by.vozhdaenko.tariffs.builder;

import by.vozhdaenko.tariffs.errorhandler.LogErrorHandler;
import by.vozhdaenko.tariffs.exception.LogicException;
import by.vozhdaenko.tariffs.exception.TechnicalException;
import by.vozhdaenko.tariffs.saxhandler.TariffContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.File;
import java.io.IOException;

/**
 * Builds tariffs.
 * Input file is XML-typed and SAX parser is using.
 */
public class TariffSAXBuilder extends AbstractTariffBuilder
{
    private XMLReader reader;
    private TariffContentHandler tariffContentHandler;

    /**
     * Creates a new builder instance and
     * initializes the SAX parser.
     *
     * @throws TechnicalException if SAX parser init fails
     */
    public TariffSAXBuilder() throws TechnicalException
    {
        try
        {
            tariffContentHandler = new TariffContentHandler();
            reader = XMLReaderFactory.createXMLReader();
            reader.setErrorHandler(new LogErrorHandler());
            reader.setContentHandler(tariffContentHandler);
        }
        catch (SAXException e)
        {
            throw new TechnicalException("sax init problem", e);
        }
    }

    /**
     * Builds tariffs from given xml file.
     *
     * @param file source that is an xml file
     * @throws TechnicalException if some technical problems occur
     * @throws LogicException     if null file is given
     */
    @Override
    public void buildTariffs(File file) throws TechnicalException, LogicException
    {
        try
        {
            reader.parse(file.getAbsolutePath());
            tariffs.addAll(tariffContentHandler.getTariffs());
        }
        catch (SAXException | IOException e)
        {
            throw new TechnicalException("parse failed", e);
        }
    }
}
