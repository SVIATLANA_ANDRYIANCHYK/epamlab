package by.vozhdaenko.tariffs.demonstration;

import by.vozhdaenko.tariffs.builder.AbstractTariffBuilder;
import by.vozhdaenko.tariffs.entity.Tariff;
import by.vozhdaenko.tariffs.exception.LogicException;
import by.vozhdaenko.tariffs.exception.TechnicalException;
import by.vozhdaenko.tariffs.factory.TariffBuilderFactory;
import by.vozhdaenko.tariffs.report.ConsoleReporter;
import by.vozhdaenko.tariffs.report.Reporter;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

public class Demo
{
    private static final Logger LOG = Logger.getLogger(Demo.class);

    static
    {
        Properties logProperties = new Properties();
        try
        {
            logProperties.load(Demo.class.getResourceAsStream("/log/log.properties"));
        }
        catch (IOException e)
        {
            throw new ExceptionInInitializerError();
        }
        PropertyConfigurator.configure(logProperties);
    }

    public static void main(String[] args)
    {
        try
        {
            Reporter reporter = new ConsoleReporter();
            reporter.report("starting demonstration...");
            TariffBuilderFactory factory = TariffBuilderFactory.getInstance();
            AbstractTariffBuilder builder = factory.createTariffBuilder("sax");
            builder.buildTariffs(new File("src/main/resources/xml/in.xml"));
            ArrayList<Tariff> list = new ArrayList<>(builder.getTariffs());
            reporter.report(Arrays.toString(list.toArray()));
            Collections.sort(list, (o1, o2) -> o1.getPayroll() - o2.getPayroll());
            reporter.report("sorted by payroll");
            reporter.report(Arrays.toString(list.toArray()));
            reporter.report("end of demonstration");
        }
        catch (TechnicalException | LogicException e)
        {
            LOG.error(e);
        }
    }
}
