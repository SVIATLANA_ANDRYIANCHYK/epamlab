package by.vozhdaenko.tariffs.entity;

import by.vozhdaenko.tariffs.exception.LogicException;
import by.vozhdaenko.tariffs.validator.TariffValidator;

/**
 * Internet tariff class
 */
public class InternetTariff extends Tariff
{
    private int trafficQuantity;

    /**
     * Default constructor.
     */
    public InternetTariff()
    {
    }

    /**
     * Creates internet tariff.
     * Its recommended to validate data before creating.
     *
     * @param id                id of the tariff
     * @param name              name of the tariff
     * @param operator          name of the operator
     * @param payroll           payroll of the tariff in money values
     * @param innerCallsPrice   inner call price
     * @param outCallsPrice     outer call price
     * @param staticCallPrice   static call price
     * @param smsPrice          SMS_PRICE price
     * @param favouriteNumbers  number of favourite phone numbers
     * @param billing           billing of the tariff
     * @param tariffSelectPrice money value to pay for tariff connection
     * @param trafficQuantity   amount of internet traffic given to user every month
     */
    public InternetTariff(int id, String name, Operator operator, int payroll, int innerCallsPrice,
                          int outCallsPrice, int staticCallPrice, int smsPrice, int favouriteNumbers,
                          Billing billing, int tariffSelectPrice, int trafficQuantity) throws LogicException
    {
        super(id, name, operator, payroll, innerCallsPrice, outCallsPrice, staticCallPrice,
                smsPrice, favouriteNumbers, billing, tariffSelectPrice);
        setTrafficQuantity(trafficQuantity);
    }

    public void setTrafficQuantity(int trafficQuantity) throws LogicException
    {
        TariffValidator.validatePositive(trafficQuantity);
        this.trafficQuantity = trafficQuantity;
    }

    /**
     * @return {@code string} representation of {@link by.vozhdaenko.tariffs.entity.InternetTariff} entity.
     */
    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("InternetTariff{");
        sb.append(super.toString()).append("trafficQuantity=");
        sb.append(trafficQuantity).append('}');
        return sb.toString();
    }
}
