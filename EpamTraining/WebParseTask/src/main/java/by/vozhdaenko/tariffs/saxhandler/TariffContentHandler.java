package by.vozhdaenko.tariffs.saxhandler;

import by.vozhdaenko.tariffs.builder.TariffElementsEnum;
import by.vozhdaenko.tariffs.entity.*;
import by.vozhdaenko.tariffs.exception.LogicException;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * Forms tariff objects during SAX parse
 */
public class TariffContentHandler extends DefaultHandler
{
    private Set<Tariff> tariffs;
    private Tariff currentTariff;
    private TariffElementsEnum currentEnum;
    private EnumSet<TariffElementsEnum> widthEnum;
    private static final Logger LOG = Logger.getLogger(TariffContentHandler.class);
    private boolean wrong;

    /**
     * Creates new instance of tariff handler and initializes it.
     */
    public TariffContentHandler()
    {
        tariffs = new HashSet<>();
        widthEnum = EnumSet.range(TariffElementsEnum.NAME, TariffElementsEnum.DISCOUNT);
    }

    /**
     * Gives an access to {@code Set} of
     * {@code Tariff}s formed after parsing.
     *
     * @return {@link java.util.Set} of
     * {@link by.vozhdaenko.tariffs.entity.Tariff} objects.
     */
    public Set<Tariff> getTariffs()
    {
        return tariffs;
    }

    /**
     * Receive notification of the start of an element.
     * <p>
     * <p>Creates a new {@link by.vozhdaenko.tariffs.entity.Tariff}
     * object.</p>
     *
     * @param uri        The Namespace URI, or the empty string if the
     *                   element has no Namespace URI or if Namespace
     *                   processing is not being performed.
     * @param localName  The local name (without prefix), or the
     *                   empty string if Namespace processing is not being
     *                   performed.
     * @param qName      The qualified name (with prefix), or the
     *                   empty string if qualified names are not available.
     * @param attributes The attributes attached to the element.  If
     *                   there are no attributes, it shall be an empty
     *                   Attributes object.
     * @throws org.xml.sax.SAXException Any SAX exception, possibly
     *                                  wrapping another exception.
     * @see org.xml.sax.ContentHandler#startElement
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        try
        {
            TariffElementsEnum type = TariffElementsEnum.valueOf(localName.replace('-', '_').toUpperCase());
            switch (type)
            {
                case TARIFFS:
                    break;
                case SOCIAL_TARIFF:
                    currentTariff = new SocialTariff();
                    currentTariff.setId(Integer.parseInt(attributes.getValue(0).replace("a", "")));
                    break;
                case ECONOMIC_TARIFF:
                    currentTariff = new EconomicTariff();
                    currentTariff.setId(Integer.parseInt(attributes.getValue(0).replace("a", "")));
                    break;
                case INTERNET_TARIFF:
                    currentTariff = new InternetTariff();
                    currentTariff.setId(Integer.parseInt(attributes.getValue(0).replace("a", "")));
                    break;
                default:
                    if (currentTariff != null)
                    {
                        TariffElementsEnum current = TariffElementsEnum.valueOf(localName.replace('-', '_').toUpperCase());
                        if (widthEnum.contains(current))
                        {
                            currentEnum = current;
                        }
                    }
            }
        }
        catch (IllegalArgumentException | LogicException e)
        {
            LOG.error(e);
        }
    }

    /**
     * Receive notification of the end of an element.
     * <p>
     * <p>Adds formed {@link by.vozhdaenko.tariffs.entity.Tariff}
     * object to the {@code Set}. This {@code Set} can be accessed by using {@link by.vozhdaenko.tariffs.saxhandler.TariffContentHandler#getTariffs} method.</p>
     *
     * @param uri       The Namespace URI, or the empty string if the
     *                  element has no Namespace URI or if Namespace
     *                  processing is not being performed.
     * @param localName The local name (without prefix), or the
     *                  empty string if Namespace processing is not being
     *                  performed.
     * @param qName     The qualified name (with prefix), or the
     *                  empty string if qualified names are not available.
     * @throws org.xml.sax.SAXException Any SAX exception, possibly
     *                                  wrapping another exception.
     * @see org.xml.sax.ContentHandler#endElement
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        TariffElementsEnum type = TariffElementsEnum.valueOf(localName.replace('-', '_').toUpperCase());
        switch (type)
        {
            case SOCIAL_TARIFF:
            case ECONOMIC_TARIFF:
            case INTERNET_TARIFF:
                if (currentTariff != null)
                {
                    tariffs.add(currentTariff);
                }
        }
    }

    /**
     * Receive notification of character data inside an element.
     * <p>
     * <p>Checks name of the element and formes a current
     * {@link by.vozhdaenko.tariffs.entity.Tariff} object in accordance to it.</p>
     *
     * @param ch     The characters.
     * @param start  The start position in the character array.
     * @param length The number of characters to use from the
     *               character array.
     * @throws org.xml.sax.SAXException Any SAX exception, possibly
     *                                  wrapping another exception.
     * @see org.xml.sax.ContentHandler#characters
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {
        try
        {
            String value = new String(ch, start, length).trim();
            if (currentEnum != null && currentTariff != null)
            {
                switch (currentEnum)
                {
                    case NAME:
                        currentTariff.setName(value);
                        break;
                    case OPERATOR:
                        currentTariff.setOperator(Operator.valueOf(value.toUpperCase()));
                        break;
                    case PAYROLL:
                        currentTariff.setPayroll(Integer.parseInt(value));
                        break;
                    case INNER:
                        currentTariff.setInnerCallsPrice(Integer.parseInt(value));
                        break;
                    case OUTER:
                        currentTariff.setOutCallsPrice(Integer.parseInt(value));
                        break;
                    case STATIC:
                        currentTariff.setStaticCallPrice(Integer.parseInt(value));
                        break;
                    case SMS_PRICE:
                        currentTariff.setSmsPrice(Integer.parseInt(value));
                        break;
                    case FAVOURITE_NUMBERS:
                        currentTariff.setFavouriteNumbers(Integer.parseInt(value));
                        break;
                    case BILLING:
                        currentTariff.setBilling(Tariff.Billing.valueOf(value.toUpperCase()));
                        break;
                    case TARIFF_SELECT_PRICE:
                        currentTariff.setTariffSelectPrice(Integer.parseInt(value));
                        break;
                    case FREE_MINUTES_NUMBER:
                        ((EconomicTariff) currentTariff).setFreeMinutesNumber(Integer.parseInt(value));
                        break;
                    case TRAFFIC_QUANTITY:
                        ((InternetTariff) currentTariff).setTrafficQuantity(Integer.parseInt(value));
                        break;
                    case DISCOUNT:
                        ((SocialTariff) currentTariff).setDiscount(Float.parseFloat(value));
                        break;
                }
                currentEnum = null;
            }
        }
        catch (IllegalArgumentException | LogicException e)
        {
            LOG.error(e);
            currentEnum = null;
            currentTariff = null;
        }
    }
}
