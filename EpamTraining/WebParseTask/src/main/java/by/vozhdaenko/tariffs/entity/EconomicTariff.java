package by.vozhdaenko.tariffs.entity;

import by.vozhdaenko.tariffs.exception.LogicException;
import by.vozhdaenko.tariffs.validator.TariffValidator;

/**
 * Economic tariff class
 */
public class EconomicTariff extends Tariff
{
    private int freeMinutesNumber;

    /**
     * Default constructor.
     */
    public EconomicTariff()
    {
    }

    /**
     * Creates new economic tariff.
     * Its recommended to validate data before creating.
     *
     * @param id                id of the tariff
     * @param name              name of the tariff
     * @param operator          name of the operator
     * @param payroll           payroll of the tariff in money values
     * @param innerCallsPrice   inner call price
     * @param outCallsPrice     outer call price
     * @param staticCallPrice   static call price
     * @param smsPrice          SMS_PRICE price
     * @param favouriteNumbers  number of favourite phone numbers
     * @param billing           billing of the tariff
     * @param tariffSelectPrice money value to pay for tariff connection
     * @param freeMinutesNumber free minutes number in month
     */
    public EconomicTariff(int id, String name, Operator operator, int payroll, int innerCallsPrice,
                          int outCallsPrice, int staticCallPrice, int smsPrice, int favouriteNumbers,
                          Billing billing, int tariffSelectPrice, int freeMinutesNumber) throws LogicException
    {
        super(id, name, operator, payroll, innerCallsPrice, outCallsPrice, staticCallPrice,
                smsPrice, favouriteNumbers, billing, tariffSelectPrice);
        setFreeMinutesNumber(freeMinutesNumber);
    }

    public void setFreeMinutesNumber(int freeMinutesNumber) throws LogicException
    {
        TariffValidator.validatePositive(freeMinutesNumber);
        this.freeMinutesNumber = freeMinutesNumber;
    }

    /**
     * @return {@code string} representation of {@link by.vozhdaenko.tariffs.entity.EconomicTariff} entity.
     */
    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("EconomicTariff{");
        sb.append(super.toString()).append("freeMinutesNumber=");
        sb.append(freeMinutesNumber).append('}');
        return sb.toString();
    }
}
