package by.vozhdaenko.tariffs.builder;

import by.vozhdaenko.tariffs.entity.EconomicTariff;
import by.vozhdaenko.tariffs.entity.Operator;
import by.vozhdaenko.tariffs.entity.SocialTariff;
import by.vozhdaenko.tariffs.entity.Tariff;
import by.vozhdaenko.tariffs.errorhandler.LogErrorHandler;
import by.vozhdaenko.tariffs.exception.LogicException;
import by.vozhdaenko.tariffs.exception.TechnicalException;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

/**
 * Tariff builder that uses XML files as a source.
 * Uses DOM for XML parsing.
 */
public class TariffDOMBuilder extends AbstractTariffBuilder
{
    private DocumentBuilder documentBuilder;
    private final static Logger LOG = Logger.getLogger(TariffDOMBuilder.class);

    /**
     * Creates a new tariff builder using DOM parsing.
     * Initializes XML parser.
     *
     * @throws TechnicalException if technical problems occur.
     */
    public TariffDOMBuilder() throws TechnicalException
    {
        initializeDocumentBuilder();
    }

    /**
     * Creates builder using existing set of tariffs.
     * Initializes XML parser.
     *
     * @param tariffs set of tariffs builder will be based on
     */
    public TariffDOMBuilder(Set<Tariff> tariffs) throws TechnicalException, LogicException
    {
        super(tariffs);
        initializeDocumentBuilder();
    }

    private void initializeDocumentBuilder() throws TechnicalException
    {
        DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
        try
        {
            f.setSchema(SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).
                    newSchema(new File(schemaPath)));
            documentBuilder = f.newDocumentBuilder();
            documentBuilder.setErrorHandler(new LogErrorHandler());
        }
        catch (ParserConfigurationException | SAXException e)
        {
            throw new TechnicalException("dom configuration problem", e);
        }
    }

    /**
     * Builds tariffs using DOM parser
     *
     * @param file source that is an xml file
     * @throws by.vozhdaenko.tariffs.exception.TechnicalException if some technical problems occur
     * @throws by.vozhdaenko.tariffs.exception.LogicException     if null file is given
     */
    @Override
    public void buildTariffs(File file) throws TechnicalException, LogicException
    {
        if (file == null)
        {
            throw new LogicException("file object is null");
        }
        try (InputStream input = new FileInputStream(file))
        {
            DOMSource domSource = new DOMSource();
            Document document = documentBuilder.parse(input);
            document.getDocumentElement().normalize();
            domSource.setNode(document);
            validate(domSource);
            NodeList nodeList = document.getElementsByTagName("tariffs").item(0).getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++)
            {
                Node tariff = nodeList.item(i);
                LOG.debug("cur " + tariff.getNodeName());
                if (tariff.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element elementTariff = (Element) tariff;
                    buildTariff(elementTariff);
                }
            }
        }
        catch (IOException | SAXException | LogicException e)
        {
            throw new TechnicalException("Problems during parsing a file", e);
        }
    }

    private void buildTariff(Element tariff) throws TechnicalException, LogicException
    {
        int id = Integer.parseInt(tariff.getAttribute("id").replaceAll("a", ""));
        String name = tariff.getElementsByTagName("name").item(0).getTextContent();
        Operator operator = Operator.
                valueOf(tariff.getElementsByTagName("operator").item(0).
                        getTextContent().toUpperCase());
        int payroll = Integer.
                parseInt(tariff.getElementsByTagName("payroll").item(0).
                        getTextContent());
        int smsPrice = Integer.
                parseInt(tariff.getElementsByTagName("sms-price").item(0).
                        getTextContent());
        int innerCallsPrice = Integer.parseInt(tariff.getElementsByTagName("inner").item(0).
                getTextContent());
        int outerCallsPrice = Integer.parseInt(tariff.getElementsByTagName("outer").item(0).
                getTextContent());
        int staticCallsPrice = Integer.parseInt(tariff.getElementsByTagName("static").item(0).
                getTextContent());
        int favouriteNumbers = Integer.parseInt(tariff.
                getElementsByTagName("favourite-numbers").item(0).getTextContent());
        Tariff.Billing billing = Tariff.Billing.valueOf(tariff.
                getElementsByTagName("billing").item(0).getTextContent().toUpperCase());
        int tariffSelectPrice = Integer.parseInt(tariff.
                getElementsByTagName("tariff-select-price").item(0).getTextContent());
        Tariff tariffObject;
        switch (tariff.getNodeName())
        {
            case "social-tariff":
                float discount = Float.parseFloat(tariff.
                        getElementsByTagName("discount").item(0).getTextContent());
                tariffObject = new SocialTariff(id, name, operator, payroll,
                        innerCallsPrice, outerCallsPrice, staticCallsPrice,
                        smsPrice, favouriteNumbers, billing, tariffSelectPrice, discount);
                break;
            case "economic-tariff":
                int freeMinutes = Integer.parseInt(tariff.
                        getElementsByTagName("free-minutes-number").item(0).getTextContent());
                tariffObject = new EconomicTariff(id, name, operator, payroll,
                        innerCallsPrice, outerCallsPrice, staticCallsPrice,
                        smsPrice, favouriteNumbers, billing, tariffSelectPrice, freeMinutes);
                break;
            case "internet-tariff":
                int trafficQuantity = Integer.parseInt(tariff.
                        getElementsByTagName("traffic-quantity").item(0).getTextContent());
                tariffObject = new SocialTariff(id, name, operator, payroll,
                        innerCallsPrice, outerCallsPrice, staticCallsPrice,
                        smsPrice, favouriteNumbers, billing, tariffSelectPrice, trafficQuantity);
                break;
            default:
                throw new TechnicalException("unsupported tariff");
        }
        tariffs.add(tariffObject);
    }
}
