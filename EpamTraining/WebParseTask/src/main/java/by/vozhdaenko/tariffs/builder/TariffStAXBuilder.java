package by.vozhdaenko.tariffs.builder;

import by.vozhdaenko.tariffs.entity.*;
import by.vozhdaenko.tariffs.exception.LogicException;
import by.vozhdaenko.tariffs.exception.TechnicalException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stax.StAXSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Builds tariffs.
 * Input file is XML-typed and StAX parser is using.
 *
 * @author - Artyom
 *         Created on 11.03.2015.
 */
public class TariffStAXBuilder extends AbstractTariffBuilder
{
    private XMLInputFactory inputFactory;

    public TariffStAXBuilder()
    {
        inputFactory = XMLInputFactory.newInstance();
    }

    /**
     * Builds tariffs from given xml file.
     *
     * @param file source that is an xml file
     * @throws by.vozhdaenko.tariffs.exception.TechnicalException if some technical problems occur
     * @throws by.vozhdaenko.tariffs.exception.LogicException     if null file is given
     */
    @Override
    public void buildTariffs(File file) throws TechnicalException, LogicException
    {
        if (file == null)
        {
            throw new LogicException("null file");
        }
        try (FileInputStream fis = new FileInputStream(file))
        {
            XMLStreamReader validationReader = inputFactory.createXMLStreamReader(fis);
            validate(new StAXSource(validationReader));
            validationReader.close();
        }
        catch (IOException | XMLStreamException e)
        {
            throw new TechnicalException(e);
        }
        try (FileInputStream fis = new FileInputStream(file))
        {
            XMLStreamReader reader = inputFactory.createXMLStreamReader(fis);
            while (reader.hasNext())
            {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT)
                {
                    String name = reader.getLocalName();
                    TariffElementsEnum elementType = TariffElementsEnum.valueOf(name.replace('-', '_').toUpperCase());
                    switch (elementType)
                    {
                        case SOCIAL_TARIFF:
                            tariffs.add(buildTariff(reader, TariffElementsEnum.SOCIAL_TARIFF));
                            break;
                        case ECONOMIC_TARIFF:
                            tariffs.add(buildTariff(reader, TariffElementsEnum.ECONOMIC_TARIFF));
                            break;
                        case INTERNET_TARIFF:
                            tariffs.add(buildTariff(reader, TariffElementsEnum.INTERNET_TARIFF));
                            break;
                    }
                }
            }
            reader.close();
        }
        catch (IOException | XMLStreamException e)
        {
            throw new TechnicalException(e);
        }
    }

    private Tariff buildTariff(XMLStreamReader reader, TariffElementsEnum type) throws LogicException, XMLStreamException, TechnicalException
    {
        Tariff tariff;
        switch (type)
        {
            case SOCIAL_TARIFF:
                tariff = new SocialTariff();
                break;
            case ECONOMIC_TARIFF:
                tariff = new EconomicTariff();
                break;
            case INTERNET_TARIFF:
                tariff = new InternetTariff();
                break;
            default:
                throw new LogicException("unknown tariff type");
        }
        int id = Integer.parseInt(reader.getAttributeValue(null,
                TariffElementsEnum.ID.getValue()).replace("a",""));
        tariff.setId(id);
        while (reader.hasNext())
        {
            String name;
            int elementType = reader.next();
            switch (elementType)
            {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    reader.next();
                    switch (TariffElementsEnum.valueOf(name.toUpperCase().replace('-', '_')))
                    {
                        case NAME:
                            tariff.setName(reader.getText());
                            break;
                        case OPERATOR:
                            tariff.setOperator(Operator.valueOf(reader.getText().toUpperCase()));
                            break;
                        case PAYROLL:
                            tariff.setPayroll(Integer.parseInt(reader.getText()));
                            break;
                        case INNER:
                            tariff.setInnerCallsPrice(Integer.parseInt(reader.getText()));
                            break;
                        case OUTER:
                            tariff.setOutCallsPrice(Integer.parseInt(reader.getText()));
                            break;
                        case STATIC:
                            tariff.setStaticCallPrice(Integer.parseInt(reader.getText()));
                            break;
                        case SMS_PRICE:
                            tariff.setSmsPrice(Integer.parseInt(reader.getText()));
                            break;
                        case FAVOURITE_NUMBERS:
                            tariff.setFavouriteNumbers(Integer.parseInt(reader.getText()));
                            break;
                        case BILLING:
                            tariff.setBilling(Tariff.Billing.valueOf(reader.getText().toUpperCase()));
                            break;
                        case TARIFF_SELECT_PRICE:
                            tariff.setTariffSelectPrice(Integer.parseInt(reader.getText()));
                            break;
                        case FREE_MINUTES_NUMBER:
                            ((EconomicTariff) tariff).setFreeMinutesNumber(Integer.parseInt(reader.getText()));
                            break;
                        case TRAFFIC_QUANTITY:
                            ((InternetTariff) tariff).setTrafficQuantity(Integer.parseInt(reader.getText()));
                            break;
                        case DISCOUNT:
                            ((SocialTariff) tariff).setDiscount(Float.parseFloat(reader.getText()));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    TariffElementsEnum elementType2 = TariffElementsEnum.valueOf(name.replace('-', '_').toUpperCase());
                    switch (elementType2)
                    {
                        case SOCIAL_TARIFF:
                        case ECONOMIC_TARIFF:
                        case INTERNET_TARIFF:
                            return tariff;
                    }
            }
        }
        throw new TechnicalException("wrong xml structure");
    }
}
