package by.vozhdaenko.tariffs.builder;

/**
 * Elements name enumeration.
 */
public enum TariffElementsEnum
{
    TARIFFS("tariffs"),
    TARIFF("tariff"),
    SOCIAL_TARIFF("social-tariff"),
    ECONOMIC_TARIFF("economic-tariff"),
    INTERNET_TARIFF("internet-tariff"),
    NAME("name"),
    OPERATOR("operator"),
    PAYROLL("payroll"),
    INNER("inner"),
    OUTER("outer"),
    STATIC("static"),
    SMS_PRICE("sms-price"),
    FAVOURITE_NUMBERS("favourite-numbers"),
    BILLING("billing"),
    TARIFF_SELECT_PRICE("tariff-select-price"),
    FREE_MINUTES_NUMBER("free-minutes-number"),
    TRAFFIC_QUANTITY("traffic-quantity"),
    DISCOUNT("discount"),
    CALL_PRICES("call-prices"),
    PARAMETERS("parameters"),
    ID("id");

    private String value;

    private TariffElementsEnum(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }
}
