package by.vozhdaenko.tariffs.entity;

import by.vozhdaenko.tariffs.exception.LogicException;
import by.vozhdaenko.tariffs.validator.TariffValidator;

/**
 * Social tariff class
 */
public class SocialTariff extends Tariff
{
    private double discount;

    /**
     * Default constructor.
     */
    public SocialTariff()
    {
    }

    /**
     * Creates new social tariff object.
     * Its recommended to validate data before creating.
     *
     * @param id                id of the tariff
     * @param name              name of the tariff
     * @param operator          name of the operator
     * @param payroll           payroll of the tariff in money values
     * @param innerCallsPrice   inner call price
     * @param outCallsPrice     outer call price
     * @param staticCallPrice   static call price
     * @param smsPrice          SMS_PRICE price
     * @param favouriteNumbers  number of favourite phone numbers
     * @param billing           billing of the tariff
     * @param tariffSelectPrice money value to pay for tariff connection
     * @param discount          discount value
     */
    public SocialTariff(int id, String name, Operator operator, int payroll, int innerCallsPrice, int outCallsPrice,
                        int staticCallPrice, int smsPrice, int favouriteNumbers, Tariff.Billing billing,
                        int tariffSelectPrice, double discount) throws LogicException
    {
        super(id, name, operator, payroll, innerCallsPrice, outCallsPrice,
                staticCallPrice, smsPrice, favouriteNumbers, billing, tariffSelectPrice);
        setDiscount(discount);
    }

    public void setDiscount(double discount) throws LogicException
    {
        TariffValidator.validateDiscount(discount);
        this.discount = discount;
    }

    /**
     * @return {@code string} representation of {@link by.vozhdaenko.tariffs.entity.SocialTariff} entity.
     */
    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("SocialTariff{");
        sb.append(super.toString()).append("discount=");
        sb.append(discount).append('}');
        return sb.toString();
    }
}
