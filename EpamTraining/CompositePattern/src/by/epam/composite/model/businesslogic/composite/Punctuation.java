package by.epam.composite.model.businesslogic.composite;


import java.util.ArrayList;

public class Punctuation implements Component{

    private String symbol;

    public Punctuation(String symbol) {
        this.symbol = symbol;
    }
    public Punctuation(){}

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }


    @Override
    public void add(Component component) {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public void remove(Component component) {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public ArrayList<Component> getComponent() {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public String toString() {
        return symbol;
    }
}
