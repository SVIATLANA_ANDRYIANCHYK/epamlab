package by.epam.composite.model.businesslogic.composite;


import java.util.ArrayList;

public class Composite implements Component {

    private ArrayList<Component> components = new ArrayList<Component>();
    @Override
    public void add(Component component) {
        components.add(component);
    }

    @Override
    public void remove(Component component) {
        components.add(component);
    }

    @Override
    public ArrayList<Component> getComponent() {
        return components;
    }

    @Override
    public String toString() {
        String str = "";
        for(int i = 0; i < components.size(); i++){
            str.join(components.get(i).toString());
        }
        return str;
    }
}
