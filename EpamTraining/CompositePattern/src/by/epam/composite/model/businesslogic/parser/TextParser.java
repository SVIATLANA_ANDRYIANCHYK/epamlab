package by.epam.composite.model.businesslogic.parser;


import by.epam.composite.model.businesslogic.composite.Composite;
import by.epam.composite.model.businesslogic.composite.Punctuation;
import by.epam.composite.model.businesslogic.composite.Word;

import java.io.*;


public class TextParser {


    public static String REGEX_WORD = "([!-@]| |\\W|\\s){1,}";
    public static String REGEX_PARAGRAPH = "[.;!?]+\\n";
    public static String REGEX_SENTENCE = "[.!?]+\\s";
    public static String REGEX_SYMBOLS = "[A-Za-z]{1,}";



    public String initialization(final String path) {
        String text = "";
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
            text = bufferedReader.readLine() + "\n";
            while(bufferedReader.ready())
                text+=bufferedReader.readLine() + "\n";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }


    public Composite parseToParagraphs(Composite wholeText, String text){

        String[] parsedText = text.split(REGEX_PARAGRAPH);

        for(int i = 0; i < parsedText.length; i++){
            Word word = new Word(parsedText[i]);
            wholeText.add(word);
        }
        return wholeText;
    }

    public Composite parseToSentence(Composite paragraphs) {
        Composite sentences = new Composite();
        int paragraphCount = paragraphs.getComponent().size();

        for(int i = 0; i < paragraphCount; i++){
            String[] parsedParagraph = paragraphs.getComponent().get(i).toString().split(REGEX_SENTENCE);
            for(int j = 0; j < parsedParagraph.length; j++){
                Word word = new Word(parsedParagraph[j]+".");
                sentences.add(word);
            }
        }
        return sentences;
    }

    public Composite parseToWords(Composite sentences) {
        Composite words = new Composite();
        int sentencesCount = sentences.getComponent().size();
        for(int i = 0; i < sentencesCount; i++){
            String[] parsedSentence = sentences.getComponent().get(i).toString().split(REGEX_WORD);
            for(int j = 0; j < parsedSentence.length; j ++){
                Word word = new Word(parsedSentence[j]);
                words.add(word);
            }
        }
        return words;
    }


    public Composite parseToOtherSymbols(Composite sentences) {
        Composite symbols = new Composite();
        int sentencesCount = sentences.getComponent().size();
        for(int i = 0; i < sentencesCount; i++){
            String[] parsedSentence = sentences.getComponent().get(i).toString().split(REGEX_SYMBOLS);
            for(int j = 0; j < parsedSentence.length; j ++){
                Punctuation punctuation = new Punctuation(parsedSentence[j]);
                symbols.add(punctuation);
            }
        }
        return symbols;
    }

    public Composite collectWordsAndSymbols(Composite words, Composite symbols){
        Composite wordsAndSymbols = new Composite();
        for(int i = 0; i < words.getComponent().size(); i++){
            wordsAndSymbols.add(words.getComponent().get(i));
            wordsAndSymbols.add(symbols.getComponent().get(i+1));
        }
        return wordsAndSymbols;
    }



}
