package by.epam.composite.model.businesslogic.composite;


import java.util.ArrayList;

public interface Component {

    public void add(Component component);
    public void remove(Component component);
    public ArrayList<Component> getComponent();

}
