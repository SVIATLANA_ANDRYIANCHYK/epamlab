package by.epam.composite.model.businesslogic.composite;


import java.util.ArrayList;

public class Word implements Component {

    /**
     * Leaf
     */

    private String word;

    public Word(String word) {
        this.word = word;
    }
    public Word(){}

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }




    @Override
    public void add(Component component) {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public void remove(Component component) {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public ArrayList<Component> getComponent() {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public String toString() {
        return word;
    }

}
