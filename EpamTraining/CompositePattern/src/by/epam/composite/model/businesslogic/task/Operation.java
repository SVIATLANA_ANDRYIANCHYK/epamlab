package by.epam.composite.model.businesslogic.task;


import by.epam.composite.model.businesslogic.composite.Component;
import by.epam.composite.model.businesslogic.composite.Composite;
import by.epam.composite.model.businesslogic.composite.Word;
import by.epam.composite.model.businesslogic.parser.TextParser;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Operation {


    public void printSentencesOrderedByWordsNumb(Composite sentences){
        TextParser textParser = new TextParser();
        int[] wordsCountArray = new int[sentences.getComponent().size()];
        for(int i = 0; i < sentences.getComponent().size(); i++){
            Composite parsedSentence = new Composite();
            parsedSentence.add(sentences.getComponent().get(i));
            Composite words = textParser.parseToWords(parsedSentence);
            int sentenceWordsCount = words.getComponent().size();
            wordsCountArray[i] = sentenceWordsCount;
        }

        for(int i = 0; i < wordsCountArray.length; i++){
            for(int j = 0; j < wordsCountArray.length-1; j++){
                if(wordsCountArray[j] < wordsCountArray[j+1]){
                    int temp = wordsCountArray[j];
                    wordsCountArray[j] = wordsCountArray[j+1];
                    wordsCountArray[j+1] = temp;
                    Component tempStr = sentences.getComponent().get(j);
                    sentences.getComponent().set(j, sentences.getComponent().get(j + 1));
                    sentences.getComponent().set((j+1), tempStr);
                }
            }
        }

        for(int i = 0; i < sentences.getComponent().size(); i++){
            System.out.println(sentences.getComponent().get(i).toString());
        }
    }

    public void sortWordsOrderedByAlphabet(Composite words) {
        String[] wordsArray = new String[words.getComponent().size()];
        for(int i = 0; i < words.getComponent().size(); i++){
            wordsArray[i] = words.getComponent().get(i).toString();
        }
        Arrays.sort(wordsArray);
        for(int i = 0; i < wordsArray.length-1; i++){
            System.out.print(wordsArray[i] + " ");
            if(wordsArray[i].charAt(0)!= wordsArray[i+1].charAt(0)){
                System.out.println("\n");
            }
        }

    }
}
