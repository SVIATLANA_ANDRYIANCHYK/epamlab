package by.epam.composite.controller;


import by.epam.composite.model.businesslogic.composite.Composite;
import by.epam.composite.model.businesslogic.parser.TextParser;
import by.epam.composite.model.businesslogic.task.Operation;

public class Run {
    public static void main(String[] args) {
        String path = "text/book.txt";
        TextParser textParser = new TextParser();
        String text = textParser.initialization(path);


        Composite wholeText = new Composite();
        wholeText = textParser.parseToParagraphs(wholeText, text);

        Composite sentences = textParser.parseToSentence(wholeText);

        Composite words = textParser.parseToWords(sentences);

        Composite symbols = textParser.parseToOtherSymbols(wholeText);


        Composite wordsAndSymbols = textParser.collectWordsAndSymbols(words, symbols);

        Operation operation = new Operation();
        operation.printSentencesOrderedByWordsNumb(sentences);
        operation.sortWordsOrderedByAlphabet(words);
        //System.out.println(symbols.getComponent().toString());
       // System.out.println(wordsAndSymbols.getComponent().toString());
//        Operation operation = new Operation();
//        operation.printSentencesOrderedByWordsNumb(sentences);

        //System.out.println(words.getComponent().toString());
       // System.out.println(sentences.getComponent().toString());

       // System.out.println(wholeText.getComponent().get(0).toString());



    }
}
