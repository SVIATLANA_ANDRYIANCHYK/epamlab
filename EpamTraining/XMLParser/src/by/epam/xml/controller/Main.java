package by.epam.xml.controller;


import by.epam.xml.model.businesslogic.builder.AbstractPlanesBuilder;
import by.epam.xml.model.businesslogic.builder.PlaneBuilderFactory;

public class Main {

    public static void main(String[] args) {
        PlaneBuilderFactory pFactory = new PlaneBuilderFactory();
        AbstractPlanesBuilder builder = pFactory.createPlaneBuilder("stax");
        builder.buildSetPlanes("data/plane.xml");
        System.out.println(builder.getPlanes().toString());
    }

}
