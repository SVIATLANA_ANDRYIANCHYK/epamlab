package com.epam.xsltmanagement.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/")
public class ProductsController {

	@RequestMapping(value = { "products" }, method = RequestMethod.GET)
	public ModelAndView showGoods(HttpServletRequest request, HttpServletResponse response, HttpSession session) {

		// String xmlFile = "/resources/xml/products.xml";
		// String contextPath = session.getServletContext().getRealPath("");
		// String xmlFilePath = contextPath + File.separator + xmlFile;

		Source source = new StreamSource(new File("/Eclipse Workspace/Projects/XSLTManagement/src/main/resources/xml/products.xml"));

		ModelAndView model = new ModelAndView("goods");
		model.addObject("xmlSource", source);
		return model;

	}

	@RequestMapping(value = { "subcategory" }, method = RequestMethod.GET)
	public ModelAndView showSubcategories(HttpServletRequest request, HttpServletResponse response, HttpSession session) {

		// String xmlFile = "/resources/xml/products.xml";
		// String contextPath = session.getServletContext().getRealPath("");
		// String xmlFilePath = contextPath + File.separator + xmlFile;

		Source source = new StreamSource(new File("/Eclipse Workspace/Projects/XSLTManagement/src/main/resources/xml/products.xml"));

		ModelAndView model = new ModelAndView("goods");
		model.addObject("xmlSource", source);
		return model;

	}

}
