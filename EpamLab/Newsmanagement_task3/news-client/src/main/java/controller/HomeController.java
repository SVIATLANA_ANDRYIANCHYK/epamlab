package controller;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import util.PageBuilder;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.ICommentService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.ServiceException;

@Controller
@RequestMapping(value = "/")
public class HomeController {

	private IAuthorService authorService;
	private INewsService newsService;
	private ITagService tagService;
	private ICommentService commentService;

	@Inject
	public HomeController(INewsService newsService, IAuthorService authorService, ITagService tagService, ICommentService commentService) {
		this.newsService = newsService;
		this.authorService = authorService;
		this.tagService = tagService;
		this.commentService = commentService;
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(@RequestParam(value = "page", defaultValue = "1") int currentPage, Model model, HttpSession session) throws ServiceException {

		PageBuilder pageBuilder = new PageBuilder();
		SearchCriteria sc = new SearchCriteria();
		sc = (SearchCriteria) session.getAttribute("searchCriteria");
		int[] pagesNumbersArray = pageBuilder.getNewsPagesArray(sc, newsService);
		model.addAttribute("pages", pagesNumbersArray);
		int newsOnPageCount = pageBuilder.getNewsOnPageCount();
		List<News> newsList = newsService.searchBySearchCriteriaAndPagination(sc, currentPage, newsOnPageCount);
		for (int i = 0; i < newsList.size(); i++) {
			newsList.get(i).setCommentsList(commentService.getListOfCommentsByNewsId(newsList.get(i).getNewsId()));
		}
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("newsList", newsList);

		List<Author> authorList = null;
		authorList = authorService.getAll();
		model.addAttribute("authors", authorList);

		List<Tag> tagList = null;
		tagList = tagService.getAll();
		model.addAttribute("tags", tagList);

		return "home";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String sendToNewsList() {
		return "redirect:/home";
	}

	@RequestMapping(value = { "/filter" }, method = RequestMethod.POST)
	public String filterNews(HttpServletRequest request, @RequestParam(value = "tagId[]", required = false) Long[] tagIdArray, @RequestParam(value = "author", required = false) Long authorId)
			throws ServiceException {

		List<Long> tagList = new ArrayList<Long>();
		if (tagIdArray != null) {
			for (int i = 0; i < tagIdArray.length; i++) {
				tagList.add(tagIdArray[i]);
			}
		}
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagList(tagList);
		request.getSession().setAttribute("searchCriteria", searchCriteria);
		return "redirect:/home";
	}

	@RequestMapping(value = { "/reset" }, method = RequestMethod.POST)
	public String reset(HttpSession session) {
		session.setAttribute("searchCriteria", null);
		return "redirect:/home";
	}

}
