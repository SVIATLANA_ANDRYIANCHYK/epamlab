package controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.OptimisticLockException;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagment.dao.StaleVersionException;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.ServiceException;

@Controller
public class AddNewsController {

	private ITagService tagService;
	private IAuthorService authorService;
	private INewsService newsService;
	@Autowired
	private NewsValidator validator;

	@Inject
	public AddNewsController(INewsService newsService, ITagService tagService, IAuthorService authorService) {
		this.newsService = newsService;
		this.tagService = tagService;
		this.authorService = authorService;
	}

	@RequestMapping(value = { "/saveNews" }, method = RequestMethod.POST)
	public String saveNews(@ModelAttribute("news") News news, @RequestParam(value = "tagId[]", required = false) Long[] tagIdArray, BindingResult result, Model model) throws ServiceException,
			StaleVersionException {
		validator.validate(news, result);
		if (!result.hasErrors()) {
			Long newsId = news.getNewsId();
			Date today = new Date();
			news.setModificationDate(today);
			List<Tag> tagList = new ArrayList<Tag>();
			if (tagIdArray != null) {
				for (int i = 0; i < tagIdArray.length; i++) {
					Tag tag = tagService.getSingleTagById(tagIdArray[i]);
					tagList.add(tag);
				}
			}
			Set<Tag> tagSet = new HashSet<Tag>(tagList);
			news.setTagSet(tagSet);
			Long authorId = news.getAuthor().getAuthorId();
			news.setAuthor(authorService.getSingleAuthorById(authorId));
			if (newsId == null) {
				newsId = newsService.create(news);
			} else {
				try {
					newsService.update(news);
				} catch (OptimisticLockException | StaleObjectStateException ex) {
					List<Tag> listTag = tagService.getAll();
					model.addAttribute("tags", listTag);

					List<Author> authorList = authorService.getAll();
					model.addAttribute("authors", authorList);
					model.addAttribute("news", news);
					model.addAttribute("error", "error.lock");
					return "addNews";
				}

			}

			return "redirect:/singleNews/" + newsId;
		} else {
			List<Tag> listTag = tagService.getAll();
			model.addAttribute("tags", listTag);

			List<Author> authorList = authorService.getAll();
			model.addAttribute("authors", authorList);
			model.addAttribute("news", news);
			return "addNews";
		}

	}

	@RequestMapping(value = { "/saveNews" }, method = RequestMethod.GET)
	public String language(Model model) throws ServiceException {
		return "redirect:/addNews";
	}

	@ExceptionHandler(ServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

}
