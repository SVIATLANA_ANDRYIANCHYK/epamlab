package controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagment.dao.StaleVersionException;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.ServiceException;

@Controller
public class AuthorController {

	private IAuthorService authorService;

	@Inject
	public AuthorController(IAuthorService authorService) {
		this.authorService = authorService;
	}

	@RequestMapping(value = { "/editAuthor/{authorId}" }, method = RequestMethod.GET)
	public String editAuthor(@PathVariable("authorId") Long authorId, Model model) throws ServiceException {
		Author authorForUpdate = authorService.getSingleAuthorById(authorId);
		model.addAttribute("authorForUpdate", authorForUpdate);
		List<Author> authorList = null;
		authorList = authorService.getAll();
		model.addAttribute("authors", authorList);
		Author author = new Author();
		model.addAttribute("authorToAdd", author);
		return "authors";
	}

	@RequestMapping(value = { "/deleteAuthor" }, method = RequestMethod.GET)
	public String deleteAuthor(Long authorId) throws ServiceException {
		authorService.delete(authorId);
		return "redirect:/authors";
	}

	@RequestMapping(value = { "/updateAuthor" }, method = RequestMethod.POST)
	public ModelAndView updateAuthor(@Valid @ModelAttribute("authorForUpdate") Author author, BindingResult result) throws ServiceException, StaleVersionException {
		if (!result.hasErrors()) {
			authorService.update(author);
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("redirect:/authors");
			return modelAndView;

		} else {
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("authors");
			return formModelUpdate();

		}

	}

	@RequestMapping(value = { "/updateAuthor" }, method = RequestMethod.GET)
	public String languageUpdate() throws ServiceException {
		return "redirect:/authors";
	}

	@RequestMapping(value = { "/addAuthor" }, method = RequestMethod.POST)
	public ModelAndView addAuthor(@Valid @ModelAttribute("authorToAdd") Author author, BindingResult result) throws ServiceException {

		if (!result.hasErrors()) {
			authorService.create(author);
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("redirect:/authors");
			return modelAndView;
		} else {

			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("authors");
			return formModel();
		}
	}

	@RequestMapping(value = { "/addAuthor" }, method = RequestMethod.GET)
	public String languageAdd() throws ServiceException {
		return "redirect:/authors";
	}

	@ExceptionHandler(ServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

	private ModelAndView formModel() throws ServiceException {
		ModelAndView model = new ModelAndView();
		List<Author> authorList = null;
		authorList = authorService.getAll();
		model.addObject("authors", authorList);

		model.setViewName("authors");

		return model;
	}

	private ModelAndView formModelUpdate() throws ServiceException {
		ModelAndView model = new ModelAndView();
		List<Author> authorList = null;
		authorList = authorService.getAll();
		model.addObject("authors", authorList);
		Author author = new Author();
		model.addObject("authorToAdd", author);
		model.setViewName("authors");

		return model;
	}

}
