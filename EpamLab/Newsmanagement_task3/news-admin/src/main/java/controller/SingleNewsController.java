package controller;

import java.util.Date;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import util.Navigator;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.service.ICommentService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ServiceException;

@Controller
public class SingleNewsController {

	private ICommentService commentService;
	private INewsService newsService;

	@Inject
	public SingleNewsController(INewsService newsService, ICommentService commentService) {
		this.commentService = commentService;
		this.newsService = newsService;
	}

	@RequestMapping(value = { "/postComment" }, method = RequestMethod.POST)
	public String postComment(@RequestParam(value = "newsId") Long newsId, @RequestParam(value = "commentText") String commentText) throws ServiceException {
		String text = commentText.replaceAll("\\s+", "");
		if (text.length() > 1 && commentText != null && commentText.length() < 100) {
			Comment comment = new Comment();
			Date today = new Date();
			comment.setCreationDate(today);
			comment.setCommentText(commentText);
			News news = newsService.getSingleNewsById(newsId);
			comment.setNews(news);

			commentService.create(comment);

		}
		return "redirect:/singleNews/" + newsId;
	}

	@RequestMapping(value = { "/deleteComment" }, method = RequestMethod.POST)
	public String deleteComment(@RequestParam(value = "newsId") Long newsId, @RequestParam(value = "commentId") Long commentId) throws ServiceException {
		commentService.delete(commentId);
		return "redirect:/singleNews/" + newsId;
	}

	@RequestMapping(value = { "/next" }, method = RequestMethod.GET)
	public String showNextNews(Long newsId, HttpSession session) throws ServiceException {
		Navigator navigator = new Navigator();
		SearchCriteria sc = (SearchCriteria) session.getAttribute("searchCtiteria");
		int newsCount = newsService.getNewsCountBySearchCriteria(sc).intValue();
		News nextNews = navigator.getNextNews(newsService, sc, 1, newsCount, newsId);
		Long nextNewsId = nextNews.getNewsId();
		return "redirect:/singleNews/" + nextNewsId;
	}

	@RequestMapping(value = { "/previous" }, method = RequestMethod.GET)
	public String showPreviousNews(Long newsId, HttpSession session) throws ServiceException {
		Navigator navigator = new Navigator();
		SearchCriteria sc = (SearchCriteria) session.getAttribute("searchCtiteria");
		int newsCount = newsService.getNewsCountBySearchCriteria(sc).intValue();
		News previousNews = navigator.getPreviousNews(newsService, sc, 1, newsCount, newsId);
		Long previousNewsId = previousNews.getNewsId();
		return "redirect:/singleNews/" + previousNewsId;
	}

	@ExceptionHandler(ServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

}
