package util;

import java.util.List;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ServiceException;
import com.epam.newsmanagment.service.VO.NewsVO;
import com.epam.newsmanagment.service.implementation.ServiceManager;

public class Navigator {

	public NewsVO getNextNewsVO(ServiceManager serviceManager, SearchCriteria sc, Long newsId, int newsCount) throws ServiceException {
		List<NewsVO> newsVOList = serviceManager.getAllNews(sc, 0, newsCount);

		NewsVO newsVO = null;
		int size = newsVOList.size();
		int nextNewsIndex = 0;
		for (int i = 0; i < size; i++) {
			Long currentNewsId = newsVOList.get(i).getNews().getNewsId();
			if (currentNewsId.equals(newsId)) {
				nextNewsIndex = i + 1;
				break;
			}
		}

		if (nextNewsIndex >= size) {
			newsVO = newsVOList.get(0);
		} else {
			newsVO = newsVOList.get(nextNewsIndex);
		}

		return newsVO;
	}

	public NewsVO getPreviousNewsVO(ServiceManager serviceManager, SearchCriteria sc, Long newsId, int newsCount) throws ServiceException {
		List<NewsVO> newsVOList = serviceManager.getAllNews(sc, 0, newsCount);

		NewsVO newsVO = null;
		int size = newsVOList.size();
		int previousNewsIndex = 0;
		for (int i = 0; i < size; i++) {
			Long currentNewsId = newsVOList.get(i).getNews().getNewsId();
			if (currentNewsId.equals(newsId)) {
				previousNewsIndex = i - 1;
				break;
			}
		}

		if (previousNewsIndex < 0) {
			newsVO = newsVOList.get(size - 1);
		} else {
			newsVO = newsVOList.get(previousNewsIndex);
		}

		return newsVO;
	}

	public News getNextNews(INewsService newsService, SearchCriteria sc, int first, int size, Long newsId) throws ServiceException {
		List<News> newsList = newsService.searchBySearchCriteriaAndPagination(sc, first, size);
		News news = null;

		int listSize = newsList.size();
		int nextNewsIndex = 0;
		for (int i = 0; i < listSize; i++) {
			Long currentNewsId = newsList.get(i).getNewsId();
			if (currentNewsId.equals(newsId)) {
				nextNewsIndex = i + 1;
				break;
			}
		}

		if (nextNewsIndex >= listSize) {
			news = newsList.get(0);
		} else {
			news = newsList.get(nextNewsIndex);
		}
		return news;
	}

	public News getPreviousNews(INewsService newsService, SearchCriteria sc, int first, int size, Long newsId) throws ServiceException {
		List<News> newsList = newsService.searchBySearchCriteriaAndPagination(sc, first, size);
		News news = null;

		int listSize = newsList.size();
		int previousNewsIndex = 0;
		for (int i = 0; i < listSize; i++) {
			Long currentNewsId = newsList.get(i).getNewsId();
			if (currentNewsId.equals(newsId)) {
				previousNewsIndex = i - 1;
				break;
			}
		}

		if (previousNewsIndex < 0) {
			news = newsList.get(listSize - 1);
		} else {
			news = newsList.get(previousNewsIndex);
		}
		return news;
	}
}
