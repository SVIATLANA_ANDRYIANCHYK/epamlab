<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="<c:url value="/resources/css/addNews.css" />"
	rel="stylesheet">




<tiles:insertDefinition name="adminTemplate">
	<tiles:putAttribute name="body">
		<sf:form method="POST" modelAttribute="news"
			action="/saveNews">
			<div class="add-content">
				<div class = "error-lock">
					<c:if test="${not empty error}">
						<spring:message code="${error}" />
					</c:if>
				</div>
				<div class="field">
					<div>
						<sf:errors path="title" cssClass="error-add" />
					</div>
					<label for="title"><spring:message code="add-news.title" /></label>
					<sf:input type="text" id="title" class="data" path="title" />

				</div>
				<div class="field">
					<label for="date"><spring:message code="add-news.date" /></label>
					<fmt:message key="date.format" var="format" />
					<fmt:formatDate value="${news.creationDate}" pattern="${format}"
						var="formattedDate" />
					<sf:input type="text" value="${formattedDate}" path="creationDate"
						class="data" readonly="true" />
				</div>
				<div class="field">
					<div>
						<sf:errors path="shortText" cssClass="error-add" />
					</div>
					<label for="brief"><spring:message code="add-news.brief" /></label>
					<sf:textarea id="brief" class="data" path="shortText"></sf:textarea>

				</div>
				<div class="field">
					<div>
						<sf:errors path="fullText" cssClass="error-add" />
					</div>
					<label for="content"><spring:message
							code="add-news.content" /></label>
					<sf:textarea id="content" class="data" path="fullText"></sf:textarea>
				</div>
				<sf:input type="hidden" path="newsId" />
				<sf:input type="hidden" path="version" />

			</div>


			<div class="selects">
				<sf:select path="author.authorId">
					<c:forEach items="${authors}" var="authors">
						<option ${author.authorId == authors.authorId ? 'selected' : '' }
							value="${authors.authorId}"><c:out
								value="${authors.authorName}" /></option>
					</c:forEach>

				</sf:select>
				<select onclick="showCheckboxes()">

				</select>
				<div id="checkboxes" style="display: none">


					<c:forEach items="${tags}" var="tag">
						<div class="checkbox">

							<input ${tagSet.contains(tag) ? 'checked' : '' } type="checkbox"
								value="${tag.tagId}" name="tagId[]" />
							<c:out value="${tag.tagName}" />

						</div>
					</c:forEach>

				</div>
			</div>
			<button type="submit">
				<spring:message code="add-news.save" />
			</button>

		</sf:form>
	</tiles:putAttribute>
</tiles:insertDefinition>
<script src="<c:url value= "/resources/js/checkboxes.js"/>"></script>