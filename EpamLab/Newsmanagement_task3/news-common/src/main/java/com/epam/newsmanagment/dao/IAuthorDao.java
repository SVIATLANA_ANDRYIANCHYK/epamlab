package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.entity.Author;

public interface IAuthorDao extends ICommonDao<Author> {
	/**
	 * 
	 * @param id
	 *            of author to get
	 * @return Author entity
	 * @throws DaoException
	 */
	public Author getSingleAuthorById(long id) throws DaoException;

}
