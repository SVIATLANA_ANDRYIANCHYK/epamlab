package com.epam.newsmanagment.service;

import java.util.List;

import com.epam.newsmanagment.entity.Comment;

public interface ICommentService extends IService<Comment> {
	/**
	 * 
	 * @param newsId
	 *            to get
	 * @return List of Comment entities
	 * @throws ServiceException
	 */
	public List<Comment> getListOfCommentsByNewsId(long newsId) throws ServiceException;

	/**
	 * Gets Comment object by id.
	 * 
	 * @param commentId
	 * @return Comment object of certain id.
	 * @throws ServiceException
	 */
	public Comment getSingleCommentById(long commentId) throws ServiceException;

	public void deleteCommentByNewsId(Long newsId) throws ServiceException;
}
