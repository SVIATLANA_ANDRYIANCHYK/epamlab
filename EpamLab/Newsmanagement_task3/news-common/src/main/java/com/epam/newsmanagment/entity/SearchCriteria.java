package com.epam.newsmanagment.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Sets parameters (criteria) of search
 * 
 * @author Sviatlana_Andryianch
 *
 * Lombok is used for constructors, equals, hasshcode, getters, setters generation
 */
@AllArgsConstructor
@NoArgsConstructor
@Data public class SearchCriteria {

	private Long authorId;
	private List<Long> tagList;

}
