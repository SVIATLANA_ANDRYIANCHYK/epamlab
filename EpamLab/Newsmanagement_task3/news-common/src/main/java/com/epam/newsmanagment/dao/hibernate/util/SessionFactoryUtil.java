package com.epam.newsmanagment.dao.hibernate.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class SessionFactoryUtil {

	public static Session getCurrentSession(SessionFactory sessionFactory) {
		return sessionFactory.getCurrentSession();
	}
}
