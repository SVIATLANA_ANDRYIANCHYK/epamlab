package com.epam.newsmanagment.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * News entity
 *
 * @author Sviatlana_Andryianch
 * Lombok is used for constructors, equals, hasshcode, getters, setters generation
 *
 */

@Entity
@Table(name = "News")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude={"commentsList", "tagSet", "author"})
@lombok.ToString(exclude = {"commentsList", "tagSet", "author"})
@lombok.Getter
@lombok.Setter
@Data public final class News implements Serializable, IEntity {

	private static final long serialVersionUID = 3773281197317274020L;

	@Id
	@SequenceGenerator(name = "NEWS_SEQ_GEN", sequenceName = "NEWS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEWS_SEQ_GEN")
	@Column(name = "NEWS_ID", precision = 0)
	private Long newsId; // Primary key

	@Column(name = "TITLE")
	private String title;

	@Column(name = "SHORT_TEXT")
	private String shortText;

	@Column(name = "FULL_TEXT")
	private String fullText;

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFICATION_DATE")
	private Date modificationDate;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "news")
	private List<Comment> commentsList;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "NEWS_TAG", joinColumns = { @JoinColumn(name = "NEWS_ID") }, inverseJoinColumns = { @JoinColumn(name = "TAG_ID") })
	private Set<Tag> tagSet;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinTable(name = "NEWS_AUTHOR", joinColumns = { @JoinColumn(name = "NEWS_ID") }, inverseJoinColumns = { @JoinColumn(name = "AUTHOR_ID") })
	private Author author;

	@Version
	private Long version;

}
