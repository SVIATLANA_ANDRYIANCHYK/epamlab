package com.epam.newsmanagment.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *Lombok is used for constructors, equals, hasshcode, getters, setters generation
 */
@SequenceGenerator(name = "TAG_SEQ", sequenceName = "TAG_SEQ")
@Entity
@Table(name = "Tag")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude={"newsSet"})
@lombok.ToString(exclude = "newsSet")
@lombok.Getter
@lombok.Setter
public class Tag implements Serializable, IEntity {
	private static final long serialVersionUID = -4741660615976012459L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAG_SEQ")
	@Column(name = "TAG_ID", precision = 0)
	private Long tagId; // Primary key
	@NotEmpty
	@NotNull
	@Size(max = 15)
	@Column(name = "TAG_NAME")
	private String tagName;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "NEWS_TAG", joinColumns = { @JoinColumn(name = "TAG_ID") }, inverseJoinColumns = { @JoinColumn(name = "NEWS_ID") })
	private Set<News> newsSet;

//	public Set<News> getNewsSet() {
//		return newsSet;
//	}
//
//	public void setNewsSet(Set<News> newsSet) {
//		this.newsSet = newsSet;
//	}
//
//	public Tag() {
//	}
//
//	public Tag(Long tagId, String tagName) {
//		super();
//		this.tagId = tagId;
//		this.tagName = tagName;
//	}
//
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
//		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Tag other = (Tag) obj;
//		if (tagId == null) {
//			if (other.tagId != null)
//				return false;
//		} else if (!tagId.equals(other.tagId))
//			return false;
//		if (tagName == null) {
//			if (other.tagName != null)
//				return false;
//		} else if (!tagName.equals(other.tagName))
//			return false;
//		return true;
//	}
//
//	@Override
//	public String toString() {
//		return "Tag [tagId=" + tagId + ", tagName=" + tagName + "]";
//	}
//
//
//	//////////////////////
//	// Getters and setters
//	/////////////////////
//
//	public Long getTagId() {
//		return tagId;
//	}
//
//	public void setTagId(Long tagId) {
//		this.tagId = tagId;
//	}
//
//	public String getTagName() {
//		return tagName;
//	}
//
//	public void setTagName(String tagName) {
//		this.tagName = tagName;
//	}
//
//	public static long getSerialversionuid() {
//		return serialVersionUID;
//	}

}

