package com.epam.newsmanagment.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Lombok is used for constructors, equals, hasshcode, getters, setters generation
 */
@SequenceGenerator(name = "COMMENTS_SEQ", sequenceName = "COMMENTS_SEQ")
@Entity
@Table(name = "Comments")
@AllArgsConstructor
@NoArgsConstructor
@Data public class Comment implements Serializable, IEntity {


	private static final long serialVersionUID = 3431305873409011465L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMENTS_SEQ")
	@Column(name = "COMMENT_ID", precision = 0)
	private Long commentId; // Primary key

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "NEWS_ID")
	private News news;

	@NotEmpty
	@NotNull
	@Column(name = "COMMENT_TEXT")
	private String commentText;

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATION_DATE")
	private Date creationDate;


	//////////////////////
	// Getters and setters
	/////////////////////

	public Long getNewsId() {
		return news.getNewsId();
	}


	public void setNewsId(Long newsId) {
		this.news.setNewsId(newsId);
	}
}
