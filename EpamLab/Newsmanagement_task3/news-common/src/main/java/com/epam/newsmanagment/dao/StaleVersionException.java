/**
 * 
 */
package com.epam.newsmanagment.dao;

/**
 * This class id used to indicate conditions that application wants to catch.
 * This exception may be thrown when the version of object in database doesn't
 * match the version of updated object. This happens when someone else updated
 * data and version in database has been changed.
 * 
 * @author Valeryia_Kakura
 *
 */
public class StaleVersionException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new exception with {@code null} as its detail message.
	 */
	public StaleVersionException() {
		super();
	}

	/**
	 * Constructs a new exception with the specified detail message. The cause
	 * is not initialized.
	 * 
	 * @param message
	 *            the detail message
	 */
	public StaleVersionException(String message) {
		super(message);
	}

	/**
	 * Constructs a new exception with the specified cause. The message is not
	 * initialized.
	 * 
	 * @param cause
	 *            cause the cause (which is saved for later retrieval by the
	 *            {@link #getCause()} method).
	 */
	public StaleVersionException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 * 
	 * @param message
	 *            the detail message
	 * @param cause
	 *            cause the cause (which is saved for later retrieval by the
	 *            {@link #getCause()} method).
	 */
	public StaleVersionException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new exception with the specified detail message, cause,
	 * suppression enabled or disabled, and writable stack trace enabled or
	 * disabled.
	 * 
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public StaleVersionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
