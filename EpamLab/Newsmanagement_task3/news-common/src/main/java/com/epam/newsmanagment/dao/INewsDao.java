package com.epam.newsmanagment.dao;

import java.util.List;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;

public interface INewsDao extends ICommonDao<News> {

	/**
	 * 
	 * @param id
	 *            to get
	 * @return News entity
	 * @throws DaoException
	 */
	public News getSingleNewsById(long id) throws DaoException;

	/**
	 * 
	 * @param searchCriteria
	 *            for searching news in database
	 * @return List of all news appropriate to search criteria
	 * @throws DaoException
	 */
	public List<News> search(SearchCriteria searchCriteria) throws DaoException;

	/**
	 * 
	 * @return the count of news in the database.
	 * @throws DaoException
	 */
	public int getNewsCount() throws DaoException;

	/**
	 * Adds entity to the linking table News_Author
	 * 
	 * @param newsId
	 *            to add
	 * @param authorId
	 *            to add
	 * @throws DaoException
	 */
	public void linkAuthorNews(long newsId, long authorId) throws DaoException;

	/**
	 * Deletes entity from linking table News_Author
	 * 
	 * @param newsId
	 *            to delete
	 * @throws DaoException
	 */
	public void unlinkAuthorNews(long newsId) throws DaoException;

	/**
	 * Adds entity to linking table News_Tag
	 * 
	 * @param newsId
	 *            to add
	 * @param tagList
	 *            to add
	 * @throws DaoException
	 */
	public void addTagToNews(long newsId, List<Tag> tagList) throws DaoException;

	/**
	 * Deletes entity from linking table News_Tag
	 * 
	 * @param newsId
	 *            to delete
	 * @throws DaoException
	 */
	public void deleteTagFromNews(long newsId) throws DaoException;

	/**
	 * Gets author id by news id.
	 * 
	 * @param newsId
	 *            to set
	 * @return id of author
	 * @throws DaoException
	 */
	public Long getAuthorIdByNewsId(long newsId) throws DaoException;

	/**
	 * 
	 * @param newsId
	 *            to set
	 * @return list of id of tags
	 * @throws DaoException
	 */
	public List<Long> getTagIdListByNewsId(long newsId) throws DaoException;

	/**
	 * Gets list of tags by certain news id.
	 * 
	 * @param newsId
	 *            to set
	 * @return list of tags
	 * @throws DaoException
	 */
	public List<Tag> getListOfTagsByNewsId(long newsId) throws DaoException;

	public List<News> searchBySearchCriteriaAndPagination(SearchCriteria searchCriteria, int start, int end) throws DaoException;

	public void deleteTagFromNewsByNewsId(long newsId) throws DaoException;

	public Long getNewsCountBySearchCriteria(SearchCriteria searchCriteria) throws DaoException;
}
