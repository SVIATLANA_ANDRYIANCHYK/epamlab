package com.epam.newsmanagment.service;

import java.util.List;

import com.epam.newsmanagment.dao.StaleVersionException;

public interface IService<T> {
	/**
	 * Adds entity to the database.
	 * 
	 * @param entity
	 *            entity for adding to a database.
	 */
	public long create(T entity) throws ServiceException;

	/**
	 * 
	 * @param entity
	 *            adding to db
	 * @return true if operation ended successfully, false - if not
	 * @throws ServiceException
	 * @throws StaleVersionException
	 */
	public void update(T entity) throws ServiceException, StaleVersionException;

	/**
	 * 
	 * @param deliting
	 *            entity
	 * @return true if operation ended successfully, false - if not
	 * @throws ServiceException
	 */
	public void delete(long id) throws ServiceException;

	/**
	 * Shows all items in the database
	 * 
	 * @return list of items
	 * @throws ServiceException
	 */
	List<T> getAll() throws ServiceException;
}
