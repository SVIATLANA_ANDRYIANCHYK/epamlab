package com.epam.newsmanagment.dao.eclipselink;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.ICommentDao;
import com.epam.newsmanagment.entity.Comment;

@Transactional
@Repository
public class CommentDao implements ICommentDao {

	@PersistenceContext
	private EntityManager entityManager;

	private static final String SELECT_ALL_COMMENTS_QUERY = "select C from Comment C";
	private static final String SELECT_COMMENTS_BY_NEWS_ID = "select  * from Comments where NEWS_ID = ";

	@Override
	public Long create(Comment entity) {
		entityManager.persist(entity);
		entityManager.flush();
		return entity.getCommentId();
	}

	@Override
	public void update(Comment entity) {
		entityManager.merge(entity);
	}

	@Override
	public void delete(long id) {
		Comment comment = entityManager.find(Comment.class, id);
		entityManager.remove(comment);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> getAll() {
		return entityManager.createQuery(SELECT_ALL_COMMENTS_QUERY).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> getListOfCommentsByNewsId(long newsId) {
		String sql = SELECT_COMMENTS_BY_NEWS_ID;
		sql += newsId;
		List<Comment> commentsList = entityManager.createNativeQuery(sql, Comment.class).getResultList();
		return commentsList;
	}

	@Override
	public Comment getSingleCommentById(long commentId) {
		return entityManager.find(Comment.class, commentId);
	}

	/////////////////////////////
	//  Unsupported methods
	////////////////////////////

	@Override
	public void deleteCommentByNewsId(Long newsId) {
		throw new UnsupportedOperationException();
	}

}
