/**
 * 
 */
package com.epam.newsmanagment.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * @author Sviatlana_Andryianch
 * Lombok is used for constructors, equals, hasshcode, getters, setters generation
 */

@SequenceGenerator(name = "AUTHOR_SEQ", sequenceName = "AUTHOR_SEQ")
@Entity
@Table(name = "Author")
@AllArgsConstructor
@NoArgsConstructor
@Data public class Author implements Serializable, IEntity {

	private static final long serialVersionUID = -7134704224666183409L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTHOR_SEQ")
	@Column(name = "AUTHOR_ID", precision = 0)
	private Long authorId; // Primary key

	@NotEmpty
	@NotNull
	@Size(max = 30)
	@Column(name = "AUTHOR_NAME")
	private String authorName;

	@Column(name = "EXPIRED")
	@Temporal(TemporalType.DATE)
	private Date expired;



}
