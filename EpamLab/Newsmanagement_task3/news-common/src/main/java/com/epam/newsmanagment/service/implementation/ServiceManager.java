package com.epam.newsmanagment.service.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.StaleVersionException;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.ICommentService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.ServiceException;
import com.epam.newsmanagment.service.VO.NewsVO;

@Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
public class ServiceManager {

	private INewsService newsService;
	private IAuthorService authorService;
	private ICommentService commentService;
	private ITagService tagService;

	/**
	 * @return the tagService
	 */
	public ITagService getTagService() {
		return tagService;
	}

	/**
	 * @param tagService
	 *            the tagService to set
	 */
	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}

	/**
	 * Adds News value object to database.
	 * 
	 * @param newsVO
	 *            to add
	 * @return id if added news
	 * @throws ServiceException
	 */
	public long addNews(NewsVO newsVO) throws ServiceException {
		Long authorId = newsVO.getAuthor().getAuthorId();
		Long newsId = newsService.create(newsVO.getNews());
		if (authorId != null) {
			newsService.addToNewsAuthor(newsId, authorId);
		}
		if (newsVO.getListTag() != null) {
			newsService.addToNewsTag(newsId, newsVO.getListTag());
		}
		return newsId;
	}

	/**
	 * Deletes News value object from database.
	 * 
	 * @param newsId
	 *            to delete
	 * @throws ServiceException
	 */
	public void deleteNews(Long newsId) throws ServiceException {
		newsService.deleteTagFromNewsByNewsId(newsId);
		newsService.deleteFromNewsAuthor(newsId);
		commentService.deleteCommentByNewsId(newsId);
		newsService.delete(newsId);

	}

	/**
	 * Edits news value object.
	 * 
	 * @param news
	 *            to edit
	 * @param listTag
	 *            to edit
	 * @param authorId
	 *            to edit
	 * @throws ServiceException
	 * @throws StaleVersionException
	 */
	public void editNews(News news, List<Tag> listTag, Long authorId) throws ServiceException, StaleVersionException {
		long newsId = news.getNewsId();
		newsService.deleteFromNewsAuthor(newsId);
		newsService.deleteTagFromNewsByNewsId(newsId);

		newsService.update(news);
		if (authorId != null) {
			newsService.addToNewsAuthor(newsId, authorId);
		}
		if (listTag != null) {
			newsService.addToNewsTag(newsId, listTag);
		}

	}

	/**
	 * Gets the list of all news value object.
	 * 
	 * @return the list of News value object.
	 * @throws ServiceException
	 */
	public List<NewsVO> getAllNews(SearchCriteria searchCriteria, int start, int end) throws ServiceException {
		List<NewsVO> newsVOList = new ArrayList<>();

		List<News> listNews = newsService.searchBySearchCriteriaAndPagination(searchCriteria, start, end);
		int size = listNews.size();

		if (size > 0) {
			for (int i = 0; i < size; i++) {
				NewsVO newsVO = new NewsVO();
				newsVO.setNews(listNews.get(i));
				Long newsId = listNews.get(i).getNewsId();

				Long authorId = newsService.getAuthorIdByNewsId(newsId);
				if (authorId != null) {
					Author author = authorService.getSingleAuthorById(authorId);
					newsVO.setAuthor(author);
				}
				List<Comment> commentsList = commentService.getListOfCommentsByNewsId(newsId);
				if (commentsList != null) {
					newsVO.setCommentsList(commentsList);
				}
				List<Tag> tagsList = newsService.getListOfTagsByNewsId(newsId);
				if (tagsList != null) {
					newsVO.setListTag(tagsList);
				}

				newsVOList.add(newsVO);
			}
		}

		return newsVOList;
	}

	/**
	 * Gets the single news value object from database by certain id of news.
	 * 
	 * @param newsId
	 *            to get
	 * @return news value object.
	 * @throws ServiceException
	 */
	public NewsVO getSingleNewsById(Long newsId) throws ServiceException {
		NewsVO newsVO = new NewsVO();
		if (newsId != null) {
			Long authorId = newsService.getAuthorIdByNewsId(newsId);
			News news = newsService.getSingleNewsById(newsId);
			if (authorId != null) {
				Author author = authorService.getSingleAuthorById(authorId);
				newsVO.setAuthor(author);
			}
			List<Comment> commentsList = commentService.getListOfCommentsByNewsId(newsId);
			List<Tag> tagsList = newsService.getListOfTagsByNewsId(newsId);

			newsVO.setNews(news);
			if (commentsList != null) {
				newsVO.setCommentsList(commentsList);
			}
			if (tagsList != null) {
				newsVO.setListTag(tagsList);
			}
		}
		return newsVO;
	}

	/**
	 * Gets the list of News value object found according to search criteria
	 * parameters.
	 * 
	 * @param searchCriteria
	 * @return list of News value object.
	 * @throws ServiceException
	 */
	public List<NewsVO> getNewsListBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		List<NewsVO> newsVOList = new ArrayList<>();

		List<News> listNews = newsService.search(searchCriteria);
		int size = listNews.size();
		if (size > 0) {
			for (int i = 0; i < size; i++) {
				NewsVO newsVO = new NewsVO();
				Long newsId = listNews.get(i).getNewsId();
				if (newsId != null) {
					Long authorId = newsService.getAuthorIdByNewsId(newsId);
					if (authorId != null) {
						Author author = authorService.getSingleAuthorById(authorId);
						newsVO.setAuthor(author);
					}
					List<Comment> commentsList = commentService.getListOfCommentsByNewsId(newsId);
					if (commentsList != null) {
						newsVO.setCommentsList(commentsList);
					}
					List<Tag> tagsList = newsService.getListOfTagsByNewsId(newsId);
					if (tagsList != null) {
						newsVO.setListTag(tagsList);
					}

					newsVO.setNews(listNews.get(i));

				}
				newsVOList.add(newsVO);
			}
		}
		return newsVOList;
	}

	public void deleteTag(Long tagId) throws ServiceException {
		newsService.deleteFromNewsTag(tagId);
		tagService.delete(tagId);
	}

	/**
	 * @return the newsService
	 */
	public INewsService getNewsService() {
		return newsService;
	}

	/**
	 * @param newsService
	 *            the newsService to set
	 */
	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	/**
	 * @return the authorService
	 */
	public IAuthorService getAuthorService() {
		return authorService;
	}

	/**
	 * @param authorService
	 *            the authorService to set
	 */
	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	/**
	 * @return the commentService
	 */
	public ICommentService getCommentService() {
		return commentService;
	}

	/**
	 * @param commentService
	 *            the commentService to set
	 */
	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

}
