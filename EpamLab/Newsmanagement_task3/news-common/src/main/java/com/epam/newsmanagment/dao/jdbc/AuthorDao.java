package com.epam.newsmanagment.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.IAuthorDao;
import com.epam.newsmanagment.dao.util.DaoUtil;
import com.epam.newsmanagment.entity.Author;

/**
 * @author Sviatlana_Andryianch
 *
 */
public class AuthorDao implements IAuthorDao {

	private DataSource dataSource;

	private static final String INSERT_AUTHOR_QUERY = "INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHOR_SEQ.nextval, ?, ?)";
	private static final String SELECT_SINGLE_AUTHOR_QUERY = "SELECT a.AUTHOR_ID, a.AUTHOR_NAME, a.EXPIRED FROM AUTHOR a WHERE AUTHOR_ID = ?";
	private static final String UPDATE_AUTHOR_QUERY = "UPDATE Author SET AUTHOR_NAME = ?, EXPIRED = ?  WHERE AUTHOR_ID = ?";
	private static final String SELECT_ALL_AUTHORS_QUERY = "SELECT a.AUTHOR_ID, a.AUTHOR_NAME, a.EXPIRED FROM AUTHOR a WHERE a.EXPIRED IS null";
	private static final String SET_EXPIRATION_DATE_QUERY = "UPDATE AUTHOR SET EXPIRED = CURRENT_TIMESTAMP WHERE AUTHOR_ID = ?";

	private static String authorId = "AUTHOR_ID";
	private static String authorName = "AUTHOR_NAME";
	private static String expired = "EXPIRED";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#create(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public Long create(Author entity) throws DaoException {
		Long id = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(INSERT_AUTHOR_QUERY, new String[] { authorId });
			preparedStatement.setString(1, entity.getAuthorName());
			if (entity.getExpired() != null) {
				Timestamp ts = new Timestamp(entity.getExpired().getTime()); // Type
				preparedStatement.setTimestamp(2, ts);
			} else {
				preparedStatement.setTimestamp(2, null);
			}

			preparedStatement.executeUpdate();
			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				id = generatedKeys.getLong(1);
			}

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}

		return id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.epam.newsmanagment.model.dao.IAuthorDao#getSingleAuthorById(long)
	 */
	public Author getSingleAuthorById(long id) throws DaoException {
		Author author = new Author();
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_SINGLE_AUTHOR_QUERY);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				author.setAuthorId(resultSet.getLong(authorId));
				author.setAuthorName(resultSet.getString(authorName));
				author.setExpired(resultSet.getTimestamp(expired));
			}

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}
		return author;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#update(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public void update(Author entity) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(UPDATE_AUTHOR_QUERY);
			preparedStatement.setString(1, entity.getAuthorName());
			if (entity.getExpired() != null) {
				Timestamp ts = new Timestamp(entity.getExpired().getTime());
				preparedStatement.setTimestamp(2, ts);
			} else {
				preparedStatement.setTimestamp(2, null);
			}

			preparedStatement.setLong(3, entity.getAuthorId());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#getAll()
	 */
	public List<Author> getAll() throws DaoException {
		Author author = null;

		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		List<Author> authorList = new ArrayList<Author>();

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_ALL_AUTHORS_QUERY);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				author = new Author();
				author.setAuthorId(resultSet.getLong(authorId));
				author.setAuthorName(resultSet.getString(authorName));
				author.setExpired(resultSet.getTimestamp(expired));
				authorList.add(author); // Add object Author to list
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}

		return authorList;
	}

	/**
	 * Makes the author of certain id expired. Sets the expiration date
	 * 
	 * @throws DaoException
	 */
	private void makeAuthorExpired(long id) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SET_EXPIRATION_DATE_QUERY);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}

	}

	/**
	 * Method is not implemented because it is not able to delete author, but
	 * possible to make him expired.
	 * 
	 * @see makeAuthorExpired method.
	 */
	@Override
	public void delete(long id) throws DaoException {
		makeAuthorExpired(id);
	}

	/////////////////////////
	// Getters and setters
	////////////////////////

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

}
