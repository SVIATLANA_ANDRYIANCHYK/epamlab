package com.epam.newsmanagment.dao.eclipselink;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.IAuthorDao;
import com.epam.newsmanagment.entity.Author;

@Transactional
@Repository
public class AuthorDao implements IAuthorDao {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long create(Author entity) {
		entityManager.persist(entity);
		entityManager.flush();
		return entity.getAuthorId();
	}

	@Override
	public void update(Author entity) {
		entityManager.merge(entity);

	}

	private void setAuthorExpired(Long authorId) {
		Author author = entityManager.find(Author.class, authorId);
		author.setExpired(new Date());
		entityManager.merge(author);
	}

	@Override
	public void delete(long id) {
		setAuthorExpired(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> getAll() throws DaoException {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Author> cq = cb.createQuery(Author.class);
		Root<Author> root = cq.from(Author.class);
		cq.where(cb.isNull(root.get("expired")));
		Query query = entityManager.createQuery(cq);
		return query.getResultList();
	}

	@Override
	public Author getSingleAuthorById(long id) throws DaoException {
		return entityManager.find(Author.class, id);
	}
}
