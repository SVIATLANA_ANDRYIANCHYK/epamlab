package com.epam.newsmanagment.dao;

import java.util.List;

import com.epam.newsmanagment.entity.IEntity;

/**
 * Abstraction of all dao entities.
 * 
 * @author Sviatlana_Andryianch
 *
 */
public interface ICommonDao<T extends IEntity> {

	/**
	 * Adds entity to the database.
	 * 
	 * @param entity
	 *            entity for adding to a database.
	 */
	public Long create(T entity) throws DaoException;

	/**
	 * 
	 * @param entity
	 *            adding to db
	 * @return true if operation ended successfully, false - if not
	 * @throws DaoException
	 * @throws StaleVersionException
	 */
	public void update(T entity) throws DaoException, StaleVersionException;

	/**
	 * 
	 * @param deliting
	 *            entity
	 * @return true if operation ended successfully, false - if not
	 * @throws DaoException
	 */
	public void delete(long id) throws DaoException;

	/**
	 * Shows all items in the database
	 * 
	 * @return list of items
	 * @throws DaoException
	 */
	public List<T> getAll() throws DaoException;

}
