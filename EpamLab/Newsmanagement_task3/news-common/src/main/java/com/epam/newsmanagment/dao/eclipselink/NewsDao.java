package com.epam.newsmanagment.dao.eclipselink;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.INewsDao;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;

@Transactional
@Repository
public class NewsDao implements INewsDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long create(News entity) {
		entityManager.persist(entity);
		entityManager.flush();
		return entity.getNewsId();
	}

	@Override
	public void update(News entity) {
		entityManager.merge(entity);
	}

	@Override
	public void delete(long id) {
		News news = entityManager.find(News.class, id);
		entityManager.remove(news);

	}

	@Override
	public List<News> searchBySearchCriteriaAndPagination(SearchCriteria searchCriteria, int pageNumber, int pageSize) {
		Query query = buildSearchCriteria(searchCriteria);
		query.setFirstResult((pageNumber - 1) * pageSize);
		query.setMaxResults(pageSize);
		@SuppressWarnings("unchecked")
		List<News> newsList = query.getResultList();

		return newsList;
	}

	@Override
	public Long getNewsCountBySearchCriteria(SearchCriteria searchCriteria) {
		@SuppressWarnings("unchecked")
		List<News> newsList = buildSearchCriteria(searchCriteria).getResultList();
		int newsCount = newsList.size();
		return (long) newsCount;
	}

	@Override
	public News getSingleNewsById(long id) {
		return entityManager.find(News.class, id);
	}

	private Query buildSearchCriteria(SearchCriteria searchCriteria) {
		StringBuilder queryStringBuilder = new StringBuilder("select distinct n from News n left join n.commentsList as c left join n.tagSet as nt left join n.author as a ");

		if (searchCriteria != null) {

			// if only author set
			if ((searchCriteria.getAuthorId() != null) && ((searchCriteria.getTagList() == null || searchCriteria.getTagList().isEmpty()))) {
				queryStringBuilder = getQueryStringOnAuthorSet(queryStringBuilder, searchCriteria);

			} else if ((searchCriteria.getAuthorId() == null) && (searchCriteria.getTagList() != null) && (!searchCriteria.getTagList().isEmpty())) {
				// if only tags set
				queryStringBuilder = getQueryStringOnTagsSet(queryStringBuilder, searchCriteria);
			} else if ((searchCriteria.getAuthorId() != null) && (searchCriteria.getTagList() != null)) {
				// if both tags and author set
				queryStringBuilder = getQueryStringOnAuthorTagsSet(queryStringBuilder, searchCriteria);
			}
		}
		queryStringBuilder
				.append(" group by n.newsId, n.title, n.shortText, n.fullText, n.creationDate, n.modificationDate, n.author.authorId, n.version order by n.modificationDate desc, size(n.commentsList) desc");
		String queryString = queryStringBuilder.toString();
		Query query = entityManager.createQuery(queryString);
		return query;
	}

	private StringBuilder getQueryStringOnAuthorSet(StringBuilder queryStringBuilder, SearchCriteria searchCriteria){
		queryStringBuilder.append("where a.authorId = ");
		Long authorId = searchCriteria.getAuthorId();
		queryStringBuilder.append(authorId);
		return queryStringBuilder;
	}

	private StringBuilder getQueryStringOnTagsSet(StringBuilder queryStringBuilder, SearchCriteria searchCriteria){
		queryStringBuilder.append("where nt.tagId in (");
		List<Long> tagIdList = searchCriteria.getTagList();
		for (int i = 0; i < tagIdList.size(); i++) {
			queryStringBuilder.append(tagIdList.get(i));
			if (i != (tagIdList.size() - 1)) {
				queryStringBuilder.append(", ");
			} else {
				queryStringBuilder.append(")");
			}
		}
		return queryStringBuilder;
	}

	private StringBuilder getQueryStringOnAuthorTagsSet(StringBuilder queryStringBuilder, SearchCriteria searchCriteria){
		queryStringBuilder.append("where a.authorId = ");
		Long authorId = searchCriteria.getAuthorId();
		queryStringBuilder.append(authorId);
		queryStringBuilder.append(" and ");
		queryStringBuilder.append("nt.tagId in (");
		List<Long> tagIdList = searchCriteria.getTagList();
		for (int i = 0; i < tagIdList.size(); i++) {
			queryStringBuilder.append(tagIdList.get(i));
			if (i != (tagIdList.size() - 1)) {
				queryStringBuilder.append(", ");
			} else {
				queryStringBuilder.append(")");
			}
		}
		return queryStringBuilder;
	}


	/////////////////////////////
	//  Unsupported methods
	////////////////////////////


	@Override
	public void deleteTagFromNewsByNewsId(long newsId) {
		throw new UnsupportedOperationException();

	}

	@Override
	public List<News> getAll() {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<News> search(SearchCriteria searchCriteria) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getNewsCount() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void linkAuthorNews(long newsId, long authorId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void unlinkAuthorNews(long newsId) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void addTagToNews(long newsId, List<Tag> tagList) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void deleteTagFromNews(long newsId) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Long getAuthorIdByNewsId(long newsId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Long> getTagIdListByNewsId(long newsId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Tag> getListOfTagsByNewsId(long newsId) {
		throw new UnsupportedOperationException();
	}

}
