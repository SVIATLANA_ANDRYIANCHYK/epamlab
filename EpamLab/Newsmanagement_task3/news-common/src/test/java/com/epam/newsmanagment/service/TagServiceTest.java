package com.epam.newsmanagment.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.StaleVersionException;
import com.epam.newsmanagment.dao.jdbc.TagDao;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.implementation.TagService;

@ContextConfiguration(locations = { "/application-context.xml" })
@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {
	@Mock
	private TagDao mockTagDao;

	@InjectMocks
	@Autowired
	private TagService tagService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(tagService);
		assertNotNull(mockTagDao);
	}

	@Test
	public void testCreate() throws DaoException, ServiceException {
		//Record behavior
		when(mockTagDao.create(any(Tag.class))).thenReturn(1L);
		//When
		long actualAddedTag = tagService.create(new Tag());
		//Then
		assertEquals(1L, actualAddedTag);
		verify(mockTagDao, times(1)).create(new Tag());
		verifyNoMoreInteractions(mockTagDao);
	}

	@Test
	public void testDelete() throws ServiceException, DaoException {
		//When
		tagService.delete(anyLong());
		//Then
		verify(mockTagDao, times(1)).delete(anyLong());
		verifyNoMoreInteractions(mockTagDao);
	}

	@Test
	public void testGetAll() throws DaoException, ServiceException {
		//Given
		List<Tag> expectedTagList = new ArrayList<Tag>();
		//Record behavior
		when(mockTagDao.getAll()).thenReturn(expectedTagList);
		//When
		List<Tag> actualTagList = tagService.getAll();
		//Then
		assertEquals(expectedTagList, actualTagList);
		verify(mockTagDao, times(1)).getAll();
		verifyNoMoreInteractions(mockTagDao);
	}

	@Test
	public void update() throws ServiceException, DaoException, StaleVersionException {
		//When
		tagService.update(any(Tag.class));
		//Then
		verify(mockTagDao, times(1)).update(any(Tag.class));
		verifyNoMoreInteractions(mockTagDao);
	}

	@Test
	public void testGetSingletagById() throws DaoException, ServiceException {
		//Given
		Tag expectedTag = new Tag();
		//Record behavior
		when(mockTagDao.getSingleTagById(anyLong())).thenReturn(expectedTag);
		//When
		Tag actualTag = tagService.getSingleTagById(anyLong());
		//Then
		assertEquals(expectedTag, actualTag);
		verify(mockTagDao, times(1)).getSingleTagById(anyLong());
		verifyNoMoreInteractions(mockTagDao);
	}
}
