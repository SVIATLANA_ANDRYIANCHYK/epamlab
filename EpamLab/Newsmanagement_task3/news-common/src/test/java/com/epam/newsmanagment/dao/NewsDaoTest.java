package com.epam.newsmanagment.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import junit.framework.Assert;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;

import com.epam.newsmanagment.dao.jdbc.NewsDao;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;

/**
 * Tests for NewsDao class
 *
 *
 * @author Sviatlana_Andryianch
 *
 */
@Transactional(TransactionMode.ROLLBACK)
@DataSet
/**
 * @see NewsDao.xml
 */
public class NewsDaoTest extends UnitilsJUnit4 {

	private final int EXPECTED_NEWS_COUNT = 3;
	private final long ID_EXTRACTING_ROW = 3L;

	// Test datasource creation
	// @see unitils.properties

	@TestDataSource
	private DataSource dataSource;

	/**
	 *
	 * Checks whether the initial row count equals to expected row count
	 *
	 * @throws DaoException
	 */
	@Test
	public void testGetAll() throws DaoException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		//When
		newsDao.getAll();
		//Then
		Assert.assertEquals(EXPECTED_NEWS_COUNT, newsDao.getAll().size());

	}

	/**
	 *
	 * Checks whether certain object added into database
	 *
	 * @throws DaoException
	 * @throws ParseException
	 */
	@Test
	public void testCreate() throws DaoException, ParseException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);

		News insertedNews = new News();
		insertedNews.setTitle("title5");
		insertedNews.setShortText("shortText5");
		insertedNews.setFullText("fullText5");

		String creationDateString = "2011-01-01 02:00:00";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date creationDate = dt.parse(creationDateString);
		insertedNews.setCreationDate(creationDate);

		String modficationDateString = "2011-01-01 02:00:00";
		Date modficationDate = dt.parse(modficationDateString);
		insertedNews.setCreationDate(modficationDate);

		insertedNews.setModificationDate(modficationDate);
		//When
		long id = newsDao.create(insertedNews);
		insertedNews.setNewsId(id);
		News receivedNews = newsDao.getSingleNewsById(id);
		//Then
		assertObjects(insertedNews, receivedNews);
	}

	/**
	 * Checks the row count before and after deleting row
	 *
	 * @throws DaoException
	 */
	@Test
	public void testDelete() throws DaoException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		//When
		newsDao.delete(ID_EXTRACTING_ROW);
		//Then
		Assert.assertEquals(EXPECTED_NEWS_COUNT - 1, newsDao.getAll().size());
	}

	/**
	 * Checks the update of the row. Compares the initial row fields and this
	 * row after update
	 *
	 * @throws DaoException
	 * @throws ParseException
	 */
	@Test
	public void testUpdate() throws DaoException, ParseException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		String creationDateString = "2011-01-01 02:00:00";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date creationDate;

		creationDate = dt.parse(creationDateString);
		String modificationDateString = "2011-01-01 02:00:00";
		Date modificationDate = dt.parse(modificationDateString);
		News news = new News();
		//When
		newsDao.update(news);
		//Then
		assertObjects(news, newsDao.getSingleNewsById(news.getNewsId()));
	}

	/**
	 * Test of search method. Checks whether the row that had to be found equals
	 * to the row that was found by the method
	 *
	 * @throws DaoException
	 * @throws ParseException
	 */
	@Test
	public void testSearch() throws DaoException, ParseException {
		//Given
		SearchCriteria sc = new SearchCriteria();
		sc.setAuthorId(1L);
		ArrayList<Long> list = new ArrayList<>();
		list.add(1L);
		list.add(2L);
		sc.setTagList(list);

		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		String creationDateString = "2011-01-01 02:00:00";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date creationDate;
		creationDate = dt.parse(creationDateString);
		String modificationDateString = "2011-01-01 02:00:00";
		Date modificationDate = dt.parse(modificationDateString);
		News news = new News();
		//When
		News actualNews =  newsDao.search(sc).get(0);
		//Then
		assertObjects(news,actualNews);

	}

	/**
	 * Checks the row count. Whether the initial row count equals to row count
	 * that was returned by the method.
	 *
	 * @throws DaoException
	 */
	@Test
	public void testGetNewsCount() throws DaoException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		//When
		int actualNewsCount = newsDao.getNewsCount();
		//Then
		Assert.assertEquals(EXPECTED_NEWS_COUNT, actualNewsCount);
	}

	@Test
	public void testAddToNewsAuthor() throws DaoException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		Long newsId = 2L;
		Long authorId = 1L;
		//When
		newsDao.linkAuthorNews(newsId, authorId);
		//Then
		Assert.assertEquals(authorId, newsDao.getAuthorIdByNewsId(newsId));
	}

	@Test
	public void testAddToNewsTag() throws DaoException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		long newsId = 2L;
		long tagId = 1L;
		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setTagName("name");
		List<Tag> tagList = new ArrayList<>();
		tagList.add(tag);
		//When
		newsDao.addTagToNews(newsId, tagList);
		int actualListSize = newsDao.getListOfTagsByNewsId(newsId).size();
		//Then
		Assert.assertEquals(1, actualListSize);
	}

	@Test
	public void testGetSingleNewsById() throws DaoException, ParseException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		String creationDateString = "2011-01-01 02:00:00";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date creationDate;
		creationDate = dt.parse(creationDateString);
		String modificationDateString = "2011-01-01 02:00:00";
		Date modificationDate = dt.parse(modificationDateString);
		News news = new News();
		//When
		News actualNews = newsDao.getSingleNewsById(news.getNewsId());
		//Then
		assertObjects(news, actualNews);
	}

	@Test
	public void testDeleteFromNewsAuthor() throws DaoException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		Long newsId = 1L;

		Long authorId = 1L;
		Assert.assertEquals(authorId, newsDao.getAuthorIdByNewsId(newsId));
		//When
		newsDao.unlinkAuthorNews(newsId);
		//Then
		Assert.assertEquals(null, newsDao.getAuthorIdByNewsId(newsId));

	}

	@Test
	public void testDeleteFromNewsTag() throws DaoException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		long newsId = 1L;
		//When
		newsDao.deleteTagFromNews(newsId);
		//Then
		Assert.assertEquals(0, newsDao.getListOfTagsByNewsId(newsId).size());
	}

	@Test
	public void testGetAuthorByNewsId() throws DaoException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		long newsId = 1L;
		//When
		long actualAuthorId = newsDao.getAuthorIdByNewsId(newsId);
		long expectedAuthorId = 1L;
		//Then
		Assert.assertEquals(expectedAuthorId, actualAuthorId);
	}

	@Test
	public void getListOfTagsByNewsId() throws DaoException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		long newsId = 1L;
		int expectedListSize = 1;
		//When
		int actualListSize = newsDao.getListOfTagsByNewsId(newsId).size();
		//Then
		Assert.assertEquals(expectedListSize, actualListSize);
	}

	@Test
	public void testGetTagIdListByNewsId() throws DaoException {
		//Given
		NewsDao newsDao = new NewsDao();
		newsDao.setDataSource(dataSource);
		long newsId = 1L;
		int expectedTagIdListSize = 1;
		//When
		int actualTagIdListSize = newsDao.getTagIdListByNewsId(newsId).size();

		//Then
		Assert.assertEquals(expectedTagIdListSize, actualTagIdListSize);
	}

	/**
	 * Method for comparison of two objects
	 *
	 * @param firstObj
	 * @param secondObj
	 * @throws DaoException
	 */
	private void assertObjects(News firstObj, News secondObj) {
		Assert.assertEquals(firstObj.getNewsId(), secondObj.getNewsId());
		Assert.assertEquals(firstObj.getTitle(), secondObj.getTitle());
		Assert.assertEquals(firstObj.getShortText(), secondObj.getShortText());
		Assert.assertEquals(firstObj.getFullText(), secondObj.getFullText());
		Assert.assertEquals(firstObj.getCreationDate().getTime(), secondObj.getCreationDate().getTime());
	}
 }
