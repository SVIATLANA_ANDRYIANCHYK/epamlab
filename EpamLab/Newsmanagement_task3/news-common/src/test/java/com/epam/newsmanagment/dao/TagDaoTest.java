package com.epam.newsmanagment.dao;

import java.text.ParseException;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;

import com.epam.newsmanagment.dao.jdbc.TagDao;
import com.epam.newsmanagment.entity.Tag;

@Transactional(TransactionMode.ROLLBACK)
@DataSet
public class TagDaoTest extends UnitilsJUnit4 {

	@TestDataSource
	private DataSource dataSource;

	private static final int EXPECTED_ROW_COUNT = 3;

	/**
	 *
	 * Checks whether certain object added into database
	 *
	 * @throws DaoException
	 * @throws ParseException
	 */
	@Test
	public void testCreate() throws DaoException {
		//Given
		TagDao dao = new TagDao();
		dao.setDataSource(dataSource);

		Tag tag = new Tag();
		Long id = dao.create(tag);
		tag.setTagId(id);
		//When
		Tag actualTag = dao.getSingleTagById(id);
		//Then
		Assert.assertEquals(tag, actualTag);
	}

	/**
	 * Checks whether the object was deleted. Checks the existence of the row in
	 * the database before and after deleting.
	 *
	 * @throws DaoException
	 */
	@Test
	public void testDelete() throws DaoException {
		//Given
		TagDao dao = new TagDao();
		dao.setDataSource(dataSource);

		Long tagId = 1L;
		Tag tagBeforeDelete = dao.getSingleTagById(tagId);
		Assert.assertNotNull(tagBeforeDelete);
		//When
		dao.delete(tagId);

		Tag tagAfterDelete = dao.getSingleTagById(tagId);
		//Then
		Assert.assertNull(tagAfterDelete);
	}

	/**
	 * Asserts the expected and actual row count.
	 *
	 * @throws DaoException
	 */
	@Test
	public void testGetAll() throws DaoException {
		//Given
		TagDao dao = new TagDao();
		dao.setDataSource(dataSource);
		//When
		List<Tag> tagList = dao.getAll();
		int actualRowCount = tagList.size();
		//Then
		Assert.assertEquals(EXPECTED_ROW_COUNT, actualRowCount);
	}

	/**
	 * Asserts expected and actual Tag entity.
	 *
	 * @throws DaoException
	 * @throws ParseException
	 */
	@Test
	public void testGetSingleTagById() throws DaoException {
		//Given
		TagDao dao = new TagDao();
		dao.setDataSource(dataSource);

		Tag expectedTag = new Tag();
		long tagId = 1L;
		//When
		Tag actualTag = dao.getSingleTagById(tagId);
		//Then
		Assert.assertEquals(expectedTag, actualTag);
	}

	/**
	 * Tests whether certain object was updated in the database. Asserts the
	 * actually adding updates with the updatable object after update.
	 *
	 * @throws DaoException
	 */
	@Test
	public void testUpdate() throws DaoException {
		//Given
		TagDao dao = new TagDao();
		dao.setDataSource(dataSource);

		Tag newTag = new Tag();
		dao.update(newTag);
		Long tagId = 1L;
		//When
		Tag actualTag = dao.getSingleTagById(tagId);
		//Then
		Assert.assertEquals(newTag, actualTag);
	}
}
