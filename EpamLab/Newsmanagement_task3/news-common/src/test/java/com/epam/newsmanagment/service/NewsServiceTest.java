package com.epam.newsmanagment.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.StaleVersionException;
import com.epam.newsmanagment.dao.jdbc.NewsDao;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.implementation.NewsService;

@ContextConfiguration(locations = { "/application-context.xml" })
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

	@Mock
	private NewsDao mockNewsDao;

	@InjectMocks
	@Autowired
	private NewsService newsService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(newsService);
		assertNotNull(mockNewsDao);
	}

	@Test
	public void testCreate() throws DaoException, ServiceException {
		//Record behavior
		when(mockNewsDao.create(any(News.class))).thenReturn(Long.valueOf(1));
		//When
		long actualAddedNewsId = newsService.create(new News());
		//Then
		verify(mockNewsDao, times(1)).create(new News());
		verifyNoMoreInteractions(mockNewsDao);
		assertEquals(1L, actualAddedNewsId);
	}

	@Test
	public void testUpdate() throws ServiceException, DaoException, StaleVersionException {
		//When
		newsService.update(any(News.class));
		//Then
		verify(mockNewsDao, times(1)).update(any(News.class));
		verifyNoMoreInteractions(mockNewsDao);
	}

	@Test
	public void testDelete() throws ServiceException, DaoException {
		//When
		newsService.delete(anyLong());
		//Then
		verify(mockNewsDao, times(1)).delete(anyLong());
		verifyNoMoreInteractions(mockNewsDao);
	}

	@Test
	public void testAddToNewsAuthor() throws ServiceException, DaoException {
		//When
		newsService.addToNewsAuthor(anyLong(), anyLong());
		//Then
		verify(mockNewsDao, times(1)).linkAuthorNews(anyLong(), anyLong());
		verifyNoMoreInteractions(mockNewsDao);

	}

	@Test
	public void testAddToNewsTag() throws ServiceException, DaoException {
		//When
		newsService.addToNewsTag(anyLong(), anyListOf(Tag.class));
		//Then
		verify(mockNewsDao, times(1)).addTagToNews(anyLong(), anyListOf(Tag.class));
		verifyNoMoreInteractions(mockNewsDao);
	}

	@Test
	public void testDeleteFrom_newsAuthor() throws ServiceException, DaoException {
		//When
		newsService.deleteFromNewsAuthor(anyLong());
		//Then
		verify(mockNewsDao, times(1)).unlinkAuthorNews(anyLong());
		verifyNoMoreInteractions(mockNewsDao);
	}

	@Test
	public void testDeleteFromNewsTag() throws ServiceException, DaoException {
		//When
		newsService.deleteFromNewsTag(anyLong());
		//Then
		verify(mockNewsDao, times(1)).deleteTagFromNews(anyLong());
		verifyNoMoreInteractions(mockNewsDao);
	}

	@Test
	public void testGetAll() throws DaoException, ServiceException {
		//Given
		List<News> listNews = new ArrayList<>();
		listNews.add(new News());
		//Record behavior
		when(mockNewsDao.getAll()).thenReturn(listNews);
		//When
		List<News> actualListNews = newsService.getAll();
		//Then
		assertEquals(listNews, actualListNews);
		verify(mockNewsDao, times(1)).getAll();
		verifyNoMoreInteractions(mockNewsDao);
	}

	@Test
	public void testGetAuthorIdByNewsId() throws DaoException, ServiceException {
		//Record behavior
		when(mockNewsDao.getAuthorIdByNewsId(anyLong())).thenReturn(1L);
		//When
		long actualAuthorIdLong = newsService.getAuthorIdByNewsId(anyLong());
		//Then
		assertEquals(1L, actualAuthorIdLong);
		verify(mockNewsDao, times(1)).getAuthorIdByNewsId(anyLong());
		verifyNoMoreInteractions(mockNewsDao);
	}

	@Test
	public void testGetListOfTagsByNewsId() throws DaoException, ServiceException {
		//Given
		List<Tag> listTags = new ArrayList<>();
		listTags.add(new Tag());
		//Record behavior
		when(mockNewsDao.getListOfTagsByNewsId(anyLong())).thenReturn(listTags);
		//When
		List<Tag> actualListOfTags = newsService.getListOfTagsByNewsId(anyLong());
		//Then
		assertEquals(listTags, actualListOfTags);
		verify(mockNewsDao, times(1)).getListOfTagsByNewsId(anyLong());
		verifyNoMoreInteractions(mockNewsDao);
	}

	@Test
	public void testNewsCount() throws DaoException, ServiceException {
		//Record behavior
		when(mockNewsDao.getNewsCount()).thenReturn(1);
		//When
		int actualNewsCount = newsService.getNewsCount();
		//Then
		assertEquals(1, actualNewsCount);
		verify(mockNewsDao, times(1)).getNewsCount();
		verifyNoMoreInteractions(mockNewsDao);
	}

	@Test
	public void testGetSingleNewsById() throws DaoException, ServiceException {
		//Given
		News expectedNews = new News();
		//Record behavior
		when(mockNewsDao.getSingleNewsById(anyLong())).thenReturn(expectedNews);
		//When
		News actualNews = newsService.getSingleNewsById(anyLong());
		//Then
		assertEquals(expectedNews, actualNews);
		verify(mockNewsDao, times(1)).getSingleNewsById(anyLong());
		verifyNoMoreInteractions(mockNewsDao);
	}

	@Test
	public void testGetTagIdListByNewsId() throws DaoException, ServiceException {
		//Given
		List<Long> expectedListOfTagId = new ArrayList<Long>();
		//Record behavior
		when(mockNewsDao.getTagIdListByNewsId(anyLong())).thenReturn(expectedListOfTagId);
		//When
		List<Long> actualListOfTagId = newsService.getTagIdListByNewsId(anyLong());
		//Then
		assertEquals(expectedListOfTagId, actualListOfTagId);
		verify(mockNewsDao, times(1)).getTagIdListByNewsId(anyLong());
		verifyNoMoreInteractions(mockNewsDao);
	}

	@Test
	public void testSearch() throws DaoException, ServiceException {
		//Given
		List<News> listNews = new ArrayList<>();
		SearchCriteria searchCriteria = new SearchCriteria();
		//Record behavior
		when(mockNewsDao.search(searchCriteria)).thenReturn(listNews);
		//When
		List<News> actualNewsList = newsService.search(searchCriteria);
		//Then
		assertEquals(listNews, actualNewsList);
		verify(mockNewsDao, times(1)).search(searchCriteria);
		verifyNoMoreInteractions(mockNewsDao);
	}

}
