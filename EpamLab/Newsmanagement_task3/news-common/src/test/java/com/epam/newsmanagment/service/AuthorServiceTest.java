package com.epam.newsmanagment.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.StaleVersionException;
import com.epam.newsmanagment.dao.jdbc.AuthorDao;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.service.implementation.AuthorService;

@ContextConfiguration(locations = { "/application-context.xml" })
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
	@Mock
	private AuthorDao mockAuthorDao;

	@InjectMocks
	@Autowired
	private AuthorService authorService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(authorService);
		assertNotNull(mockAuthorDao);
	}

	@Test
	public void testCreate() throws DaoException, ServiceException {
		//Record Behaviour
		when(mockAuthorDao.create(any(Author.class))).thenReturn(1L);
		//When
		long actualAddedAuthor = authorService.create(new Author());
		//Then
		assertEquals(1L, actualAddedAuthor);
		verify(mockAuthorDao, times(1)).create(new Author());
		verifyNoMoreInteractions(mockAuthorDao);
	}

	@Test
	public void testDelete() throws ServiceException, DaoException {
		//When
		authorService.delete(anyLong());
		//Then
		verify(mockAuthorDao, times(1)).delete(anyLong());
		verifyNoMoreInteractions(mockAuthorDao);
	}

	@Test
	public void testGetAll() throws DaoException, ServiceException {
		//Given
		List<Author> expectedAuthorList = new ArrayList<Author>();
		//Record behavior
		when(mockAuthorDao.getAll()).thenReturn(expectedAuthorList);
		//When
		List<Author> actualAuthorList = authorService.getAll();
		//Then
		assertEquals(expectedAuthorList, actualAuthorList);
		verify(mockAuthorDao, times(1)).getAll();
		verifyNoMoreInteractions(mockAuthorDao);
	}

	@Test
	public void testGetSingleAuthorById() throws DaoException, ServiceException {
		//given
		Author expectedAuthor = new Author();
		//Record behavior
		when(mockAuthorDao.getSingleAuthorById(anyLong())).thenReturn(expectedAuthor);
		//When
		Author actualAuthor = authorService.getSingleAuthorById(anyLong());
		//Then
		assertEquals(expectedAuthor, actualAuthor);
		verify(mockAuthorDao, times(1)).getSingleAuthorById(anyLong());
		verifyNoMoreInteractions(mockAuthorDao);
	}

	@Test
	public void testUpdate() throws ServiceException, DaoException, StaleVersionException {
		//When
		authorService.update(any(Author.class));
		//Then
		verify(mockAuthorDao, times(1)).update(any(Author.class));
		verifyNoMoreInteractions(mockAuthorDao);
	}
}
