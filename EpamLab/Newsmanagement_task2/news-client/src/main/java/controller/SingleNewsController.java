package controller;

import java.util.Date;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import util.Navigator;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.service.ICommentService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ServiceException;
import com.epam.newsmanagment.service.VO.NewsVO;
import com.epam.newsmanagment.service.implementation.ServiceManager;

@Controller
public class SingleNewsController {

	private ServiceManager serviceManager;
	private ICommentService commentService;
	private INewsService newsService;

	@Inject
	public SingleNewsController(ServiceManager serviceManager, ICommentService commentService, INewsService newsService) {
		this.serviceManager = serviceManager;
		this.commentService = commentService;
		this.newsService = newsService;
	}

	@RequestMapping(value = { "/view/{newsId}" }, method = RequestMethod.GET)
	public String viewSingleNews(@PathVariable("newsId") Long newsId, Model model, HttpSession session) throws ServiceException {
		NewsVO newsVO = serviceManager.getSingleNewsById(newsId);
		model.addAttribute("newsVO", newsVO);

		return "single-news";
	}

	@RequestMapping(value = { "/postComment" }, method = RequestMethod.POST)
	public String postComment(@RequestParam(value = "newsId") Long newsId, @RequestParam(value = "commentText") String commentText) throws ServiceException {
		if (!commentText.isEmpty() && commentText.length() < 100) {
			Comment comment = new Comment();
			Date today = new Date();
			comment.setCreationDate(today);
			comment.setCommentText(commentText);
			comment.setNewsId(newsId);
			commentService.create(comment);
		}
		return "redirect:/view/" + newsId;
	}

	@RequestMapping(value = { "/next" }, method = RequestMethod.GET)
	public String showNextNews(Long newsId, HttpSession session) throws ServiceException {
		Navigator navigator = new Navigator();
		SearchCriteria sc = (SearchCriteria) session.getAttribute("searchCtiteria");
		int newsCount = newsService.getNewsCount();
		NewsVO nextNews = navigator.getNextNewsVO(serviceManager, sc, newsId, newsCount);
		Long nextNewsId = nextNews.getNews().getId();
		return "redirect:/view/" + nextNewsId;
	}

	@RequestMapping(value = { "/previous" }, method = RequestMethod.GET)
	public String showPreviousNews(Long newsId, HttpSession session) throws ServiceException {
		Navigator navigator = new Navigator();
		SearchCriteria sc = (SearchCriteria) session.getAttribute("searchCtiteria");
		int newsCount = newsService.getNewsCount();
		NewsVO previousNews = navigator.getPreviousNewsVO(serviceManager, sc, newsId, newsCount);
		Long previousNewsId = previousNews.getNews().getId();
		return "redirect:/view/" + previousNewsId;
	}
}
