<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<link href="<c:url value="/resources/css/single-news.css" />"
	rel="stylesheet">
</head>
<body>
	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="body">
			<a href="/news-client/home"><spring:message code="single-news.back" /></a>
			<div class="news">
				<div class="title"><c:out value = "${newsVO.news.title}"/></div>
				<div class="author"><c:out value = "(by ${newsVO.author.authorName})"/></div>
				<fmt:message key="date.format" var="format" />
						<fmt:formatDate value="${newsVO.news.modificationDate}"
							pattern="${format}" var="formattedDate" />
				<div class="date"><c:out value = "${formattedDate}"/></div>
				<div class="full-text"><c:out value = "${newsVO.news.fullText}"/></div>

				<div class="comments">
					<c:forEach items="${newsVO.commentsList}" var="comment">
						<fmt:message key="date.format" var="format" />
						<fmt:formatDate value="${comment.creationDate}"
							pattern="${format}" var="formattedDate" />
						<div class="comment-date"><c:out value = "${formattedDate}"/></div>
						<div class="comment-text"><c:out value = "${comment.commentText}"/></div>
						<input type="hidden" value="${comment.commentId}" name="commentId">
						<input type="hidden" value="${newsVO.news.id}" name="newsId">


					</c:forEach>
				</div>

				<form action="/news-client/postComment" method="POST">
					<textarea name="commentText" class="comment-area"></textarea>
					<input type="hidden" value="${newsVO.news.id}" name="newsId">

					<div>
						<button type="submit" class="button"><spring:message code="single-news.post" /></button>
					</div>
				</form>

				<div class="navigation">
					<div><a href="/news-client/previous?newsId=${newsVO.news.id}"><spring:message code="single-news.previous" /></a></div>
					<div><a href="/news-client/next?newsId=${newsVO.news.id}" class="next"><spring:message code="single-news.next" /></a></div>
				</div>
			</div>
		</tiles:putAttribute>
	</tiles:insertDefinition>
</body>
</html>