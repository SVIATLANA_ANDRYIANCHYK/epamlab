<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="header-content">
	<h2>
		<spring:message code="header.label" />
	</h2>
	<div class = "languages">
		<a href="?lang=en"><spring:message code="header.language.en" /></a> |
		<a href="?lang=ru"><spring:message code="header.language.ru" /></a>
	</div>

	
	</div> 
