package com.epam.newsmanagment.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.ITagDao;
import com.epam.newsmanagment.entity.Tag;

public class TagDao implements ITagDao {

	private static final String SELECT_ALL_TAGS_QUERY = "SELECT t.TAG_ID, t.TAG_NAME FROM Tag t";
	private static final String INSERT_TAG_QUERY = "INSERT INTO Tag (TAG_ID, TAG_NAME) VALUES (TAG_SEQ.nextval, ?)";
	private static final String UPDATE_TAG_QUERY = "UPDATE Tag SET TAG_NAME = ? WHERE TAG_ID = ?";
	private static final String DELETE_TAG_QUERY = "DELETE FROM Tag WHERE TAG_ID = ?";
	private static final String SELECT_SINGLE_TAG_BY_ID_QUERY = "SELECT t.TAG_NAME FROM TAG t WHERE TAG_ID = ?";

	private static String tagId = "TAG_ID";
	private static String tagName = "TAG_NAME";
	private DataSource dataSource;

	/**
	 * @param dataSource
	 *            the dataSource to set
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#create(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public Long create(Tag entity) throws DaoException {
		Long id = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement((INSERT_TAG_QUERY), new String[] { tagId });
			preparedStatement.setString(1, entity.getTagName());
			preparedStatement.executeUpdate();

			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				id = generatedKeys.getLong(1);
			}

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			closeDaoResources(dataSource, connection, preparedStatement, null);
		}

		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#update(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public void update(Tag entity) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(UPDATE_TAG_QUERY);
			preparedStatement.setString(1, entity.getTagName());
			preparedStatement.setLong(2, entity.getTagId());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			closeDaoResources(dataSource, connection, preparedStatement, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#getAll()
	 */
	public List<Tag> getAll() throws DaoException {
		Tag tag = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		List<Tag> tagList = new ArrayList<>();

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_ALL_TAGS_QUERY);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tag = new Tag();
				tag.setTagId(resultSet.getLong(tagId));
				tag.setTagName(resultSet.getString(tagName));
				tagList.add(tag); // Add object Tag to list
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}

		return tagList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#delete(long)
	 */
	@Override
	public void delete(long id) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_TAG_QUERY);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			closeDaoResources(dataSource, connection, preparedStatement, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.ITagDao#getSingleTagById(long)
	 */
	public Tag getSingleTagById(long tagId) throws DaoException {
		Tag tag = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_SINGLE_TAG_BY_ID_QUERY);
			preparedStatement.setLong(1, tagId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				tag = new Tag();
				tag.setTagId(tagId);
				tag.setTagName(resultSet.getString(tagName));
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}
		return tag;
	}

	private void closeDaoResources(DataSource dataSource, Connection connection, Statement statement, ResultSet resultSet) throws DaoException {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				throw new DaoException(e.getMessage(), e);
			}

		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				throw new DaoException(e.getMessage(), e);
			}
		}
		DataSourceUtils.releaseConnection(connection, dataSource);
	}
}
