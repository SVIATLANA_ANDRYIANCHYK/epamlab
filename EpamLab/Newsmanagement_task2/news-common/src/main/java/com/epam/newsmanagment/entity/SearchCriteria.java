package com.epam.newsmanagment.entity;

import java.util.List;

/**
 * Sets parameters (criteria) of search
 * 
 * @author Sviatlana_Andryianch
 *
 */
public class SearchCriteria {

	private Long authorId;
	private List<Long> tagList;

	public SearchCriteria() {
	}

	/**
	 * @param authorId
	 * @param tagList
	 */
	public SearchCriteria(long authorId, List<Long> tagList) {
		super();
		this.authorId = authorId;
		this.tagList = tagList;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagList() {
		return tagList;
	}

	public void setTagList(List<Long> tagList) {
		this.tagList = tagList;
	}
}
