package com.epam.newsmanagment.service.implementation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.INewsDao;
import com.epam.newsmanagment.dao.implementations.NewsDao;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ServiceException;

@Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
public class NewsService implements INewsService {

	private INewsDao newsDao;
	private static final Logger LOG = LoggerFactory.getLogger(NewsService.class);

	/**
	 * 
	 */
	public NewsService() {
		super();
	}

	/**
	 * @param dao
	 */
	public NewsService(NewsDao dao) {
		super();
		this.newsDao = dao;
	}

	/**
	 * @return the newsDao
	 */
	public INewsDao getNewsDao() {
		return newsDao;
	}

	/**
	 * @param newsDao
	 *            the newsDao to set
	 */
	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#create(java.lang.Object)
	 */
	@Override
	public long create(News entity) throws ServiceException {
		try {
			return newsDao.create(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(News entity) throws ServiceException {
		try {
			newsDao.update(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#delete(long)
	 */
	@Override
	public void delete(long id) throws ServiceException {
		try {
			newsDao.delete(id);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#getAll()
	 */
	@Override
	public List<News> getAll() throws ServiceException {
		try {
			return newsDao.getAll();
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#getSingleNewsById(long)
	 */
	@Override
	public News getSingleNewsById(long id) throws ServiceException {
		try {
			return newsDao.getSingleNewsById(id);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.INewsService#search(com.epam.
	 * newsmanagment.model.dao.entity.SearchCriteria)
	 */
	@Override
	public List<News> search(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDao.search(searchCriteria);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	public List<News> searchBySearchCriteriaAndPagination(SearchCriteria searchCriteria, int start, int end) throws ServiceException {
		try {
			return newsDao.searchBySearchCriteriaAndPagination(searchCriteria, start, end);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.INewsService#getNewsCount()
	 */
	@Override
	public int getNewsCount() throws ServiceException {
		try {
			return newsDao.getNewsCount();
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#addToNewsAuthor(long,
	 * long)
	 */
	@Override
	public void addToNewsAuthor(long newsId, long authorId) throws ServiceException {
		try {
			newsDao.linkAuthorNews(newsId, authorId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#deleteFromNewsAuthor
	 * (long)
	 */
	@Override
	public void deleteFromNewsAuthor(long newsId) throws ServiceException {
		try {
			newsDao.unlinkAuthorNews(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#deleteFromNewsTag(long)
	 */
	@Override
	public void deleteFromNewsTag(long tagId) throws ServiceException {
		try {
			newsDao.deleteTagFromNews(tagId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.INewsService#addToNewsTag(long,
	 * java.util.List)
	 */
	@Override
	public void addToNewsTag(long newsId, List<Tag> tagList) throws ServiceException {
		try {
			newsDao.addTagToNews(newsId, tagList);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#getAuthorIdByNewsId
	 * (long)
	 */
	@Override
	public Long getAuthorIdByNewsId(long newsId) throws ServiceException {
		try {
			return newsDao.getAuthorIdByNewsId(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#getTagIdListByNewsId
	 * (long)
	 */
	@Override
	public List<Long> getTagIdListByNewsId(long newsId) throws ServiceException {
		try {
			return newsDao.getTagIdListByNewsId(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#getListOfTagsByNewsId
	 * (long)
	 */
	@Override
	public List<Tag> getListOfTagsByNewsId(long newsId) throws ServiceException {
		try {
			return newsDao.getListOfTagsByNewsId(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}

	}

	@Override
	public void deleteTagFromNewsByNewsId(long newsId) throws ServiceException {
		try {
			newsDao.deleteTagFromNewsByNewsId(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}

	}

}
