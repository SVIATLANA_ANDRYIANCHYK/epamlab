package com.epam.newsmanagment.service.VO;

import java.util.List;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.Tag;

public class NewsVO {
	/**
	 * Value object for entity News.
	 */

	private News news;

	private Author author;
	private List<Tag> listTag;
	private List<Comment> commentsList;

	//
	/**
	 * @return the commentsList
	 */
	public List<Comment> getCommentsList() {
		return commentsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NewsVO [news=" + news + ", author=" + author + ", listTag=" + listTag + ", commentsList=" + commentsList + "]";
	}

	/**
	 * @param commentsList
	 *            the commentsList to set
	 */
	public void setCommentsList(List<Comment> commentsList) {
		this.commentsList = commentsList;
	}

	/**
	 * @return the news
	 */
	public News getNews() {
		return news;
	}

	/**
	 * @param news
	 *            the news to set
	 */
	public void setNews(News news) {
		this.news = news;
	}

	/**
	 * @return the author
	 */
	public Author getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(Author author) {
		this.author = author;
	}

	/**
	 * @return the listTag
	 */
	public List<Tag> getListTag() {
		return listTag;
	}

	/**
	 * @param listTag
	 *            the listTag to set
	 */
	public void setListTag(List<Tag> listTag) {
		this.listTag = listTag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((commentsList == null) ? 0 : commentsList.hashCode());
		result = prime * result + ((listTag == null) ? 0 : listTag.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsVO other = (NewsVO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (commentsList == null) {
			if (other.commentsList != null)
				return false;
		} else if (!commentsList.equals(other.commentsList))
			return false;
		if (listTag == null) {
			if (other.listTag != null)
				return false;
		} else if (!listTag.equals(other.listTag))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		return true;
	}

}
