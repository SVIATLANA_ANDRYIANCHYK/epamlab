package com.epam.newsmanagment;

import java.text.ParseException;

import com.epam.newsmanagment.service.ServiceException;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) throws ParseException, ServiceException {
		/*
		 * ApplicationContext ctx = new
		 * ClassPathXmlApplicationContext("application-context.xml");
		 * ServiceManager sm = (ServiceManager) ctx.getBean("serviceManager");
		 * 
		 * NewsVO newsVO = new NewsVO(); Author author = new Author();
		 * author.setAuthorId(1L); author.setAuthorName("name");
		 * 
		 * String creationDateString = "2011-01-01 02:00:00"; SimpleDateFormat
		 * dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); Date creationDate =
		 * dt.parse(creationDateString);
		 * 
		 * News news = new News(1L, "title", "st", "ft", creationDate,
		 * creationDate); List<Tag> tagList = new ArrayList<Tag>(); Tag tag =
		 * new Tag(); tag.setTagName("nnnaa"); tag.setTagId(1L);
		 * tagList.add(tag);
		 * 
		 * newsVO.setAuthor(author); newsVO.setNews(news);
		 * newsVO.setListTag(tagList);
		 * 
		 * sm.addNews(newsVO);
		 */
		// QueryBuilder builder = new QueryBuilder();
		// SearchCriteria sCriteria = new SearchCriteria();
		// Long id = 1L;
		// sCriteria.setAuthorId(id);
		// List<Long> tagList = new ArrayList<Long>();
		// tagList.add(1L);
		// tagList.add(2L);
		// sCriteria.setTagList(tagList);
		// System.out.print(builder.buildPagedNewsQuery(sCriteria, 1, 3));
	}
}
