package com.epam.newsmanagment.service.implementation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.IAuthorDao;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.ServiceException;

@Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
public class AuthorService implements IAuthorService {

	@Autowired
	private IAuthorDao authorDao;
	private static final Logger LOG = LoggerFactory.getLogger(AuthorService.class);

	/**
	 * @param authorDao
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#create(java.lang.Object)
	 */
	@Override
	public long create(Author entity) throws ServiceException {
		try {
			return authorDao.create(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/**
	 * @return the authorDao
	 */
	public IAuthorDao getAuthorDao() {
		return authorDao;
	}

	/**
	 * @param authorDao
	 *            the authorDao to set
	 */
	public void setAuthorDao(IAuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(Author entity) throws ServiceException {
		try {
			authorDao.update(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#delete(long)
	 */
	@Override
	public void delete(long id) throws ServiceException {
		try {
			authorDao.delete(id);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#getAll()
	 */
	@Override
	public List<Author> getAll() throws ServiceException {
		try {
			return authorDao.getAll();
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IAuthorService#getSingleAuthorById
	 * (long)
	 */
	@Override
	public Author getSingleAuthorById(long id) throws ServiceException {
		try {
			return authorDao.getSingleAuthorById(id);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

}
