package com.epam.newsmanagment.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagment.dao.DaoException;

public class DaoUtil {
	public static void closeDaoResources(DataSource dataSource, Connection connection, Statement statement, ResultSet resultSet) throws DaoException {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				throw new DaoException(e.getMessage(), e);
			}

		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				throw new DaoException(e.getMessage(), e);
			}
		}
		DataSourceUtils.releaseConnection(connection, dataSource);
	}
}
