package com.epam.newsmanagment.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;

import com.epam.newsmanagment.dao.implementations.CommentDao;
import com.epam.newsmanagment.entity.Comment;

@Transactional(TransactionMode.ROLLBACK)
@DataSet
public class CommentDaoTest extends UnitilsJUnit4 {

	private static final int EXPECTED_ROW_COUNT = 1;

	@TestDataSource
	private DataSource dataSource;

	/**
	 * 
	 * Checks whether certain object added into database
	 * 
	 * @throws DaoException
	 * @throws ParseException
	 */
	@Test
	public void testCreate() throws ParseException, DaoException {
		CommentDao commentDao = new CommentDao();
		commentDao.setDataSource(dataSource);

		Comment expectedComment = new Comment();
		expectedComment.setCommentId(2L);
		expectedComment.setCommentText("Text 2");
		expectedComment.setNewsId(1L);

		String creationDateString = "2011-01-01 02:00:00";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date creationDate = dt.parse(creationDateString);

		expectedComment.setCreationDate(creationDate);
		Long id = commentDao.create(expectedComment);
		expectedComment.setCommentId(id);
		Comment actualComment = commentDao.getSingleCommentById(id);
		assertObjects(expectedComment, actualComment);

	}

	/**
	 * Checks whether the object was deleted. Checks the existence of the row in
	 * the database before and after deleting.
	 * 
	 * @throws DaoException
	 */
	@Test
	public void testDelete() throws DaoException {
		CommentDao commentDao = new CommentDao();
		commentDao.setDataSource(dataSource);

		Long id = 1L;
		Comment commentBeforeDelete = commentDao.getSingleCommentById(id);
		Assert.assertNotNull(commentBeforeDelete);

		commentDao.delete(id);
		Comment commentAfterDelete = commentDao.getSingleCommentById(id);

		Assert.assertNull(commentAfterDelete);

	}

	/**
	 * Asserts the expected and actual row count.
	 * 
	 * @throws DaoException
	 */
	@Test
	public void testGetAll() throws DaoException {
		CommentDao commentDao = new CommentDao();
		commentDao.setDataSource(dataSource);

		List<Comment> commentList = commentDao.getAll();
		int actualRowCount = commentList.size();

		Assert.assertEquals(EXPECTED_ROW_COUNT, actualRowCount);
	}

	/**
	 * Asserts expected and actual sizes of returned list.
	 * 
	 * @throws DaoException
	 */
	@Test
	public void testGetListOfCommentsByNewsId() throws DaoException {
		CommentDao commentDao = new CommentDao();
		commentDao.setDataSource(dataSource);
		Long newsId = 1L;
		List<Comment> commentList = commentDao.getListOfCommentsByNewsId(newsId);
		int actualListSize = commentList.size();
		int expectedListSize = 1;
		Assert.assertEquals(expectedListSize, actualListSize);
	}

	/**
	 * Asserts expected and actual Comment entity.
	 * 
	 * @throws DaoException
	 * @throws ParseException
	 */
	@Test
	public void testGetSingleCommentById() throws DaoException, ParseException {
		CommentDao commentDao = new CommentDao();
		commentDao.setDataSource(dataSource);

		Long commentId = 1L;
		Comment actualComment = commentDao.getSingleCommentById(commentId);
		String creationDateString = "2011-01-01 02:00:00";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date creationDate = dt.parse(creationDateString);
		Comment expectedComment = new Comment(1L, 1L, "Text 1", creationDate);
		assertObjects(expectedComment, actualComment);
	}

	/**
	 * Tests whether certain object was updated in the database. Asserts the
	 * actually adding updates with the updatable object after update.
	 * 
	 * @throws DaoException
	 */
	@Test
	public void testUpdate() throws ParseException, DaoException {
		CommentDao commentDao = new CommentDao();
		commentDao.setDataSource(dataSource);

		String creationDateString = "2011-01-01 02:00:00";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date creationDate = dt.parse(creationDateString);
		Comment expectedComment = new Comment(1L, 1L, "New Text", creationDate);
		commentDao.update(expectedComment);
		Long commentId = 1L;
		Comment actualComment = commentDao.getSingleCommentById(commentId);
		assertObjects(expectedComment, actualComment);

	}

	/**
	 * Method for comparison of two objects
	 * 
	 * @param firstObj
	 * @param secondObj
	 * @throws DaoException
	 */
	private void assertObjects(Comment firstComment, Comment secondComment) {
		Assert.assertEquals(firstComment.getCommentId(), secondComment.getCommentId());
		Assert.assertEquals(firstComment.getCommentText(), secondComment.getCommentText());
		Assert.assertEquals(firstComment.getNewsId(), secondComment.getNewsId());
		Assert.assertEquals(firstComment.getCreationDate().getTime(), secondComment.getCreationDate().getTime());

	}
}
