package com.epam.newsmanagment.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.implementations.TagDao;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.implementation.TagService;

@ContextConfiguration(locations = { "/application-context.xml" })
@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {
	@Mock
	private TagDao mockTagDao;

	@InjectMocks
	@Autowired
	private TagService tagService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(tagService);
		assertNotNull(mockTagDao);
	}

	@Test
	public void testCreate() throws DaoException, ServiceException {
		when(mockTagDao.create(any(Tag.class))).thenReturn(1L);
		long actualAddedTag = tagService.create(new Tag());
		assertEquals(1L, actualAddedTag);
		verify(mockTagDao, times(1)).create(new Tag());
		verifyNoMoreInteractions(mockTagDao);
	}

	@Test
	public void testDelete() throws ServiceException, DaoException {
		tagService.delete(anyLong());
		verify(mockTagDao, times(1)).delete(anyLong());
		verifyNoMoreInteractions(mockTagDao);
	}

	@Test
	public void testGetAll() throws DaoException, ServiceException {
		List<Tag> expectedTagList = new ArrayList<Tag>();
		when(mockTagDao.getAll()).thenReturn(expectedTagList);
		List<Tag> actualTagList = tagService.getAll();
		assertEquals(expectedTagList, actualTagList);
		verify(mockTagDao, times(1)).getAll();
		verifyNoMoreInteractions(mockTagDao);
	}

	@Test
	public void update() throws ServiceException, DaoException {
		tagService.update(any(Tag.class));
		verify(mockTagDao, times(1)).update(any(Tag.class));
		verifyNoMoreInteractions(mockTagDao);
	}

	@Test
	public void testGetSingletagById() throws DaoException, ServiceException {
		Tag expectedTag = new Tag();
		when(mockTagDao.getSingleTagById(anyLong())).thenReturn(expectedTag);
		Tag actualTag = tagService.getSingleTagById(anyLong());
		assertEquals(expectedTag, actualTag);
		verify(mockTagDao, times(1)).getSingleTagById(anyLong());
		verifyNoMoreInteractions(mockTagDao);
	}
}
