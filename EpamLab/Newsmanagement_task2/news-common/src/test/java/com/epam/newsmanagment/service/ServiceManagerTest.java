package com.epam.newsmanagment.service;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.VO.NewsVO;
import com.epam.newsmanagment.service.implementation.AuthorService;
import com.epam.newsmanagment.service.implementation.CommentService;
import com.epam.newsmanagment.service.implementation.NewsService;
import com.epam.newsmanagment.service.implementation.ServiceManager;
import com.epam.newsmanagment.service.implementation.TagService;

@ContextConfiguration(locations = { "/application-context.xml" })
@RunWith(MockitoJUnitRunner.class)
public class ServiceManagerTest {

	@Mock
	private NewsService mockNewsService;
	@Mock
	private AuthorService mockAuthorService;
	@Mock
	private CommentService mockCommentService;
	@Mock
	private TagService mockTagService;

	@InjectMocks
	@Autowired
	private ServiceManager serviceManager;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(serviceManager);
		assertNotNull(mockAuthorService);
		assertNotNull(mockNewsService);
		assertNotNull(mockCommentService);
	}

	/**
	 * Verifies whether all services were called in method
	 * 
	 * @throws ServiceException
	 * @throws ParseException
	 */
	@Test
	public void testAddNews() throws ServiceException, ParseException {

		NewsVO newsVO = new NewsVO();
		Author author = new Author(1L, "Name", null);
		List<Comment> commentsList = new ArrayList<Comment>();
		String creationDateString = "2011-01-01 02:00:00";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date creationDate = dt.parse(creationDateString);
		Comment comment = new Comment(1L, 1L, "Text", creationDate);
		commentsList.add(comment);
		News news = new News(1L, "title", "st", "ft", creationDate, creationDate);
		List<Tag> tagList = new ArrayList<Tag>();
		Tag tag = new Tag(1L, "name");
		tagList.add(tag);
		newsVO.setAuthor(author);
		newsVO.setCommentsList(commentsList);
		newsVO.setNews(news);
		newsVO.setListTag(tagList);

		long newsId = serviceManager.addNews(newsVO);
		verify(mockNewsService, times(1)).create(newsVO.getNews());
		verify(mockNewsService, times(1)).addToNewsAuthor(newsId, author.getAuthorId());
		verify(mockNewsService, times(1)).addToNewsTag(newsId, newsVO.getListTag());
	}

	/**
	 * Verifies whether all services were called in method
	 * 
	 * @throws ServiceException
	 */
	@Test
	public void testDeleteNews() throws ServiceException {
		Long idNews = anyLong();
		serviceManager.deleteNews(idNews);
		verify(mockNewsService, times(1)).deleteFromNewsAuthor(idNews);
		verify(mockNewsService, times(1)).deleteFromNewsTag(idNews);
		verify(mockNewsService, times(1)).delete(idNews);
	}

	/**
	 * Verifies whether all services were called in method
	 * 
	 * @throws ServiceException
	 * @throws ParseException
	 */
	@Test
	public void tesJtEditNews() throws ServiceException, ParseException {
		String creationDateString = "2011-01-01 02:00:00";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date creationDate = dt.parse(creationDateString);
		News news = new News(1L, "title", "st", "ft", creationDate, creationDate);
		List<Tag> tagList = new ArrayList<Tag>();
		Tag tag = new Tag(1L, "name");
		tagList.add(tag);
		Author author = new Author(1L, "Name", null);
		serviceManager.editNews(news, tagList, author.getAuthorId());
		verify(mockNewsService, times(1)).deleteFromNewsAuthor(news.getId());
		verify(mockNewsService, times(1)).deleteFromNewsTag(news.getId());
		verify(mockNewsService, times(1)).update(news);
		verify(mockNewsService, times(1)).addToNewsAuthor(news.getId(), author.getAuthorId());
		verify(mockNewsService, times(1)).addToNewsTag(news.getId(), tagList);
	}

	/**
	 * Verifies whether all services were called in method
	 * 
	 * @throws ServiceException
	 * @throws ParseException
	 */
	@Test
	public void testGetAllNews() throws ServiceException, ParseException {
		// serviceManager.getAllNews();
		// verify(mockNewsService, times(1)).getNewsCount();
		// verify(mockNewsService, times(1)).getAll();
	}

	/**
	 * Verifies whether all services were called in method
	 * 
	 * @throws ServiceException
	 */
	@Test
	public void testGetNewsListBySearchCriteria() throws ServiceException {
		SearchCriteria sc = any(SearchCriteria.class);
		serviceManager.getNewsListBySearchCriteria(sc);
		verify(mockNewsService, times(1)).search(sc);
	}

	/**
	 * Verifies whether all services were called in method
	 * 
	 * @throws ServiceException
	 */
	@Test
	public void testGetSingleNewsById() throws ServiceException {
		Long id = anyLong();
		mockNewsService.getSingleNewsById(id);
		verify(mockNewsService, times(1)).getSingleNewsById(id);
	}

}
