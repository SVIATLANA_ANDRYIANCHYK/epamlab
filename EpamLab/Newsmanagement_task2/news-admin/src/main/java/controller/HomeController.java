package controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import util.PageBuilder;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.ServiceException;
import com.epam.newsmanagment.service.VO.NewsVO;
import com.epam.newsmanagment.service.implementation.ServiceManager;

@Controller
@RequestMapping(value = "/")
public class HomeController {

	private ITagService tagService;
	private IAuthorService authorService;
	private ServiceManager serviceManager;

	@Inject
	public HomeController(ITagService tagService, IAuthorService authorService, ServiceManager serviceManager) {
		this.tagService = tagService;
		this.authorService = authorService;

		this.serviceManager = serviceManager;

	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String showNewsList(Principal principal) {

		return "login";
	}

	@RequestMapping(value = { "/addNews" }, method = RequestMethod.GET)
	public String showAddNews(Model model) throws ServiceException {
		NewsVO newsVO = new NewsVO();
		News news = new News();
		Date today = new Date();
		Author author = new Author();
		List<Comment> commentsList = new ArrayList<Comment>();
		List<Tag> listTag = tagService.getAll();
		model.addAttribute("tags", listTag);

		List<Author> authorList = authorService.getAll();
		model.addAttribute("authors", authorList);

		newsVO.setNews(news);
		newsVO.setAuthor(author);
		newsVO.setCommentsList(commentsList);
		newsVO.setListTag(listTag);
		newsVO.getNews().setCreationDate(today);
		newsVO.getNews().setModificationDate(today);
		model.addAttribute("newsVO", newsVO);

		return "addNews";
	}

	@RequestMapping(value = { "/authors" }, method = RequestMethod.GET)
	public String showAuthors(Model model) throws ServiceException {
		List<Author> authorList = null;
		authorList = authorService.getAll();
		model.addAttribute("authors", authorList);
		Author author = new Author();
		model.addAttribute("authorToAdd", author);
		return "authors";
	}

	@RequestMapping(value = { "/tags" }, method = RequestMethod.GET)
	public String showTags(Model model) throws ServiceException {
		List<Tag> tagList = null;

		tagList = tagService.getAll();

		model.addAttribute("tags", tagList);
		Tag tag = new Tag();
		model.addAttribute("tagToAdd", tag);
		return "tags";
	}

	@RequestMapping(value = { "/newsList" }, method = RequestMethod.GET)
	public String showNewsList(@RequestParam(value = "page", defaultValue = "1") int currentPage, Model model, HttpSession session) throws ServiceException {
		PageBuilder pageBuilder = new PageBuilder();
		SearchCriteria sc = new SearchCriteria();
		sc = (SearchCriteria) session.getAttribute("searchCriteria");
		int[] pagesNumbersArray = pageBuilder.getNewsPagesArray(serviceManager, sc);
		model.addAttribute("pages", pagesNumbersArray);

		List<NewsVO> newsList = pageBuilder.getCurrentPageNews(serviceManager, currentPage, sc);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("newsList", newsList);

		List<Author> authorList = null;
		authorList = authorService.getAll();
		model.addAttribute("authors", authorList);

		List<Tag> tagList = null;
		tagList = tagService.getAll();
		model.addAttribute("tags", tagList);

		return "newsList";
	}

	@ExceptionHandler(ServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

}
