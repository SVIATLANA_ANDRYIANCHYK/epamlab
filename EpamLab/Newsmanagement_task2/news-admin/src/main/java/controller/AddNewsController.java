package controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.ServiceException;
import com.epam.newsmanagment.service.VO.NewsVO;
import com.epam.newsmanagment.service.implementation.ServiceManager;

@Controller
public class AddNewsController {

	private ServiceManager serviceManager;
	private ITagService tagService;
	private IAuthorService authorService;
	@Autowired
	NewsValidator validator;

	@Inject
	public AddNewsController(ServiceManager serviceManager, ITagService tagService, IAuthorService authorService) {
		this.serviceManager = serviceManager;
		this.tagService = tagService;
		this.authorService = authorService;
	}

	@RequestMapping(value = { "/saveNews" }, method = RequestMethod.POST)
	public String saveNews(@ModelAttribute("newsVO") NewsVO newsVO, Locale locale, @RequestParam(value = "tagId[]", required = false) Long[] tagIdArray, BindingResult result, Model model)
			throws ServiceException {
		validator.validate(newsVO, result);
		if (!result.hasErrors()) {
			Long newsId = newsVO.getNews().getId();
			Date today = new Date();
			newsVO.getNews().setModificationDate(today);
			List<Tag> tagList = new ArrayList<Tag>();
			if (tagIdArray != null) {
				for (int i = 0; i < tagIdArray.length; i++) {
					Tag tag = tagService.getSingleTagById(tagIdArray[i]);
					tagList.add(tag);
				}
			}
			newsVO.setListTag(tagList);
			if (newsId == null) {
				newsId = serviceManager.addNews(newsVO);
			} else {
				serviceManager.editNews(newsVO.getNews(), newsVO.getListTag(), newsVO.getAuthor().getAuthorId());
			}

			return "redirect:/singleNews/" + newsId;
		} else {
			List<Tag> listTag = tagService.getAll();
			model.addAttribute("tags", listTag);

			List<Author> authorList = authorService.getAll();
			model.addAttribute("authors", authorList);
			model.addAttribute("newsVO", newsVO);
			return "addNews";
		}

	}

	@RequestMapping(value = { "/saveNews" }, method = RequestMethod.GET)
	public String language(Model model) throws ServiceException {
		return "redirect:/addNews";
	}

	@ExceptionHandler(ServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

}
