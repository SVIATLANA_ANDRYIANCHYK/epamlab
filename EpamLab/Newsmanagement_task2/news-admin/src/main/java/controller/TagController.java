package controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.ServiceException;
import com.epam.newsmanagment.service.implementation.ServiceManager;

@Controller
public class TagController {

	private ITagService tagService;
	private ServiceManager serviceManager;

	@Inject
	public TagController(ITagService tagService, ServiceManager serviceManager) {
		this.tagService = tagService;
		this.serviceManager = serviceManager;
	}

	@RequestMapping(value = { "/deleteTag" }, method = RequestMethod.GET)
	public String deleteTag(Long tagId) throws ServiceException {
		serviceManager.deleteTag(tagId);
		return "redirect:/tags";
	}

	@RequestMapping(value = { "/editTag/{tagId}" }, method = RequestMethod.GET)
	public String editTag(@PathVariable("tagId") Long tagId, Model model) throws ServiceException {
		Tag tagForUpdate = tagService.getSingleTagById(tagId);
		model.addAttribute("tagForUpdate", tagForUpdate);
		List<Tag> tagList = null;
		try {
			tagList = tagService.getAll();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		model.addAttribute("tags", tagList);
		Tag tag = new Tag();
		model.addAttribute("tagToAdd", tag);
		return "tags";
	}

	@RequestMapping(value = { "/updateTag" }, method = RequestMethod.POST)
	public ModelAndView updateTag(@Valid @ModelAttribute("tagForUpdate") Tag tag, BindingResult result) throws ServiceException {
		if (!result.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView();
			tagService.update(tag);
			modelAndView.setViewName("redirect:/tags");
			return modelAndView;
		} else {
			return formModelUpdate();
		}
	}

	@RequestMapping(value = { "/updateTag" }, method = RequestMethod.GET)
	public String languageUpdate() throws ServiceException {
		return "redirect:/tags";
	}

	@RequestMapping(value = { "/addTag" }, method = RequestMethod.POST)
	public ModelAndView addTag(@Valid @ModelAttribute("tagToAdd") Tag tag, BindingResult result) throws ServiceException {
		if (!result.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView();
			tagService.create(tag);
			modelAndView.setViewName("redirect:/tags");
			return modelAndView;

		} else {
			return formModel();
		}
	}

	@RequestMapping(value = { "/addTag" }, method = RequestMethod.GET)
	public String languageAdd() throws ServiceException {
		return "redirect:/tags";
	}

	@ExceptionHandler(ServiceException.class)
	public String handleException(HttpServletRequest request, ServiceException ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

	private ModelAndView formModel() throws ServiceException {
		ModelAndView model = new ModelAndView();
		List<Tag> tagList = null;

		tagList = tagService.getAll();

		model.addObject("tags", tagList);
		model.setViewName("tags");
		return model;
	}

	private ModelAndView formModelUpdate() throws ServiceException {
		ModelAndView model = new ModelAndView();
		List<Tag> tagList = null;

		tagList = tagService.getAll();

		model.addObject("tags", tagList);
		model.setViewName("tags");
		Tag tag = new Tag();
		model.addObject("tagToAdd", tag);
		return model;
	}

}
