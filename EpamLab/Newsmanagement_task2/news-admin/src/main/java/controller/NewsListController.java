package controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.ServiceException;
import com.epam.newsmanagment.service.VO.NewsVO;
import com.epam.newsmanagment.service.implementation.ServiceManager;

@Controller
public class NewsListController {

	private IAuthorService authorService;

	private ITagService tagService;
	private ServiceManager serviceManager;

	@Inject
	public NewsListController(IAuthorService authorService, ITagService tagService, ServiceManager serviceManager) {
		this.authorService = authorService;

		this.tagService = tagService;
		this.serviceManager = serviceManager;
	}

	@RequestMapping(value = { "/editNews/{newsId}" }, method = RequestMethod.GET)
	public String editNews(@PathVariable("newsId") Long newsId, Model model) throws ServiceException {

		NewsVO newsVO = serviceManager.getSingleNewsById(newsId);

		Date today = new Date();

		List<Comment> commentsList = new ArrayList<Comment>();
		List<Tag> listTag = tagService.getAll();
		model.addAttribute("tags", listTag);

		List<Author> authorList = authorService.getAll();
		model.addAttribute("authors", authorList);

		newsVO.setCommentsList(commentsList);

		newsVO.getNews().setModificationDate(today);
		model.addAttribute("newsVO", newsVO);
		return "addNews";
	}

	@RequestMapping(value = { "/filter" }, method = RequestMethod.POST)
	public String filterNews(HttpServletRequest request, @RequestParam(value = "tagId[]", required = false) Long[] tagIdArray, @RequestParam(value = "author", required = false) Long authorId)
			throws ServiceException {

		List<Long> tagList = new ArrayList<Long>();
		if (tagIdArray != null) {
			for (int i = 0; i < tagIdArray.length; i++) {
				tagList.add(tagIdArray[i]);
			}
		}
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagList(tagList);
		request.getSession().setAttribute("searchCriteria", searchCriteria);
		return "redirect:/newsList";
	}

	@RequestMapping(value = { "/reset" }, method = RequestMethod.POST)
	public String reset(HttpSession session) {
		session.setAttribute("searchCriteria", null);
		return "redirect:/newsList";
	}

	@RequestMapping(value = { "/deleteNews" }, method = RequestMethod.POST)
	public String deleteNews(@RequestParam(value = "newsToDelete[]", required = false) Long[] newsToDeleteArray) throws ServiceException {
		if (newsToDeleteArray != null) {
			for (int i = 0; i < newsToDeleteArray.length; i++) {
				serviceManager.deleteNews(newsToDeleteArray[i]);
			}
		}
		return "redirect:/newsList";
	}

	@RequestMapping(value = { "/singleNews/{newsId}" }, method = RequestMethod.GET)
	public String showSingleNews(@PathVariable("newsId") Long newsId, Model model) throws ServiceException {
		NewsVO newsVO = serviceManager.getSingleNewsById(newsId);
		model.addAttribute("newsVO", newsVO);

		return "single-news";
	}

	@ExceptionHandler(ServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

}
