package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.service.ServiceException;
import com.epam.newsmanagment.service.VO.NewsVO;
import com.epam.newsmanagment.service.implementation.ServiceManager;

public class PageBuilder {
	public int[] getNewsPagesArray(ServiceManager serviceManager, SearchCriteria sc) throws ServiceException {

		Properties property = new Properties();
		String fileName = "settings.properties";
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
		try {
			property.load(inputStream);
		} catch (IOException e) {
			throw new ServiceException(e.getMessage(), e);
		}

		List<NewsVO> newsList = serviceManager.getNewsListBySearchCriteria(sc);

		int newsCount = newsList.size();
		int newsOnPageCount = Integer.parseInt(property.getProperty("news-on-page"));

		int pagesCount = newsCount / newsOnPageCount;
		if ((newsCount - pagesCount * newsOnPageCount) != 0) {
			pagesCount += 1;
		}
		int[] pagesArray = new int[pagesCount];
		for (int i = 0; i < pagesArray.length; i++) {
			pagesArray[i] = i + 1;
		}
		return pagesArray;
	}

	public List<NewsVO> getCurrentPageNews(ServiceManager serviceManager, int currentPage, SearchCriteria sc) throws ServiceException {
		Properties property = new Properties();
		String fileName = "settings.properties";
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
		try {
			property.load(inputStream);
		} catch (IOException e) {
			throw new ServiceException(e.getMessage(), e);
		}

		int newsOnPageCount = Integer.parseInt(property.getProperty("news-on-page"));
		int start = (currentPage - 1) * newsOnPageCount + 1;
		int end = start + newsOnPageCount - 1;
		List<NewsVO> newsList = serviceManager.getAllNews(sc, start, end);

		return newsList;
	}
}
