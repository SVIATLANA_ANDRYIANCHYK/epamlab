package com.epam.newsmanagement;


import com.epam.newsmanagement.entity.Author;
import org.apache.cxf.jaxrs.client.WebClient;
import org.eclipse.persistence.jpa.jpql.model.query.LikeExpressionStateObject;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sviatlana_Andryianch on 1/21/2016.
 */
public class Test {
    public static void main(String[] args) {
        String baseURL = "http://localhost:8099/news-admin/rest";

        String detailsURL = "/home/";

        String username = "authors";

        WebClient client = WebClient.create(baseURL);
        WebClient getClient = client.path(detailsURL).path(username).accept(MediaType.APPLICATION_XML);

        List<Author> authorList = getClient.get(new GenericType<List<Author>>(){});

        System.out.print(authorList);
    }
}
