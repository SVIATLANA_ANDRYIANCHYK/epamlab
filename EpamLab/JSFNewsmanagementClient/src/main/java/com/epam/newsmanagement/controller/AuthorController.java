package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.jaxrs.client.AuthorClient;

//import javax.faces.bean.RequestScoped;
//import javax.faces.context.FacesContext;
//import javax.inject.Inject;
//import javax.inject.Named;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by Sviatlana_Andryianch on 1/21/2016.
// */
//@Named(value = "authorController")
//@RequestScoped
//public class AuthorController {
//    @Inject
//    private AuthorClient authorClient = new AuthorClient();
//    private Author author = new Author();
//    List<Author> authors = new ArrayList<Author>();
//    FacesContext facesContext = FacesContext.getCurrentInstance();
//
//    public List<Author> getAllAuthors() {
//        authors = authorClient.getAuthors();
//        return authors;
//    }
//}
