package com.epam.newsmanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "tag")
@SequenceGenerator(name = "TAG_SEQ", sequenceName = "TAG_SEQ")
@JsonIgnoreProperties
@XmlRootElement
public class Tag implements Serializable, IEntity {

	/**
	 * For deserialization with no exception after modification.
	 */
	private static final long serialVersionUID = -4741660615976012459L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAG_SEQ")
	@Column(name = "tag_id", precision = 0)
	private Long tagId; // Primary key
	@NotEmpty
	@NotNull
	@Size(max = 15)
	@Column(name = "tag_name")
	private String tagName;

//	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//	@JoinTable(name = "news_tag", joinColumns = { @JoinColumn(name = "tag_id") }, inverseJoinColumns = { @JoinColumn(name = "news_id") })
//	@JsonBackReference
//	private Set<News> newsSet;

	/**
	 * @return the newsSet
	 */
//	public Set<News> getNewsSet() {
//		return newsSet;
//	}
//
//	/**
//	 * @param newsSet
//	 *            the newsSet to set
//	 */
//	public void setNewsSet(Set<News> newsSet) {
//		this.newsSet = newsSet;
//	}

	/**
	 * Default constructor
	 */
	public Tag() {
	}

	/**
	 * Parameterized constructor
	 * 
	 * @param tagId
	 * @param tagName
	 */
	public Tag(Long tagId, String tagName) {
		super();
		this.tagId = tagId;
		this.tagName = tagName;
	}

	/**
	 * @return the tagId
	 */
	public Long getTagId() {
		return tagId;
	}

	/**
	 * @param tagId
	 *            the tagId to set
	 */
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	/**
	 * @return the tagName
	 */
	public String getTagName() {
		return tagName;
	}

	/**
	 * @param tagName
	 *            the tagName to set
	 */
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * @see java.lang.Object#hashCode()
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (tagId == null) {
			if (other.tagId != null)
				return false;
		} else if (!tagId.equals(other.tagId))
			return false;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

	/*
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Tag [tagId=" + tagId + ", tagName=" + tagName + "]";
	}

}

// package com.epam.newsmanagment.entity;
//
// import java.io.Serializable;
//
// import javax.persistence.Column;
// import javax.persistence.Entity;
// import javax.persistence.GeneratedValue;
// import javax.persistence.GenerationType;
// import javax.persistence.Id;
// import javax.persistence.SequenceGenerator;
// import javax.persistence.Table;
// import javax.validation.constraints.NotNull;
// import javax.validation.constraints.Size;
//
// import org.hibernate.validator.constraints.NotEmpty;
//
// @SequenceGenerator(name = "TAG_SEQ", sequenceName = "TAG_SEQ")
// @Entity
// @Table(name = "Tag")
// public class Tag implements Serializable, IEntity {
//
// /**
// * For deserialization with no exception after modification.
// */
// private static final long serialVersionUID = -4741660615976012459L;
// @Id
// @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAG_SEQ")
// @Column(name = "TAG_ID", precision = 0)
// private Long tagId; // Primary key
// @NotEmpty
// @NotNull
// @Size(max = 15)
// @Column(name = "TAG_NAME")
// private String tagName;
//
// // @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
// // @JoinTable(name = "NEWS_TAG", joinColumns = { @JoinColumn(name =
// // "TAG_ID") }, inverseJoinColumns = { @JoinColumn(name = "NEWS_ID") })
// // private Set<News> newsSet;
//
// /**
// * @return the newsSet
// */
// // public Set<News> getNewsSet() {
// // return newsSet;
// // }
// //
// // /**
// // * @param newsSet
// // * the newsSet to set
// // */
// // public void setNewsSet(Set<News> newsSet) {
// // this.newsSet = newsSet;
// // }
//
// /**
// * Default constructor
// */
// public Tag() {
// }
//
// /**
// * Parameterized constructor
// *
// * @param tagId
// * @param tagName
// */
// public Tag(Long tagId, String tagName) {
// super();
// this.tagId = tagId;
// this.tagName = tagName;
// }
//
// /**
// * @return the tagId
// */
// public Long getTagId() {
// return tagId;
// }
//
// /**
// * @param tagId
// * the tagId to set
// */
// public void setTagId(Long tagId) {
// this.tagId = tagId;
// }
//
// /**
// * @return the tagName
// */
// public String getTagName() {
// return tagName;
// }
//
// /**
// * @param tagName
// * the tagName to set
// */
// public void setTagName(String tagName) {
// this.tagName = tagName;
// }
//
// /**
// * @return the serialversionuid
// */
// public static long getSerialversionuid() {
// return serialVersionUID;
// }
//
// /*
// * @see java.lang.Object#hashCode()
// */
//
// /*
// * (non-Javadoc)
// *
// * @see java.lang.Object#hashCode()
// */
// @Override
// public int hashCode() {
// final int prime = 31;
// int result = 1;
// result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
// result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
// return result;
// }
//
// /*
// * (non-Javadoc)
// *
// * @see java.lang.Object#equals(java.lang.Object)
// */
// @Override
// public boolean equals(Object obj) {
// if (this == obj)
// return true;
// if (obj == null)
// return false;
// if (getClass() != obj.getClass())
// return false;
// Tag other = (Tag) obj;
// if (tagId == null) {
// if (other.tagId != null)
// return false;
// } else if (!tagId.equals(other.tagId))
// return false;
// if (tagName == null) {
// if (other.tagName != null)
// return false;
// } else if (!tagName.equals(other.tagName))
// return false;
// return true;
// }
//
// /*
// * @see java.lang.Object#toString()
// */
// @Override
// public String toString() {
// return "Tag [tagId=" + tagId + ", tagName=" + tagName + "]";
// }
//
// }
