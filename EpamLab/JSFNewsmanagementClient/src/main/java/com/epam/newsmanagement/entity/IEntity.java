package com.epam.newsmanagement.entity;

/**
 * All entities implement this interface to have the same type.
 * 
 * @author Sviatlana_Andryianch
 *
 */
public interface IEntity {

}
