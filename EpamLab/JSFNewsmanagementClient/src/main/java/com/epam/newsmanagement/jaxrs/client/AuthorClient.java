package com.epam.newsmanagement.jaxrs.client;

import com.epam.newsmanagement.entity.Author;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.UriBuilder;

import java.net.URI;
import java.util.List;

/**
 * Created by Sviatlana_Andryianch on 1/21/2016.
 */
public class AuthorClient {
    private URI uri;
    private Client client;

    public AuthorClient(){

        uri = UriBuilder
                .fromUri("http://localhost:8099/news-admin/rest/home/authors")
                .port(8080).build();
        client = ClientBuilder.newClient();
    }

    public List<Author> getAuthors(){
        List<Author> authors = client.target(uri)
                .request()
                .get(new GenericType<List<Author>>(){});
        return authors;
    }

    public void close(){
        client.close();
    }

}
