package com.epam.newsmanagement.bean;

import com.epam.newsmanagement.entity.Author;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Sviatlana_Andryianch on 1/25/2016.
 */
@Component
@ManagedBean(name = "authorBean")
@SessionScoped
public class AuthorBean {

    public List<Author> getAuthors(){
        String baseURL = "http://localhost:8099/news-admin/rest";

        String detailsURL = "/home/";

        String username = "authors";

        WebClient client = WebClient.create(baseURL);
        WebClient getClient = client.path(detailsURL).path(username).accept(MediaType.APPLICATION_XML);

        List<Author> authorList = getClient.get(new GenericType<List<Author>>(){});
        return authorList;
    }

    public String test(){
        return "Hello";
    }
}
