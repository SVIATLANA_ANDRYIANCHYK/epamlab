package com.epam.powermock;

public class ExampleHandler {

	public void method1() {
		// do something
		method2();
	}

	public void method2() {
		throw new UnsupportedOperationException();
	}

	public void method3() {
		// do something
		method4("abc");
	}

	private String method4(String str) {
		throw new UnsupportedOperationException();
	}

}
