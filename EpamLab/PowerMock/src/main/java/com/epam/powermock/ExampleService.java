package com.epam.powermock;

public class ExampleService {
	
	private ExampleHandler handler;
	
	public String sayMessage() {
		String message = ExampleUtil.sayHello();
		return message += "Have a good day.";
	}
	
	public void printMessage() {
		ExampleUtil.printHello();
		handler.method1();
	}
	
	public String doSomething() {
		handler.method3();
		return "Done!";
	}

	public String strangeMethod() {
		privateMethod();
		return "Done!";
	}

	private String privateMethod() {
		throw new UnsupportedOperationException();
	}
}
