package com.epam.powermock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.times;
import static org.junit.Assert.assertNotNull;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.support.membermodification.MemberMatcher.method;

/**
 * Created by Sviatlana_Andryianch on 2/29/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ExampleHandler.class, ExampleUtil.class, ExampleService.class })
public class ExampleServiceTest {

    public static final String ACTUAL_RESULT_STRING = "Done!";

    private ExampleHandler exampleHandler = PowerMockito.mock(ExampleHandler.class);
    @InjectMocks
    private ExampleService exampleService = PowerMockito.spy(new ExampleService());


    @Before
    public void setupMock() {
        assertNotNull(exampleService);
        assertNotNull(exampleHandler);
    }


    @Test
    public void sayMessageTest(){
        //Given
        String expectedString = "Have a good day.";
        //Record behavior
        PowerMockito.mockStatic(ExampleUtil.class);
        PowerMockito.when(ExampleUtil.sayHello())
                .thenReturn("");
        //When
        String actualString = exampleService.sayMessage();
        //Then
        assertEquals(expectedString, actualString);
        PowerMockito.verifyStatic();
        ExampleUtil.sayHello();

    }

    @Test
    public void printMessageTest(){
        //Record behavior
        PowerMockito.doNothing().when(exampleHandler).method1();
        //When
        exampleService.printMessage();
        //Then
        verify(exampleHandler, times(1)).method1();
        PowerMockito.verifyStatic(times(1));
        ExampleUtil.printHello();
    }

    @Test
    public void doSomethingTest(){
        //Record behavior
        PowerMockito.doNothing().when(exampleHandler).method3();
        //When
        String actualString = exampleService.doSomething();
        //Then
        assertEquals(ACTUAL_RESULT_STRING, actualString);
        verify(exampleHandler, times(1)).method3();
        verifyNoMoreInteractions(exampleHandler);
    }



    @Test
    public void strangeMethodTest() throws Exception {

        ExampleService spyService = PowerMockito.spy(new ExampleService());

        //Record Behaviour
        doReturn("test").when(spyService, "privateMethod");

        //When
        String result = spyService.strangeMethod();

        //Then
        verifyPrivate(spyService).invoke("privateMethod");
        assertEquals("Done!", result);
    }

}
