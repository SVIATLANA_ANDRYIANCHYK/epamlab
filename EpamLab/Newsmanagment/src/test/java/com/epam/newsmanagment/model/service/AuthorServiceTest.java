package com.epam.newsmanagment.model.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.newsmanagment.model.dao.DaoException;
import com.epam.newsmanagment.model.dao.entity.Author;
import com.epam.newsmanagment.model.dao.implementations.AuthorDao;
import com.epam.newsmanagment.model.service.implementation.AuthorService;

@ContextConfiguration(locations = { "/application-context.xml" })
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
	@Mock
	private AuthorDao mockAuthorDao;

	@InjectMocks
	@Autowired
	private AuthorService authorService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(authorService);
		assertNotNull(mockAuthorDao);
	}

	@Test
	public void testCreate() throws DaoException, ServiceException {
		when(mockAuthorDao.create(any(Author.class))).thenReturn(1L);
		long actualAddedAuthor = authorService.create(new Author());
		assertEquals(1L, actualAddedAuthor);
		verify(mockAuthorDao, times(1)).create(new Author());
		verifyNoMoreInteractions(mockAuthorDao);
	}

	@Test
	public void testDelete() throws ServiceException, DaoException {
		authorService.delete(anyLong());
		verify(mockAuthorDao, times(1)).delete(anyLong());
		verifyNoMoreInteractions(mockAuthorDao);
	}

	@Test
	public void testGetAll() throws DaoException, ServiceException {
		List<Author> expectedAuthorList = new ArrayList<Author>();
		when(mockAuthorDao.getAll()).thenReturn(expectedAuthorList);
		List<Author> actualAuthorList = authorService.getAll();
		assertEquals(expectedAuthorList, actualAuthorList);
		verify(mockAuthorDao, times(1)).getAll();
		verifyNoMoreInteractions(mockAuthorDao);
	}

	@Test
	public void testGetSingleAuthorById() throws DaoException, ServiceException {
		Author expectedAuthor = new Author();
		when(mockAuthorDao.getSingleAuthorById(anyLong())).thenReturn(expectedAuthor);
		Author actualAuthor = authorService.getSingleAuthorById(anyLong());
		assertEquals(expectedAuthor, actualAuthor);
		verify(mockAuthorDao, times(1)).getSingleAuthorById(anyLong());
		verifyNoMoreInteractions(mockAuthorDao);
	}

	@Test
	public void testUpdate() throws ServiceException, DaoException {
		authorService.update(any(Author.class));
		verify(mockAuthorDao, times(1)).update(any(Author.class));
		verifyNoMoreInteractions(mockAuthorDao);
	}
}
