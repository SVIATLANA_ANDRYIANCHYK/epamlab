package com.epam.newsmanagment.model.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.newsmanagment.model.dao.DaoException;
import com.epam.newsmanagment.model.dao.entity.Comment;
import com.epam.newsmanagment.model.dao.implementations.CommentDao;
import com.epam.newsmanagment.model.service.implementation.CommentService;

@ContextConfiguration(locations = { "/application-context.xml" })
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
	@Mock
	private CommentDao mockCommentDao;

	@InjectMocks
	@Autowired
	private CommentService commentService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(commentService);
		assertNotNull(mockCommentDao);
	}

	@Test
	public void testCreate() throws DaoException, ServiceException {
		when(mockCommentDao.create(any(Comment.class))).thenReturn(1L);
		long actualAddedAuthor = commentService.create(new Comment());
		assertEquals(1L, actualAddedAuthor);
		verify(mockCommentDao, times(1)).create(new Comment());
		verifyNoMoreInteractions(mockCommentDao);
	}

	@Test
	public void testDelete() throws ServiceException, DaoException {
		commentService.delete(anyLong());
		verify(mockCommentDao, times(1)).delete(anyLong());
		verifyNoMoreInteractions(mockCommentDao);
	}

	@Test
	public void testGetAll() throws DaoException, ServiceException {
		List<Comment> expectedCommentList = new ArrayList<Comment>();
		when(mockCommentDao.getAll()).thenReturn(expectedCommentList);
		List<Comment> actualCommentList = commentService.getAll();
		assertEquals(expectedCommentList, actualCommentList);
		verify(mockCommentDao, times(1)).getAll();
		verifyNoMoreInteractions(mockCommentDao);
	}

	@Test
	public void testGetListOfCommentsByNewsId() throws DaoException, ServiceException {
		List<Comment> expectedCommentList = new ArrayList<Comment>();
		when(mockCommentDao.getListOfCommentsByNewsId(anyLong())).thenReturn(expectedCommentList);
		List<Comment> actualCommentList = commentService.getListOfCommentsByNewsId(anyLong());
		assertEquals(expectedCommentList, actualCommentList);
		verify(mockCommentDao, times(1)).getListOfCommentsByNewsId(anyLong());
		verifyNoMoreInteractions(mockCommentDao);
	}

	@Test
	public void testGetSingleCommentById() throws DaoException, ServiceException {
		Comment expectedComment = new Comment();
		when(mockCommentDao.getSingleCommentById(anyLong())).thenReturn(expectedComment);
		Comment actualComment = commentService.getSingleCommentById(anyLong());
		assertEquals(expectedComment, actualComment);
		verify(mockCommentDao, times(1)).getSingleCommentById(anyLong());
		verifyNoMoreInteractions(mockCommentDao);
	}

	@Test
	public void update() throws ServiceException, DaoException {
		commentService.update(any(Comment.class));
		verify(mockCommentDao, times(1)).update(any(Comment.class));
		verifyNoMoreInteractions(mockCommentDao);
	}

}
