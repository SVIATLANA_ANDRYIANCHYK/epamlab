package com.epam.newsmanagment.model.dao;

import java.text.ParseException;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;

import com.epam.newsmanagment.model.dao.entity.Tag;
import com.epam.newsmanagment.model.dao.implementations.TagDao;

@Transactional(TransactionMode.ROLLBACK)
@DataSet
public class TagDaoTest extends UnitilsJUnit4 {

	@TestDataSource
	private DataSource dataSource;

	private static final int EXPECTED_ROW_COUNT = 3;

	/**
	 * 
	 * Checks whether certain object added into database
	 * 
	 * @throws DaoException
	 * @throws ParseException
	 */
	@Test
	public void testCreate() throws DaoException {
		TagDao dao = new TagDao();
		dao.setDataSource(dataSource);

		Tag tag = new Tag(4L, "Name 4");
		Long id = dao.create(tag);
		tag.setTagId(id);
		Tag actualTag = dao.getSingleTagById(id);
		Assert.assertEquals(tag, actualTag);
	}

	/**
	 * Checks whether the object was deleted. Checks the existence of the row in
	 * the database before and after deleting.
	 * 
	 * @throws DaoException
	 */
	@Test
	public void testDelete() throws DaoException {
		TagDao dao = new TagDao();
		dao.setDataSource(dataSource);

		Long tagId = 1L;
		Tag tagBeforeDelete = dao.getSingleTagById(tagId);
		Assert.assertNotNull(tagBeforeDelete);

		dao.delete(tagId);

		Tag tagAfterDelete = dao.getSingleTagById(tagId);
		Assert.assertNull(tagAfterDelete);
	}

	/**
	 * Asserts the expected and actual row count.
	 * 
	 * @throws DaoException
	 */
	@Test
	public void testGetAll() throws DaoException {
		TagDao dao = new TagDao();
		dao.setDataSource(dataSource);
		List<Tag> tagList = dao.getAll();
		int actualRowCount = tagList.size();
		Assert.assertEquals(EXPECTED_ROW_COUNT, actualRowCount);
	}

	/**
	 * Asserts expected and actual Tag entity.
	 * 
	 * @throws DaoException
	 * @throws ParseException
	 */
	@Test
	public void testGetSingleTagById() throws DaoException {
		TagDao dao = new TagDao();
		dao.setDataSource(dataSource);

		Tag expectedTag = new Tag(1L, "Name 1");
		long tagId = 1L;
		Tag actualTag = dao.getSingleTagById(tagId);
		Assert.assertEquals(expectedTag, actualTag);
	}

	/**
	 * Tests whether certain object was updated in the database. Asserts the
	 * actually adding updates with the updatable object after update.
	 * 
	 * @throws DaoException
	 */
	@Test
	public void testUpdate() throws DaoException {
		TagDao dao = new TagDao();
		dao.setDataSource(dataSource);

		Tag newTag = new Tag(1L, "Name 4");
		dao.update(newTag);
		Long tagId = 1L;
		Tag actualTag = dao.getSingleTagById(tagId);
		Assert.assertEquals(newTag, actualTag);
	}
}
