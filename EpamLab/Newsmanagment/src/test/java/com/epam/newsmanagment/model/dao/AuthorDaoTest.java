package com.epam.newsmanagment.model.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sql.DataSource;

import junit.framework.Assert;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;

import com.epam.newsmanagment.model.dao.entity.Author;
import com.epam.newsmanagment.model.dao.implementations.AuthorDao;

@Transactional(TransactionMode.ROLLBACK)
@DataSet
public class AuthorDaoTest extends UnitilsJUnit4 {

	private static final int EXPECTED_AUTHOR_COUNT = 2;

	@TestDataSource
	private DataSource dataSource;

	/**
	 * 
	 * Checks whether certain object added into database
	 * 
	 * @throws DaoException
	 * @throws ParseException
	 */
	@Test
	public void testCreate() throws DaoException, ParseException {
		AuthorDao authorDao = new AuthorDao();
		authorDao.setDataSource(dataSource);

		Author insertedAuthor = new Author();
		insertedAuthor.setAuthorId(2L);
		insertedAuthor.setAuthorName("name1");

		String expiredDateString = "2011-01-01 02:00:00";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date expiredDate = dt.parse(expiredDateString);

		insertedAuthor.setExpired(expiredDate);

		long id = authorDao.create(insertedAuthor);
		insertedAuthor.setAuthorId(id);

		Author receivedAuthor = authorDao.getSingleAuthorById(id);
		assertObjects(insertedAuthor, receivedAuthor);
	}

	/**
	 * Tests whether certain object was updated in the database. Asserts the
	 * actually adding updates with the updatable object after update.
	 * 
	 * @throws DaoException
	 */
	@Test
	public void testUpdate() throws DaoException {
		AuthorDao authorDao = new AuthorDao();
		authorDao.setDataSource(dataSource);

		Author author = new Author(1L, "name_changed", null);
		authorDao.update(author);

		assertObjects(author, authorDao.getSingleAuthorById(author.getAuthorId()));
	}

	/**
	 * Asserts the expected and actual row count.
	 * 
	 * @throws DaoException
	 */
	@Test
	public void testGetAll() throws DaoException {
		AuthorDao authorDao = new AuthorDao();
		authorDao.setDataSource(dataSource);
		Assert.assertEquals(EXPECTED_AUTHOR_COUNT, authorDao.getAll().size());

	}

	/**
	 * Checks whether the value of expiration date was set to the certain author
	 * 
	 * @throws DaoException
	 */
	@Test
	public void testDelete() throws DaoException {
		AuthorDao authorDao = new AuthorDao();
		authorDao.setDataSource(dataSource);
		long authorId = 1L;
		authorDao.delete(authorId);
		Assert.assertNotNull(authorDao.getSingleAuthorById(authorId).getExpired());

	}

	/**
	 * Asserts expected and actual Author entity.
	 * 
	 * @throws ParseException
	 * @throws DaoException
	 */
	@Test
	public void testGetSingleAuthorById() throws ParseException, DaoException {
		AuthorDao authorDao = new AuthorDao();
		authorDao.setDataSource(dataSource);

		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorId(1L);
		expectedAuthor.setAuthorName("name1");

		String expiredDateString = "2011-01-01 02:00:00";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date expiredDate = dt.parse(expiredDateString);
		expectedAuthor.setExpired(expiredDate);

		long requiredAuthorId = 1L;
		assertObjects(expectedAuthor, authorDao.getSingleAuthorById(requiredAuthorId));

	}

	/**
	 * Method for comparison of two objects
	 * 
	 * @param firstObj
	 * @param secondObj
	 * @throws DaoException
	 */
	private void assertObjects(Author firstObj, Author secondObj) {
		Assert.assertEquals(firstObj.getAuthorId(), secondObj.getAuthorId());
		Assert.assertEquals(firstObj.getAuthorName(), secondObj.getAuthorName());
		if (firstObj.getExpired() != null && secondObj.getExpired() != null) {
			Assert.assertEquals(firstObj.getExpired().getTime(), secondObj.getExpired().getTime());
		}
	}
}
