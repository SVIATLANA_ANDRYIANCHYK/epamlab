package com.epam.newsmanagment.model.service.VO;

import java.util.List;

import com.epam.newsmanagment.model.dao.entity.Author;
import com.epam.newsmanagment.model.dao.entity.Comment;
import com.epam.newsmanagment.model.dao.entity.News;
import com.epam.newsmanagment.model.dao.entity.Tag;

public class NewsVO {
	/**
	 * Value object for entity News.
	 */

	private News news;
	private Author author;
	private List<Tag> listTag;
	private List<Comment> commentsList;

	/**
	 * @return the commentsList
	 */
	public List<Comment> getCommentsList() {
		return commentsList;
	}

	/**
	 * @param commentsList
	 *            the commentsList to set
	 */
	public void setCommentsList(List<Comment> commentsList) {
		this.commentsList = commentsList;
	}

	/**
	 * @return the news
	 */
	public News getNews() {
		return news;
	}

	/**
	 * @param news
	 *            the news to set
	 */
	public void setNews(News news) {
		this.news = news;
	}

	/**
	 * @return the author
	 */
	public Author getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(Author author) {
		this.author = author;
	}

	/**
	 * @return the listTag
	 */
	public List<Tag> getListTag() {
		return listTag;
	}

	/**
	 * @param listTag
	 *            the listTag to set
	 */
	public void setListTag(List<Tag> listTag) {
		this.listTag = listTag;
	}

}
