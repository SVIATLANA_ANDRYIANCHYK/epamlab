package com.epam.newsmanagment.model.service.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.model.dao.entity.Author;
import com.epam.newsmanagment.model.dao.entity.Comment;
import com.epam.newsmanagment.model.dao.entity.News;
import com.epam.newsmanagment.model.dao.entity.SearchCriteria;
import com.epam.newsmanagment.model.dao.entity.Tag;
import com.epam.newsmanagment.model.service.ServiceException;
import com.epam.newsmanagment.model.service.VO.NewsVO;

@Transactional
public class ServiceManager {

	private NewsService newsService;
	private AuthorService authorService;
	private CommentService commentService;
	private TagService tagService;

	/**
	 * @return the tagService
	 */
	public TagService getTagService() {
		return tagService;
	}

	/**
	 * @param tagService
	 *            the tagService to set
	 */
	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	/**
	 * Adds News value object to database.
	 * 
	 * @param newsVO
	 *            to add
	 * @return id if added news
	 * @throws ServiceException
	 */
	public long addNews(NewsVO newsVO) throws ServiceException {

		long authorId = newsVO.getAuthor().getAuthorId();
		long newsId = newsService.create(newsVO.getNews());
		newsService.addToNewsTag(newsId, newsVO.getListTag());
		newsService.addToNewsAuthor(newsId, authorId);
		return newsId;
	}

	/**
	 * Deletes News value object from database.
	 * 
	 * @param newsId
	 *            to delete
	 * @throws ServiceException
	 */
	public void deleteNews(long newsId) throws ServiceException {
		newsService.deleteFromNewsTag(newsId);
		newsService.deleteFromNewsAuthor(newsId);
		newsService.delete(newsId);
	}

	/**
	 * Edits news value object.
	 * 
	 * @param news
	 *            to edit
	 * @param listTag
	 *            to edit
	 * @param authorId
	 *            to edit
	 * @throws ServiceException
	 */
	public void editNews(News news, List<Tag> listTag, long authorId) throws ServiceException {
		long newsId = news.getId();
		newsService.deleteFromNewsAuthor(newsId);
		newsService.deleteFromNewsTag(newsId);

		newsService.update(news);
		newsService.addToNewsAuthor(newsId, authorId);
		newsService.addToNewsTag(newsId, listTag);

	}

	/**
	 * Gets the list of all news value object.
	 * 
	 * @return the list of News value object.
	 * @throws ServiceException
	 */
	public List<NewsVO> getAllNews() throws ServiceException {
		List<NewsVO> newsVOList = new ArrayList<>();
		NewsVO newsVO = new NewsVO();
		int size = newsService.getNewsCount();
		List<News> listNews = newsService.getAll();
		for (int i = 0; i < size; i++) {

			long newsId = listNews.get(i).getId();
			long authorId = newsService.getAuthorIdByNewsId(newsId);
			Author author = authorService.getSingleAuthorById(authorId);
			List<Comment> commentsList = commentService.getListOfCommentsByNewsId(newsId);
			List<Tag> tagsList = newsService.getListOfTagsByNewsId(newsId);

			newsVO.setNews(listNews.get(i));
			newsVO.setAuthor(author);
			newsVO.setCommentsList(commentsList);
			newsVO.setListTag(tagsList);

			newsVOList.add(newsVO);
		}

		return newsVOList;
	}

	/**
	 * Gets the single news value object from database by certain id of news.
	 * 
	 * @param newsId
	 *            to get
	 * @return news value object.
	 * @throws ServiceException
	 */
	public NewsVO getSingleNewsById(long newsId) throws ServiceException {
		NewsVO newsVO = new NewsVO();

		long authorId = newsService.getAuthorIdByNewsId(newsId);
		News news = newsService.getSingleNewsById(newsId);
		Author author = authorService.getSingleAuthorById(authorId);
		List<Comment> commentsList = commentService.getListOfCommentsByNewsId(newsId);
		List<Tag> tagsList = newsService.getListOfTagsByNewsId(newsId);

		newsVO.setNews(news);
		newsVO.setAuthor(author);
		newsVO.setCommentsList(commentsList);
		newsVO.setListTag(tagsList);

		return newsVO;
	}

	/**
	 * Gets the list of News value object found according to search criteria
	 * parameters.
	 * 
	 * @param searchCriteria
	 * @return list of News value object.
	 * @throws ServiceException
	 */
	public List<NewsVO> getNewsListBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		List<NewsVO> newsVOList = new ArrayList<>();
		NewsVO newsVO = new NewsVO();
		List<News> listNews = newsService.search(searchCriteria);
		int size = listNews.size();
		for (int i = 0; i < size; i++) {
			long newsId = listNews.get(i).getId();
			long authorId = newsService.getAuthorIdByNewsId(newsId);
			Author author = authorService.getSingleAuthorById(authorId);
			List<Comment> commentsList = commentService.getListOfCommentsByNewsId(newsId);
			List<Tag> tagsList = newsService.getListOfTagsByNewsId(newsId);

			newsVO.setNews(listNews.get(i));
			newsVO.setAuthor(author);
			newsVO.setCommentsList(commentsList);
			newsVO.setListTag(tagsList);

			newsVOList.add(newsVO);
		}

		return newsVOList;
	}

	/**
	 * @return the newsService
	 */
	public NewsService getNewsService() {
		return newsService;
	}

	/**
	 * @param newsService
	 *            the newsService to set
	 */
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	/**
	 * @return the authorService
	 */
	public AuthorService getAuthorService() {
		return authorService;
	}

	/**
	 * @param authorService
	 *            the authorService to set
	 */
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	/**
	 * @return the commentService
	 */
	public CommentService getCommentService() {
		return commentService;
	}

	/**
	 * @param commentService
	 *            the commentService to set
	 */
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

}
