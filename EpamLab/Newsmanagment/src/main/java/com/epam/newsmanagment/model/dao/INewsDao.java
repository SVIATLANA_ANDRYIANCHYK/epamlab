package com.epam.newsmanagment.model.dao;

import java.util.List;

import com.epam.newsmanagment.model.dao.entity.News;
import com.epam.newsmanagment.model.dao.entity.SearchCriteria;
import com.epam.newsmanagment.model.dao.entity.Tag;

public interface INewsDao extends IDao<News> {

	/**
	 * 
	 * @param id
	 *            to get
	 * @return News entity
	 * @throws DaoException
	 */
	public News getSingleNewsById(long id) throws DaoException;

	/**
	 * 
	 * @param searchCriteria
	 *            for searching news in database
	 * @return List of all news appropriate to search criteria
	 * @throws DaoException
	 */
	public List<News> search(SearchCriteria searchCriteria) throws DaoException;

	/**
	 * 
	 * @return the count of news in the database.
	 * @throws DaoException
	 */
	public int getNewsCount() throws DaoException;

	/**
	 * Adds entity to the linking table News_Author
	 * 
	 * @param newsId
	 *            to add
	 * @param authorId
	 *            to add
	 * @throws DaoException
	 */
	public void addToNewsAuthor(long newsId, long authorId) throws DaoException;

	/**
	 * Deletes entity from linking table News_Author
	 * 
	 * @param newsId
	 *            to delete
	 * @throws DaoException
	 */
	public void deleteFromNewsAuthor(long newsId) throws DaoException;

	/**
	 * Adds entity to linking table News_Tag
	 * 
	 * @param newsId
	 *            to add
	 * @param tagList
	 *            to add
	 * @throws DaoException
	 */
	public void addToNewsTag(long newsId, List<Tag> tagList) throws DaoException;

	/**
	 * Deletes entity from linking table News_Tag
	 * 
	 * @param newsId
	 *            to delete
	 * @throws DaoException
	 */
	public void deleteFromNewsTag(long newsId) throws DaoException;

	/**
	 * Gets author id by news id.
	 * 
	 * @param newsId
	 *            to set
	 * @return id of author
	 * @throws DaoException
	 */
	public Long getAuthorIdByNewsId(long newsId) throws DaoException;

	/**
	 * 
	 * @param newsId
	 *            to set
	 * @return list of id of tags
	 * @throws DaoException
	 */
	public List<Long> getTagIdListByNewsId(long newsId) throws DaoException;

	/**
	 * Gets list of tags by certain news id.
	 * 
	 * @param newsId
	 *            to set
	 * @return list of tags
	 * @throws DaoException
	 */
	public List<Tag> getListOfTagsByNewsId(long newsId) throws DaoException;
}
