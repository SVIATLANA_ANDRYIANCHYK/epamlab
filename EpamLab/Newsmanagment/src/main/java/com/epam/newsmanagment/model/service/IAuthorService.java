package com.epam.newsmanagment.model.service;

import com.epam.newsmanagment.model.dao.entity.Author;

public interface IAuthorService extends IService<Author> {
	/**
	 * 
	 * @param id
	 *            of author to get
	 * @return Author entity
	 * @throws ServiceException
	 */
	public Author getSingleAuthorById(long id) throws ServiceException;

}
