package com.epam.newsmanagment.model.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagment.model.dao.DaoException;
import com.epam.newsmanagment.model.dao.INewsDao;
import com.epam.newsmanagment.model.dao.entity.News;
import com.epam.newsmanagment.model.dao.entity.SearchCriteria;
import com.epam.newsmanagment.model.dao.entity.Tag;
import com.epam.newsmanagment.model.util.QueryBuilder;
import com.epam.newsmanagment.model.util.TypeConverter;

public class NewsDao implements INewsDao {

	/**
	 * Data source object for taking the connection. Initialized in
	 * application-context.xml using setDataSource method
	 */
	private DataSource dataSource;

	/**
	 * SQL queries
	 */
	private static final String SELECT_ALL_NEWS_QUERY = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, COUNT(COMMENT_ID) AS COMMENT_COUNT FROM NEWS LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE ORDER BY MODIFICATION_DATE DESC, COMMENT_COUNT DESC";
	private static final String INSERT_NEWS_QUERY = "INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (NEWS_SEQ.nextval, ?, ?, ?, ?, ?)";
	private static final String DELETE_NEWS_QUERY = "DELETE FROM NEWS WHERE NEWS_ID = ?";
	private static final String SELECT_SINGLE_NEWS_QUERY = "SELECT n.NEWS_ID, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE FROM NEWS n WHERE n.NEWS_ID = ?";
	private static final String UPDATE_NEWS_QUERY = "UPDATE News SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";
	private static final String SELECT_NEWS_COUNT_QUERY = "SELECT COUNT(*) AS NEWS_COUNT FROM News";
	private static final String INSERT_NEWS_AUTHOR_QUERY = "INSERT INTO News_Author (NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
	private static final String DELETE_NEWS_AUTHOR_QUERY = "DELETE FROM News_Author WHERE NEWS_ID = ?";
	private static final String INSERT_NEWS_TAG_QUERY = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?, ?)";
	private static final String DELETE_NEWS_TAG_QUERY = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
	private static final String SELECT_AUTHOR_ID_BY_NEWS_ID = "SELECT na.AUTHOR_ID FROM NEWS_AUTHOR na WHERE NEWS_ID = ?";
	private static final String SELECT_TAG_ID_BY_NEWS_ID = "SELECT tn.TAG_ID FROM NEWS_TAG tn WHERE NEWS_ID = ?";

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#getAll()
	 */
	public List<News> getAll() throws DaoException {

		News news = null; // Create object News
		ResultSet resultSet = null;
		List<News> newsList = new ArrayList<News>();

		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_NEWS_QUERY)) {
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				news = new News();
				news.setId(resultSet.getLong("NEWS_ID"));
				news.setTitle(resultSet.getString("TITLE"));
				news.setShortText(resultSet.getString("SHORT_TEXT"));
				news.setFullText(resultSet.getString("FULL_TEXT"));
				news.setCreationDate(resultSet.getTimestamp("CREATION_DATE"));
				news.setModificationDate(resultSet.getDate("MODIFICATION_DATE"));
				newsList.add(news); // Add object News to list
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

		return newsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#create(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public Long create(News entity) throws DaoException {
		Long id = null;
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement((INSERT_NEWS_QUERY), new String[] { "NEWS_ID" })) {
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			Timestamp ts = new Timestamp(entity.getCreationDate().getTime()); // Type
																				// cast
			preparedStatement.setTimestamp(4, ts);
			TypeConverter converter = new TypeConverter();
			preparedStatement.setDate(5, converter.convertJavaDateToSqlDate(entity.getModificationDate()));
			preparedStatement.executeUpdate();

			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				id = generatedKeys.getLong(1);
			}

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}

		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#update(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public void update(News entity) throws DaoException {
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_NEWS_QUERY)) {
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			Timestamp ts = new Timestamp(entity.getCreationDate().getTime()); // Type
																				// cast
			preparedStatement.setTimestamp(4, ts);
			TypeConverter converter = new TypeConverter();
			preparedStatement.setDate(5, converter.convertJavaDateToSqlDate(entity.getModificationDate()));
			preparedStatement.setLong(6, entity.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#delete(long)
	 */
	public void delete(long id) throws DaoException {
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_QUERY)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#getSingleNewsById(long)
	 */
	@Override
	public News getSingleNewsById(long id) throws DaoException {
		News news = new News();
		ResultSet resultSet = null;
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_SINGLE_NEWS_QUERY)) {
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				news.setId(resultSet.getLong("NEWS_ID"));
				news.setTitle(resultSet.getString("TITLE"));
				news.setShortText(resultSet.getString("SHORT_TEXT"));
				news.setFullText(resultSet.getString("FULL_TEXT"));

				news.setCreationDate(resultSet.getTimestamp("CREATION_DATE"));

				news.setModificationDate(resultSet.getDate("MODIFICATION_DATE"));
			}

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}
		return news;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.INewsDao#search(com.epam.newsmanagment
	 * .model.dao.entity.SearchCriteria)
	 */
	public List<News> search(SearchCriteria searchCriteria) throws DaoException {
		List<News> newsList = new ArrayList<>();
		QueryBuilder operator = new QueryBuilder();
		String query = operator.buildSearchQuery(searchCriteria);

		News news = null; // Create object News
		ResultSet resultSet = null;

		try (Connection connection = dataSource.getConnection(); Statement statement = connection.createStatement()) {
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				news = new News();
				news.setId(resultSet.getLong("NEWS_ID"));
				news.setTitle(resultSet.getString("TITLE"));
				news.setShortText(resultSet.getString("SHORT_TEXT"));
				news.setFullText(resultSet.getString("FULL_TEXT"));
				news.setCreationDate(resultSet.getTimestamp("CREATION_DATE"));
				news.setModificationDate(resultSet.getDate("MODIFICATION_DATE"));
				newsList.add(news); // Add object News to list
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}

		return newsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#getNewsCount()
	 */
	public int getNewsCount() throws DaoException {
		int newsCount = 0;
		ResultSet resultSet = null;
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_NEWS_COUNT_QUERY)) {
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				newsCount = resultSet.getInt("NEWS_COUNT");
			}

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}

		return newsCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#addToNewsAuthor(long,
	 * long)
	 */
	public void addToNewsAuthor(long newsId, long authorId) throws DaoException {
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEWS_AUTHOR_QUERY)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#deleteFromNewsAuthor(long)
	 */
	public void deleteFromNewsAuthor(long newsId) throws DaoException {
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_AUTHOR_QUERY)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#addToNewsTag(long,
	 * java.util.List)
	 */
	public void addToNewsTag(long newsId, List<Tag> tagList) throws DaoException {

		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEWS_TAG_QUERY)) {
			for (int i = 0; i < tagList.size(); i++) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagList.get(i).getTagId());
				preparedStatement.addBatch();
			}

			preparedStatement.executeBatch();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#deleteFromNewsTag(long)
	 */
	public void deleteFromNewsTag(long newsId) throws DaoException {
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_TAG_QUERY)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#getAuthorIdByNewsId(long)
	 */
	public Long getAuthorIdByNewsId(long newsId) throws DaoException {
		Long authorId = null;
		ResultSet resultSet = null;
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_AUTHOR_ID_BY_NEWS_ID)) {
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				authorId = resultSet.getLong("AUTHOR_ID");
			}

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}
		return authorId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#getTagIdListByNewsId(long)
	 */
	public List<Long> getTagIdListByNewsId(long newsId) throws DaoException {
		long tagId = 0;
		ResultSet resultSet = null;
		List<Long> tagIdList = new ArrayList<>();
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TAG_ID_BY_NEWS_ID)) {
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tagId = resultSet.getLong("TAG_ID");
				tagIdList.add(tagId);
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}
		return tagIdList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.INewsDao#getListOfTagsByNewsId(long)
	 */
	public List<Tag> getListOfTagsByNewsId(long newsId) throws DaoException {
		List<Long> tagIdList = getTagIdListByNewsId(newsId);
		List<Tag> tagList = new ArrayList<>();
		TagDao tagDao = new TagDao();
		tagDao.setDataSource(dataSource);
		for (int i = 0; i < tagIdList.size(); i++) {
			tagList.add(tagDao.getSingleTagById(tagIdList.get(i)));
		}
		return tagList;
	}
}
