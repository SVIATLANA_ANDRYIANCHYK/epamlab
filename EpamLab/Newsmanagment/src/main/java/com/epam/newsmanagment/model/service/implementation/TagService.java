package com.epam.newsmanagment.model.service.implementation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagment.model.dao.DaoException;
import com.epam.newsmanagment.model.dao.entity.Tag;
import com.epam.newsmanagment.model.dao.implementations.TagDao;
import com.epam.newsmanagment.model.service.ITagService;
import com.epam.newsmanagment.model.service.ServiceException;

public class TagService implements ITagService {

	@Autowired
	private TagDao tagDao;

	private static final Logger LOG = LoggerFactory.getLogger(TagService.class);

	/**
	 * @return the tagDao
	 */
	public TagDao getTagDao() {
		return tagDao;
	}

	/**
	 * @param tagDao
	 *            the tagDao to set
	 */
	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#create(java.lang.Object)
	 */
	@Override
	public long create(Tag entity) throws ServiceException {
		try {
			return tagDao.create(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(Tag entity) throws ServiceException {
		try {
			tagDao.update(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#delete(long)
	 */
	@Override
	public void delete(long id) throws ServiceException {
		try {
			tagDao.delete(id);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#getAll()
	 */
	@Override
	public List<Tag> getAll() throws ServiceException {
		try {
			return tagDao.getAll();
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.ITagService#getSingleTagById(long)
	 */
	@Override
	public Tag getSingleTagById(long tagId) throws ServiceException {
		try {
			return tagDao.getSingleTagById(tagId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

}
