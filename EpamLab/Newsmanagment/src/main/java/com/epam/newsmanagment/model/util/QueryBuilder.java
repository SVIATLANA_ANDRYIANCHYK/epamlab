package com.epam.newsmanagment.model.util;

import com.epam.newsmanagment.model.dao.entity.SearchCriteria;

/**
 * Forms SQL query according to parameters of search criteria parameters
 * 
 * @author Sviatlana_Andryianch
 *
 */
public class QueryBuilder {

	public String buildSearchQuery(SearchCriteria searchCriteria) {

		StringBuilder query = new StringBuilder("SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM News ");
		// if only author set
		if ((searchCriteria.getAuthorId() != 0) && (searchCriteria.getTagList().isEmpty())) {
			query.append("INNER JOIN NEWS_AUTHOR ON on NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID = ");
			query.append(searchCriteria.getAuthorId());
			// if only list of tags set
		} else if ((searchCriteria.getAuthorId() == 0) && (!searchCriteria.getTagList().isEmpty())) {
			query.append("INNER JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID WHERE NEWS_TAG.TAG_ID IN (");
			for (int i = 0; i < searchCriteria.getTagList().size(); i++) {
				query.append(searchCriteria.getTagList().get(i));
				if (i != (searchCriteria.getTagList().size() - 1)) {
					query.append(", ");
				} else {
					query.append(")");
				}
			}

			// if both list of tags and author set
		} else if ((searchCriteria.getAuthorId() != 0) && (!searchCriteria.getTagList().isEmpty())) {
			query.append("INNER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID ");
			query.append("INNER JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID WHERE NEWS_TAG.TAG_ID IN (");
			for (int i = 0; i < searchCriteria.getTagList().size(); i++) {
				query.append(searchCriteria.getTagList().get(i));
				if (i != (searchCriteria.getTagList().size() - 1)) {
					query.append(", ");
				} else {
					query.append(") ");
				}
			}
			query.append("AND NEWS_AUTHOR.AUTHOR_ID = ");
			query.append(searchCriteria.getAuthorId());
		}

		return query.toString();
	}
}
