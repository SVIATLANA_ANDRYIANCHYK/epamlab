package com.epam.newsmanagment.model.dao;

import java.util.List;

import com.epam.newsmanagment.model.dao.entity.IEntity;

/**
 * Abstraction of all dao entities.
 * 
 * @author Sviatlana_Andryianch
 *
 */
public interface IDao<T extends IEntity> {

	/**
	 * Adds entity to the database.
	 * 
	 * @param entity
	 *            entity for adding to a database.
	 */
	public Long create(T entity) throws DaoException;

	/**
	 * 
	 * @param entity
	 *            adding to db
	 * @return true if operation ended successfully, false - if not
	 * @throws DaoException
	 */
	public void update(T entity) throws DaoException;

	/**
	 * 
	 * @param deliting
	 *            entity
	 * @return true if operation ended successfully, false - if not
	 * @throws DaoException
	 */
	public void delete(long id) throws DaoException;

	/**
	 * Shows all items in the database
	 * 
	 * @return list of items
	 * @throws DaoException
	 */
	public List<T> getAll() throws DaoException;

}
