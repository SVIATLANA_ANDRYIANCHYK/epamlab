package com.epam.newsmanagment.model.service.implementation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.newsmanagment.model.dao.DaoException;
import com.epam.newsmanagment.model.dao.entity.News;
import com.epam.newsmanagment.model.dao.entity.SearchCriteria;
import com.epam.newsmanagment.model.dao.entity.Tag;
import com.epam.newsmanagment.model.dao.implementations.NewsDao;
import com.epam.newsmanagment.model.service.INewsService;
import com.epam.newsmanagment.model.service.ServiceException;

public class NewsService implements INewsService {

	private NewsDao newsDao;
	private static final Logger LOG = LoggerFactory.getLogger(NewsService.class);

	/**
	 * 
	 */
	public NewsService() {
		super();
	}

	/**
	 * @param dao
	 */
	public NewsService(NewsDao dao) {
		super();
		this.newsDao = dao;
	}

	/**
	 * @return the newsDao
	 */
	public NewsDao getNewsDao() {
		return newsDao;
	}

	/**
	 * @param newsDao
	 *            the newsDao to set
	 */
	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#create(java.lang.Object)
	 */
	@Override
	public long create(News entity) throws ServiceException {
		try {
			return newsDao.create(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(News entity) throws ServiceException {
		try {
			newsDao.update(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#delete(long)
	 */
	@Override
	public void delete(long id) throws ServiceException {
		try {
			newsDao.delete(id);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#getAll()
	 */
	@Override
	public List<News> getAll() throws ServiceException {
		try {
			return newsDao.getAll();
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#getSingleNewsById(long)
	 */
	@Override
	public News getSingleNewsById(long id) throws ServiceException {
		try {
			return newsDao.getSingleNewsById(id);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.INewsService#search(com.epam.
	 * newsmanagment.model.dao.entity.SearchCriteria)
	 */
	@Override
	public List<News> search(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDao.search(searchCriteria);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.INewsService#getNewsCount()
	 */
	@Override
	public int getNewsCount() throws ServiceException {
		try {
			return newsDao.getNewsCount();
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#addToNewsAuthor(long,
	 * long)
	 */
	@Override
	public void addToNewsAuthor(long newsId, long authorId) throws ServiceException {
		try {
			newsDao.addToNewsAuthor(newsId, authorId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#deleteFromNewsAuthor
	 * (long)
	 */
	@Override
	public void deleteFromNewsAuthor(long newsId) throws ServiceException {
		try {
			newsDao.deleteFromNewsAuthor(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#deleteFromNewsTag(long)
	 */
	@Override
	public void deleteFromNewsTag(long newsId) throws ServiceException {
		try {
			newsDao.deleteFromNewsTag(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.INewsService#addToNewsTag(long,
	 * java.util.List)
	 */
	@Override
	public void addToNewsTag(long newsId, List<Tag> tagList) throws ServiceException {
		try {
			newsDao.addToNewsTag(newsId, tagList);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#getAuthorIdByNewsId
	 * (long)
	 */
	@Override
	public Long getAuthorIdByNewsId(long newsId) throws ServiceException {
		try {
			return newsDao.getAuthorIdByNewsId(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#getTagIdListByNewsId
	 * (long)
	 */
	@Override
	public List<Long> getTagIdListByNewsId(long newsId) throws ServiceException {
		try {
			return newsDao.getTagIdListByNewsId(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.INewsService#getListOfTagsByNewsId
	 * (long)
	 */
	@Override
	public List<Tag> getListOfTagsByNewsId(long newsId) throws ServiceException {
		try {
			return newsDao.getListOfTagsByNewsId(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}

	}

}
