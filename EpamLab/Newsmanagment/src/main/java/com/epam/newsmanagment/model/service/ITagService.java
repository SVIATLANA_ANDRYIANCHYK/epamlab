package com.epam.newsmanagment.model.service;

import com.epam.newsmanagment.model.dao.entity.Tag;

public interface ITagService extends IService<Tag> {
	/**
	 * Gets Tag object by certain tag id.
	 * 
	 * @param tagId
	 * @return Tag object
	 * @throws ServiceException
	 */
	public Tag getSingleTagById(long tagId) throws ServiceException;
}
