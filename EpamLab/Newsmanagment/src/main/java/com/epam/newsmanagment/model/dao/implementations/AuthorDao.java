package com.epam.newsmanagment.model.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagment.model.dao.DaoException;
import com.epam.newsmanagment.model.dao.IAuthorDao;
import com.epam.newsmanagment.model.dao.entity.Author;

/**
 * @author Sviatlana_Andryianch
 *
 */
public class AuthorDao implements IAuthorDao {

	private DataSource dataSource;

	private static final String INSERT_AUTHOR_QUERY = "INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHOR_SEQ.nextval, ?, ?)";
	private static final String SELECT_SINGLE_AUTHOR_QUERY = "SELECT a.AUTHOR_ID, a.AUTHOR_NAME, a.EXPIRED FROM AUTHOR a WHERE AUTHOR_ID = ?";
	private static final String UPDATE_AUTHOR_QUERY = "UPDATE Author SET AUTHOR_NAME = ?, EXPIRED = ?  WHERE AUTHOR_ID = ?";
	private static final String SELECT_ALL_AUTHORS_QUERY = "SELECT a.AUTHOR_ID, a.AUTHOR_NAME, a.EXPIRED FROM AUTHOR a";
	private static final String SET_EXPIRATION_DATE_QUERY = "UPDATE AUTHOR SET EXPIRED = CURRENT_TIMESTAMP WHERE AUTHOR_ID = ?";

	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * @param dataSource
	 *            the dataSource to set
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#create(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public Long create(Author entity) throws DaoException {
		Long id = null;
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(INSERT_AUTHOR_QUERY, new String[] { "AUTHOR_ID" })) {

			preparedStatement.setString(1, entity.getAuthorName());
			Timestamp ts = new Timestamp(entity.getExpired().getTime()); // Type
			preparedStatement.setTimestamp(2, ts);
			preparedStatement.executeUpdate();
			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				id = generatedKeys.getLong(1);
			}

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IAuthorDao#getSingleAuthorById(long)
	 */
	public Author getSingleAuthorById(long id) throws DaoException {
		Author author = new Author();
		ResultSet resultSet = null;
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_SINGLE_AUTHOR_QUERY)) {
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				author.setAuthorId(resultSet.getLong("AUTHOR_ID"));
				author.setAuthorName(resultSet.getString("AUTHOR_NAME"));
				author.setExpired(resultSet.getTimestamp("EXPIRED"));
			}

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}
		return author;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#update(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public void update(Author entity) throws DaoException {

		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_AUTHOR_QUERY)) {
			preparedStatement.setString(1, entity.getAuthorName());
			if (entity.getExpired() != null) {
				Timestamp ts = new Timestamp(entity.getExpired().getTime()); // Type
				// cast
				preparedStatement.setTimestamp(2, ts);
			} else {
				preparedStatement.setTimestamp(2, null);
			}

			preparedStatement.setLong(3, entity.getAuthorId());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#getAll()
	 */
	public List<Author> getAll() throws DaoException {
		Author author = null;

		ResultSet resultSet = null;
		List<Author> authorList = new ArrayList<Author>();

		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_AUTHORS_QUERY)) {
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				author = new Author();
				author.setAuthorId(resultSet.getLong("AUTHOR_ID"));
				author.setAuthorName(resultSet.getString("AUTHOR_NAME"));
				author.setExpired(resultSet.getTimestamp("EXPIRED"));
				authorList.add(author); // Add object Author to list
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

		return authorList;
	}

	/**
	 * Makes the author of certain id expired. Sets the expiration date
	 * 
	 * @throws DaoException
	 */
	private void makeAuthorExpired(long id) throws DaoException {

		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SET_EXPIRATION_DATE_QUERY)) {

			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

	}

	/**
	 * Method is not implemented because it is not able to delete author, but
	 * possible to make him expired.
	 * 
	 * @see makeAuthorExpired method.
	 */
	@Override
	public void delete(long id) throws DaoException {
		makeAuthorExpired(id);
	}

}
