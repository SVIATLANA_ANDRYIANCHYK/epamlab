package com.epam.newsmanagment.model.dao.entity;

import java.util.List;

/**
 * Sets parameters (criteria) of search
 * 
 * @author Sviatlana_Andryianch
 *
 */
public class SearchCriteria {

	private long authorId;
	private List<Long> tagList;

	public SearchCriteria() {
	}

	/**
	 * @param authorId
	 * @param tagList
	 */
	public SearchCriteria(long authorId, List<Long> tagList) {
		super();
		this.authorId = authorId;
		this.tagList = tagList;
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagList() {
		return tagList;
	}

	public void setTagList(List<Long> tagList) {
		this.tagList = tagList;
	}
}
