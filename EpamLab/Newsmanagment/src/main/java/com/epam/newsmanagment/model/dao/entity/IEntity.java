package com.epam.newsmanagment.model.dao.entity;

/**
 * All entities implement this interface to have the same type.
 * 
 * @author Sviatlana_Andryianch
 *
 */
public interface IEntity {

}
