package com.epam.newsmanagment.model.dao;

import com.epam.newsmanagment.model.dao.entity.Tag;

public interface ITagDao extends IDao<Tag> {
	/**
	 * Gets Tag object by certain tag id.
	 * 
	 * @param tagId
	 * @return Tag object
	 * @throws DaoException
	 */
	public Tag getSingleTagById(long tagId) throws DaoException;
}
