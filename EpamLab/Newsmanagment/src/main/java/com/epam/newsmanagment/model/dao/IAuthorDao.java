package com.epam.newsmanagment.model.dao;

import com.epam.newsmanagment.model.dao.entity.Author;

public interface IAuthorDao extends IDao<Author> {
	/**
	 * 
	 * @param id
	 *            of author to get
	 * @return Author entity
	 * @throws DaoException
	 */
	public Author getSingleAuthorById(long id) throws DaoException;

}
