package com.epam.newsmanagment.model.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagment.model.dao.DaoException;
import com.epam.newsmanagment.model.dao.ICommentDao;
import com.epam.newsmanagment.model.dao.entity.Comment;

public class CommentDao implements ICommentDao {

	private static final String INSERT_COMMENT_QUERY = "INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES (COMMENTS_SEQ.nextval, ?, ?, ?)";
	private static final String UPDATE_COMMENT_QUERY = "UPDATE Comments SET NEWS_ID = ?, COMMENT_TEXT = ?, CREATION_DATE = ? WHERE COMMENT_ID = ?";
	private static final String SELECT_ALL_COMMENTS_QUERY = "SELECT c.COMMENT_ID, c.NEWS_ID, c.COMMENT_TEXT, c.CREATION_DATE FROM COMMENTS c";
	private static final String DELETE_COMMENT_QUERY = "DELETE FROM Comments WHERE COMMENT_ID = ?";
	private static final String SELECT_COMMENTS_OF_CERTAIN_NEWS_QUERY = "SELECT c.COMMENT_ID, c.COMMENT_TEXT, c.CREATION_DATE FROM COMMENTS c WHERE c.NEWS_ID = ?";
	private static final String SELECT_SINGLE_COMMENT_QUERY = "SELECT c.NEWS_ID, c.COMMENT_TEXT, c.CREATION_DATE FROM COMMENTS c WHERE c.COMMENT_ID = ?";

	private DataSource dataSource;

	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * @param dataSource
	 *            the dataSource to set
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#create(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public Long create(Comment entity) throws DaoException {
		Long id = null;
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement((INSERT_COMMENT_QUERY), new String[] { "COMMENT_ID" })) {
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getCommentText());
			Timestamp ts = new Timestamp(entity.getCreationDate().getTime()); // Type
																				// cast
			preparedStatement.setTimestamp(3, ts);
			preparedStatement.executeUpdate();

			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				id = generatedKeys.getLong(1);
			}

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#update(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public void update(Comment entity) throws DaoException {

		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_COMMENT_QUERY)) {
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getCommentText());
			Timestamp ts = new Timestamp(entity.getCreationDate().getTime()); // Type
																				// cast
			preparedStatement.setTimestamp(3, ts);
			preparedStatement.setLong(4, entity.getCommentId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#getAll()
	 */
	public List<Comment> getAll() throws DaoException {
		Comment comment = null;
		ResultSet resultSet = null;
		List<Comment> commentList = new ArrayList<>();
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_COMMENTS_QUERY)) {
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				comment = new Comment();
				comment.setCommentId(resultSet.getLong("COMMENT_ID"));
				comment.setNewsId(resultSet.getLong("NEWS_ID"));
				comment.setCommentText(resultSet.getString("COMMENT_TEXT"));
				comment.setCreationDate(resultSet.getTimestamp("CREATION_DATE"));

				commentList.add(comment); // Add object Comment to list
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

		return commentList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#delete(long)
	 */
	@Override
	public void delete(long id) throws DaoException {
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(DELETE_COMMENT_QUERY)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.ICommentDao#getListOfCommentsByNewsId
	 * (long)
	 */
	@Override
	public List<Comment> getListOfCommentsByNewsId(long newsId) throws DaoException {

		Comment comment = null;
		ResultSet resultSet = null;
		List<Comment> commentList = new ArrayList<>();
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_COMMENTS_OF_CERTAIN_NEWS_QUERY)) {
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				comment = new Comment();
				comment.setCommentId(resultSet.getLong("COMMENT_ID"));
				comment.setNewsId(newsId);
				comment.setCommentText(resultSet.getString("COMMENT_TEXT"));
				comment.setCreationDate(resultSet.getTimestamp("CREATION_DATE"));

				commentList.add(comment); // Add object Comment to list
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

		return commentList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.ICommentDao#getSingleCommentById(long)
	 */
	public Comment getSingleCommentById(long commentId) throws DaoException {
		Comment comment = null;
		ResultSet resultSet = null;

		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_SINGLE_COMMENT_QUERY)) {
			preparedStatement.setLong(1, commentId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				comment = new Comment();
				comment.setCommentId(commentId);
				comment.setNewsId(resultSet.getLong("NEWS_ID"));
				comment.setCommentText(resultSet.getString("COMMENT_TEXT"));
				comment.setCreationDate(resultSet.getTimestamp("CREATION_DATE"));

			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

		return comment;
	}
}
