package com.epam.newsmanagment.model.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagment.model.dao.DaoException;
import com.epam.newsmanagment.model.dao.ITagDao;
import com.epam.newsmanagment.model.dao.entity.Tag;

public class TagDao implements ITagDao {

	private static final String SELECT_ALL_TAGS_QUERY = "SELECT t.TAG_ID, t.TAG_NAME FROM Tag t";
	private static final String INSERT_TAG_QUERY = "INSERT INTO Tag (TAG_ID, TAG_NAME) VALUES (TAG_SEQ.nextval, ?)";
	private static final String UPDATE_TAG_QUERY = "UPDATE Tag SET TAG_NAME = ? WHERE TAG_ID = ?";
	private static final String DELETE_TAG_QUERY = "DELETE FROM Tag WHERE TAG_ID = ?";
	private static final String SELECT_SINGLE_TAG_BY_ID_QUERY = "SELECT t.TAG_NAME FROM TAG t WHERE TAG_ID = ?";

	private DataSource dataSource;

	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * @param dataSource
	 *            the dataSource to set
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#create(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public Long create(Tag entity) throws DaoException {
		Long id = null;
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement((INSERT_TAG_QUERY), new String[] { "TAG_ID" })) {
			preparedStatement.setString(1, entity.getTagName());
			preparedStatement.executeUpdate();

			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				id = generatedKeys.getLong(1);
			}

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#update(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public void update(Tag entity) throws DaoException {
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TAG_QUERY)) {
			preparedStatement.setString(1, entity.getTagName());
			preparedStatement.setLong(2, entity.getTagId());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#getAll()
	 */
	public List<Tag> getAll() throws DaoException {
		Tag tag = null;
		ResultSet resultSet = null;
		List<Tag> tagList = new ArrayList<>();
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_TAGS_QUERY)) {
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tag = new Tag();
				tag.setTagId(resultSet.getLong("TAG_ID"));
				tag.setTagName(resultSet.getString("TAG_NAME"));
				tagList.add(tag); // Add object Tag to list
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

		return tagList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#delete(long)
	 */
	@Override
	public void delete(long id) throws DaoException {
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TAG_QUERY)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.ITagDao#getSingleTagById(long)
	 */
	public Tag getSingleTagById(long tagId) throws DaoException {
		Tag tag = null;
		ResultSet resultSet = null;
		try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_SINGLE_TAG_BY_ID_QUERY)) {
			preparedStatement.setLong(1, tagId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				tag = new Tag();
				tag.setTagId(tagId);
				tag.setTagName(resultSet.getString("TAG_NAME"));
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}
		return tag;
	}
}
