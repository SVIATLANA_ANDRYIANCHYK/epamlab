package com.epam.newsmanagment.model.dao.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * News entity
 * 
 * @author Sviatlana_Andryianch
 *
 */
public final class News implements Serializable, IEntity {

	/**
	 * For deserialization with no exception after modification.
	 */
	private static final long serialVersionUID = 3773281197317274020L;

	private Long newsId; // Primary key
	private String title;
	private String shortText;
	private String fullText;
	private Date creationDate;
	private Date modificationDate;

	/**
	 * Default constructor
	 */
	public News() {
	}

	/**
	 * Parameterized constructor
	 * 
	 * @param id
	 * @param title
	 * @param shortText
	 * @param fullText
	 * @param creationDate
	 * @param modificationDate
	 */
	public News(String title, String shortText, String fullText, Date creationDate, Date modificationDate) {
		super();

		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	/**
	 * @param newsId
	 * @param title
	 * @param shortText
	 * @param fullText
	 * @param creationDate
	 * @param modificationDate
	 */
	public News(Long newsId, String title, String shortText, String fullText, Date creationDate, Date modificationDate) {
		super();
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return newsId;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.newsId = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the shortText
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * @param shortText
	 *            the shortText to set
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * @return the fullText
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * @param fullText
	 *            the fullText to set
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + (int) (newsId ^ (newsId >>> 32));
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (newsId != other.newsId)
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "News [id=" + newsId + ", title=" + title + ", shortText=" + shortText + ", fullText=" + fullText + ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + "]";
	}

}
