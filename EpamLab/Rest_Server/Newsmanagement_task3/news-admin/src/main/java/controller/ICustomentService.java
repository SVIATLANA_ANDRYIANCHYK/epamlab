package controller;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.service.ServiceException;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


/**
 * Created by SvetaPC on 30.11.2015.
 */
@Path("/customer")
public interface ICustomentService {

    @GET
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/test/{newsId}")
    public News getCustomerDetails(@Description("Customer name") @PathParam("newsId") final Long newsId) throws ServiceException;

//    @POST
//    @Description(value = "Resource", target = DocTarget.RESOURCE)
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
//    @Path("/addcustomer")
//    public GenericResponse addCustomer(@Description("New customer") Customer customer);

}
