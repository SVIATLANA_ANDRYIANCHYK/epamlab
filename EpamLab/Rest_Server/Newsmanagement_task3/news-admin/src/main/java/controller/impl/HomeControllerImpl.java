package controller.impl;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.*;
import controller.IHomeController;
import org.springframework.stereotype.Component;
import util.PageBuilder;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by SvetaPC on 09.12.2015.
 */

public class HomeControllerImpl implements IHomeController {

    private ITagService tagService;
    private IAuthorService authorService;
    private INewsService newsService;
    private ICommentService commentService;

    @Inject
    public HomeControllerImpl(ITagService tagService, IAuthorService authorService, INewsService newsService, ICommentService commentService) {
        this.tagService = tagService;
        this.authorService = authorService;
        this.newsService = newsService;
        this.commentService = commentService;
    }

    @Override
    public List<Author> getAllAuthors() throws ServiceException {
        List<Author> authorList = authorService.getAll();
        return authorList;
    }

    @Override
    public List<Tag> getAllTags() throws ServiceException {
        List<Tag> tagList = tagService.getAll();
        return tagList;
    }

    @Override
    public List<News> getAllNews(int currentPage, HttpSession session) throws ServiceException {
        PageBuilder pageBuilder = new PageBuilder();
        SearchCriteria sc = new SearchCriteria();
        sc = (SearchCriteria) session.getAttribute("searchCriteria");

        int newsOnPageCount = pageBuilder.getNewsOnPageCount();

        List<News> newsList = newsService.searchBySearchCriteriaAndPagination(sc, currentPage, newsOnPageCount);
        for (int i = 0; i < newsList.size(); i++) {
            newsList.get(i).setCommentsList(commentService.getListOfCommentsByNewsId(newsList.get(i).getNewsId()));
        }

        return newsList;
    }

    @Override
    public int[] getPagesArray(HttpSession session) throws ServiceException {
        PageBuilder pageBuilder = new PageBuilder();
        SearchCriteria sc = new SearchCriteria();
        sc = (SearchCriteria) session.getAttribute("searchCriteria");
        int[] pagesNumbersArray = pageBuilder.getNewsPagesArray(sc, newsService);
        return pagesNumbersArray;
    }


}
