package controller;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.service.ServiceException;
import controller.impl.GenericResponse;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by SvetaPC on 08.12.2015.
 */
@Path("/addNewsService")
public interface IAddNewsController {

    @POST
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/saveNews")
    public GenericResponse saveNews(@Description("New news") News news, @RequestParam(value = "tagId[]", required = false) Long[] tagIdArray, BindingResult result) throws ServiceException;


}
