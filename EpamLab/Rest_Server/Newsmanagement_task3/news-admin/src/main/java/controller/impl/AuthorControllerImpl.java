package controller.impl;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.ServiceException;
import controller.IAuthorController;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Created by SvetaPC on 08.12.2015.
 */

public class AuthorControllerImpl implements IAuthorController {

    private IAuthorService authorService;

    @Inject
    public AuthorControllerImpl(IAuthorService authorService) {
        this.authorService = authorService;
    }

    @Override
    public GenericResponse editAuthor(@Description("Edit author") Long authorId) throws ServiceException {
        //Author authorForUpdate = authorService.getSingleAuthorById(authorId);
        throw new UnsupportedOperationException();

    }

    @Override
    public GenericResponse deleteAuthor(@Description("Delete author") Long authorId) throws ServiceException {
        GenericResponse response = new GenericResponse();
        authorService.delete(authorId);
        response.setMessage("Author deleted");
        response.setSuccess(true);
        return response;
    }

    @Override
    public GenericResponse updateAuthor(@Valid @Description("Update author") Author author, BindingResult result) throws ServiceException {
        GenericResponse response = new GenericResponse();
        if (!result.hasErrors()) {
            authorService.update(author);
            response.setMessage("Author updated");
            response.setSuccess(true);
        } else {
            response.setMessage("Error updating author");
            response.setSuccess(false);
        }
        return response;
    }

    @Override
    public GenericResponse addAuthor(@Valid @Description("Add author") Author author, BindingResult result) throws ServiceException {
        GenericResponse response = new GenericResponse();

        if (!result.hasErrors()) {
            authorService.create(author);
            response.setMessage("Author added");
            response.setSuccess(true);
        } else {
            response.setMessage("Error while adding author");
            response.setSuccess(false);
        }
        return response;
    }
}
