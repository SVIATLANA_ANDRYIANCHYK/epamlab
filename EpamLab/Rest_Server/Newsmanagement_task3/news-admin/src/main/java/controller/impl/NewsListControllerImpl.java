package controller.impl;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.service.*;
import controller.INewsListController;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SvetaPC on 09.12.2015.
 */
//@Component("newsListService")
public class NewsListControllerImpl implements INewsListController {

    private IAuthorService authorService;
    private ITagService tagService;
    private INewsService newsService;
    private ICommentService commentService;

    @Inject
    public NewsListControllerImpl(IAuthorService authorService, ITagService tagService, INewsService newsService, ICommentService commentService) {
        this.authorService = authorService;
        this.commentService = commentService;
        this.tagService = tagService;
        this.newsService = newsService;
    }

    @Override
    public HttpSession setSearchCriteria(HttpSession session, @RequestParam(value = "tagId[]", required = false) Long[] tagIdArray, @RequestParam(value = "author", required = false) Long authorId) {
        List<Long> tagList = new ArrayList<Long>();
        if (tagIdArray != null) {
            for (int i = 0; i < tagIdArray.length; i++) {
                tagList.add(tagIdArray[i]);
            }
        }
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthorId(authorId);
        searchCriteria.setTagList(tagList);
        session.setAttribute("searchCriteria", searchCriteria);
        return  session;
        }

    @Override
    public HttpSession reset(HttpSession session) {
        session.setAttribute("searchCriteria", null);
        return  session;
    }

    @Override
    public GenericResponse deleteNews(@RequestParam(value = "newsToDelete[]", required = false) Long[] newsToDeleteArray) throws ServiceException {
        GenericResponse response = new GenericResponse();
        if (newsToDeleteArray != null) {
            for (int i = 0; i < newsToDeleteArray.length; i++) {
                newsService.delete(newsToDeleteArray[i]);
            }
        }
        response.setMessage("News deleted");
        response.setSuccess(true);
        return response;
    }

    @Override
    public News getSingleNews(@Description("Single news") Long newsId) throws ServiceException {
        News news = newsService.getSingleNewsById(newsId);
        news.setCommentsList(commentService.getListOfCommentsByNewsId(newsId));
        return news;
    }
}
