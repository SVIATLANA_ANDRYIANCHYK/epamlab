package controller;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.service.ServiceException;
import controller.impl.GenericResponse;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;
import org.springframework.validation.BindingResult;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by SvetaPC on 08.12.2015.
 */
@Path("/authorService")
public interface IAuthorController {

    @GET
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/editAuthor/{authorId}")
    public GenericResponse editAuthor(@Description("Edit author") @PathParam("authorId") final Long authorId) throws ServiceException;

    @GET
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/deleteAuthor/{authorId}")
    public GenericResponse deleteAuthor(@Description("Delete author") @PathParam("authorId") final Long authorId) throws ServiceException;

    @POST
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/updateAuthor")
    public GenericResponse updateAuthor(@Valid @Description("Update author") Author author, BindingResult result) throws ServiceException;

    @POST
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/addAuthor")
    public GenericResponse addAuthor(@Valid @Description("Add author") Author author, BindingResult result) throws ServiceException;



    }
