package controller;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.ServiceException;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by SvetaPC on 09.12.2015.
 */
@Path("/home")
public interface IHomeController {

    @GET
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/authors")
    public List<Author> getAllAuthors() throws ServiceException;

    @GET
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/tags")
    public List<Tag>  getAllTags() throws ServiceException;

    @GET
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/newsList")
    public List<News> getAllNews(@RequestParam(value = "page", defaultValue = "1") int currentPage, HttpSession session) throws ServiceException;

    @GET
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/pages")
    public int[] getPagesArray(HttpSession session) throws ServiceException;

}
