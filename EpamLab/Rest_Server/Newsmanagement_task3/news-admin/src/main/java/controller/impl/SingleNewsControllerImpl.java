package controller.impl;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.service.ICommentService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ServiceException;
import controller.ISingleNewsController;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Date;

/**
 * Created by SvetaPC on 09.12.2015.
 */
//@Component("singleNewsService")
public class SingleNewsControllerImpl implements ISingleNewsController {

    private ICommentService commentService;
    private INewsService newsService;

    @Inject
    public SingleNewsControllerImpl(INewsService newsService, ICommentService commentService) {
        this.commentService = commentService;
        this.newsService = newsService;
    }

    @Override
    public GenericResponse postComment(@Description("news id") Long newsId, @Description("comment text") String commentText) throws ServiceException {
        String text = commentText.replaceAll("\\s+", "");
        GenericResponse response = new GenericResponse();
        if (text.length() > 1 && commentText != null && commentText.length() < 100) {
            Comment comment = new Comment();
            Date today = new Date();
            comment.setCreationDate(today);
            comment.setCommentText(commentText);
            comment.setNewsId(newsId);
            commentService.create(comment);
            response.setSuccess(true);
            response.setMessage("Comment added");
        }
        else{
            response.setSuccess(false);
            response.setMessage("Comment not added");
        }
        return response;
    }

    @Override
    public GenericResponse deleteComment(@Description("News id") Long newsId, @Description("Comment id") Long commentId) throws ServiceException {
        GenericResponse response = new GenericResponse();
        commentService.delete(commentId);
        response.setSuccess(true);
        response.setMessage("Comment added");
        return response;
    }
}
