package controller;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.service.ServiceException;
import controller.impl.GenericResponse;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


/**
 * Created by SvetaPC on 09.12.2015.
 */
@Path("/newsList")
public interface INewsListController {

    @POST
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/filter")
    public HttpSession setSearchCriteria(HttpSession session, @RequestParam(value = "tagId[]", required = false) Long[] tagIdArray, @RequestParam(value = "author", required = false) Long authorId);

    @POST
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/reset")
    public HttpSession reset(HttpSession session);

    @POST
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/deleteNews")
    public GenericResponse deleteNews(@RequestParam(value = "newsToDelete[]", required = false) Long[] newsToDeleteArray) throws ServiceException;

    @GET
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/singleNews/{newsId}")
    public News getSingleNews(@Description("Single news") @PathParam("newsId") final Long newsId) throws ServiceException;

    }
