package controller.impl;

import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.ServiceException;
import controller.ITagController;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Created by SvetaPC on 09.12.2015.
 */
public class TagControllerImpl implements ITagController {

    private ITagService tagService;

    @Inject
    public TagControllerImpl(ITagService tagService) {
        this.tagService = tagService;
    }

    @Override
    public GenericResponse deleteTag(Long tagId) throws ServiceException {
        tagService.delete(tagId);
        GenericResponse response = new GenericResponse();
        response.setMessage("Tag deleted");
        response.setSuccess(true);
        return response;
    }

    @Override
    public GenericResponse updateTag(@Valid @Description("tag for update") Tag tag, BindingResult result) throws ServiceException {
        GenericResponse response = new GenericResponse();

        if (!result.hasErrors()) {
            tagService.update(tag);
            response.setMessage("Tag updated");
            response.setSuccess(true);
        } else {
            response.setMessage("Tag not updated");
            response.setSuccess(false);
        }
        return  response;
    }

    @Override
    public GenericResponse addTag(@Valid @Description("tag to add") Tag tag, BindingResult result) throws ServiceException {
        GenericResponse response = new GenericResponse();

        if (!result.hasErrors()) {
            tagService.create(tag);
            response.setMessage("Tag created");
            response.setSuccess(true);

        } else {
            response.setMessage("Tag not created");
            response.setSuccess(false);
        }
        return response;
    }
}
