package controller;

import com.epam.newsmanagment.service.ServiceException;
import controller.impl.GenericResponse;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by SvetaPC on 09.12.2015.
 */
@Path("/singleNews")
public interface ISingleNewsController {

    @POST
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/postComment")
    public GenericResponse postComment(@Description("news id") Long newsId, @Description("comment text") String commentText) throws ServiceException;

    @POST
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/deleteComment")
    public  GenericResponse deleteComment(@Description("News id") Long newsId, @Description("Comment id") Long commentId) throws ServiceException;


}
