package controller;

import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.ServiceException;
import controller.impl.GenericResponse;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;
import org.springframework.validation.BindingResult;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by SvetaPC on 09.12.2015.
 */
@Path("/tags")
public interface ITagController {

    @GET
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/deleteTag/{tagId}")
    public GenericResponse deleteTag(@PathParam("tagId")Long tagId) throws ServiceException;

    @POST
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/updateTag")
    public GenericResponse updateTag(@Valid @Description("tag for update") Tag tag, BindingResult result) throws ServiceException;

    @POST
    @Description(value = "Resource", target = DocTarget.RESOURCE)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/addTag")
    public GenericResponse addTag(@Valid @Description("tag to add") Tag tag, BindingResult result) throws ServiceException;



    }
