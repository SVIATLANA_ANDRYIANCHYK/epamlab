package controller.impl;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.service.*;

import controller.ICustomentService;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by SvetaPC on 30.11.2015.
 */

public class CustomerServiceImpl implements ICustomentService {

    private INewsService newsService;
    private ICommentService commentService;

    @Inject
    public CustomerServiceImpl(INewsService newsService, ICommentService commentService) {
        this.newsService = newsService;
        this.commentService = commentService;
    }

    @Override
    public News getCustomerDetails(Long newsId) throws ServiceException {
        //System.out.println(newsId);
        News news = newsService.getSingleNewsById(newsId);
        news.setCommentsList(commentService.getListOfCommentsByNewsId(newsId));
        //System.out.println(news);
        return news;
    }

//    @Override
//    public GenericResponse addCustomer(Customer customer) {
//        GenericResponse response = new GenericResponse();
//        if(customer != null && customer.getAge() > 18){
//            response.setMessage("Added");
//            response.setSuccess(true);
//        }
//
//        else {
//            response.setMessage("Not Added");
//            response.setSuccess(false);
//        }
//
//        return response;
//    }
}
