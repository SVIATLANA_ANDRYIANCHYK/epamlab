package controller.impl;


import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.ServiceException;
import controller.IAddNewsController;
import controller.NewsValidator;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import java.util.*;


public class AddNewsControllerImpl implements IAddNewsController {

    private ITagService tagService;
    private IAuthorService authorService;
    private INewsService newsService;
    @Autowired
    private NewsValidator validator;

    @Inject
    public AddNewsControllerImpl(INewsService newsService, ITagService tagService, IAuthorService authorService) {
        this.newsService = newsService;
        this.tagService = tagService;
        this.authorService = authorService;
    }

    @Override
    public GenericResponse saveNews(@Description("New news") News news, @RequestParam(value = "tagId[]", required = false) Long[] tagIdArray, BindingResult result) throws ServiceException {
        validator.validate(news, result);
        GenericResponse response = new GenericResponse();
        if (!result.hasErrors()) {
            Long newsId = news.getId();
            Date today = new Date();
            news.setModificationDate(today);
            List<Tag> tagList = new ArrayList<Tag>();
            if (tagIdArray != null) {
                for (int i = 0; i < tagIdArray.length; i++) {
                    Tag tag = tagService.getSingleTagById(tagIdArray[i]);
                    tagList.add(tag);
                }
            }
            Set<Tag> tagSet = new HashSet<Tag>(tagList);
            news.setTagSet(tagSet);
            Long authorId = news.getAuthor().getAuthorId();
            news.setAuthor(authorService.getSingleAuthorById(authorId));
            if (newsId == null) {
                newsId = newsService.create(news);
            } else {
                newsService.update(news);
            }
            response.setMessage("Added");
            response.setSuccess(true);

        } else {
            response.setMessage("Not Added");
            response.setSuccess(false);
        }
        return response;
    }


}
