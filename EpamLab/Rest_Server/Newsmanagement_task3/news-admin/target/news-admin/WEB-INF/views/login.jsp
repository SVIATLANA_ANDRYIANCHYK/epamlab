<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<body>

	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="body">
			<spring:url var="authUrl" value="/static/j_spring_security_check" />
				<form method="POST" action="${authUrl}">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<div class="form">
						<div class="field">
							<label ><spring:message code="body.login" /></label>
							<input type="text" name="username" />
						</div>
						<div class="field">
							<label><spring:message
									code="body.password" /></label> 
							<input type="text" name="password" />
						</div>
						<button class="btn" type="submit">
							<spring:message code="body.button" />
						</button>
					</div>
				</form>
			
			
		</tiles:putAttribute>
	</tiles:insertDefinition>
</body>
</html>
