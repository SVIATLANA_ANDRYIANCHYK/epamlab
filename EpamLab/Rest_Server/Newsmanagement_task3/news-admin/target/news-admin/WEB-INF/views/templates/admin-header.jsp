<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="header-content">
	<div class="logout">
		<c:url value="/logout" var="logoutUrl" />
		<div class="hello"><spring:message code="header.hello"/>
			<c:out value = "${pageContext.request.userPrincipal.name}"/></div>
		<sf:form action="${logoutUrl}" method="post">
			<div class="logout-btn">
				<input type="submit" value="<spring:message code="header.logout" />" />
			</div>
		</sf:form>

	</div>
	<h2>
		<spring:message code="header.label" />
	</h2>
	<div class="languages">
		<a href="?lang=en"><spring:message code="header.language.en" /></a> |
		<a href="?lang=ru"><spring:message code="header.language.ru" /></a>
	</div>



</div>
