<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<p><img src = "<c:url value="/resources/images/arrow.png" />">  <a href="/news-admin/newsList"><spring:message code="menu.list"/></a></p>
<p><img src = "<c:url value="/resources/images/arrow.png" />">  <a href="/news-admin/addNews"><spring:message code="menu.addnews"/></a></p>
<p><img src = "<c:url value="/resources/images/arrow.png" />">  <a href="/news-admin/authors"><spring:message code="menu.authors"/></a></p>
<p><img src = "<c:url value="/resources/images/arrow.png" />">  <a href="/news-admin/tags"><spring:message code="menu.tags"/></a></p>
