<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<link href="<c:url value="/resources/css/home.css" />" rel="stylesheet">
</head>
<body>
	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="body">

			<div class="search-criteria">
				<form method = "POST" action = "/news-client/filter" class = "filter-form">
					<select name = "author">
						<option value=""><spring:message code="home.authors" /></option>
						<c:forEach items="${authors}" var="author">
							<option value="${author.authorId}">"${author.authorName}"</option>
						</c:forEach>
					</select> 
					<select onclick="showCheckboxes()">
						<option disabled><spring:message code="home.tags" /></option>
					</select>
					<div id="checkboxes" style="display: none">
	
						<c:forEach items="${tags}" var="tag">
						
							<div class="checkbox">
								<input type="checkbox" value="${tag.tagId}" name="tagId[]" />${tag.tagName}
							</div>
						</c:forEach>
					</div>
					<button type="submit"><spring:message code="home.filter" /></button>
				</form>
				<form method = "POST" action = "/news-client/reset" class = "reset">
					<button type="submit"><spring:message code="home.reset" /></button>
				</form>
			</div>
			<c:choose>
			<c:when test="${empty newsList}">
				<div class="message">
					<spring:message code="label.no.news" />
				</div>
			</c:when>
			<c:otherwise>
			<div class="news">
				<c:forEach items="${newsList}" var="newsList">
					<div class="single-news">
						<div class="title"><c:out value = "${newsList.title}"/></div>

						<div class="author"><c:out value = "(by ${newsList.author.authorName})"/></div>
						<fmt:message key="date.format" var="format" />
						<fmt:formatDate value="${newsList.modificationDate}"
							pattern="${format}" var="formattedDate" />

						<div class="date"><c:out value = "${formattedDate}"/></div>

						<div class="short-text"><c:out value = "${newsList.shortText}"/></div>

						<div class="right-block">
							<div class="tags">
								<c:forEach items="${newsList.tagSet}" var="tagSet">
									<c:out value = "${tagSet.tagName}"/>
								</c:forEach>
							</div>
							<div class="comments">

								<spring:message code="home.comments" />
								<c:out value = "(${fn:length(newsList.commentsList)})"/>
							</div>
							<div class="view">
								<a href="/news-client/view/${newsList.id}"><spring:message
										code="home.view" /></a>
							</div>

						</div>

					</div>
				</c:forEach>
				<input type="hidden" value="${currentPage}">

			</div>
			</c:otherwise>
			</c:choose>

			<div class="pages-panel">
				<div class="pages">
					<c:forEach items="${pages}" var="page">

						<form method="GET" action="/news-client/home">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
							<c:choose>
								<c:when test="${page == currentPage}">
									<div class="button">
										<button class="current-page" id="${page}" type="submit"
											name="page" value="${page}"><c:out value = "${page}"/></button>
									</div>
								</c:when>
								<c:otherwise>
									<div class="button">
										<button id="${page}" type="submit" name="page" value="${page}">${page}</button>
									</div>
								</c:otherwise>

							</c:choose>
						</form>

					</c:forEach>
				</div>
			</div>
		</tiles:putAttribute>
	</tiles:insertDefinition>
	<script type="text/javascript" src="<c:url value= "/resources/js/checkboxes.js"/>">
	
	</script>

</body>
</html>
