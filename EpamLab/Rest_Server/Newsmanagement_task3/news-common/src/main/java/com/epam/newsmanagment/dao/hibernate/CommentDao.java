package com.epam.newsmanagment.dao.hibernate;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.ICommentDao;
import com.epam.newsmanagment.entity.Comment;

@Repository("commentDao")
@Transactional
public class CommentDao implements ICommentDao {

	private static final String FROM_COMMENTS_STRING = "from Comment";

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Long create(Comment entity) throws DaoException {
		Session session = sessionFactory.getCurrentSession();
		Long id = (Long) session.save(entity);
		return id;
	}

	@Override
	public void update(Comment entity) throws DaoException {
		sessionFactory.getCurrentSession().merge(entity);
	}

	@Override
	public void delete(long id) throws DaoException {
		Comment comment = new Comment();
		comment.setCommentId(id);
		sessionFactory.getCurrentSession().delete(comment);

	}

	@Override
	public List<Comment> getAll() throws DaoException {
		@SuppressWarnings("unchecked")
		List<Comment> commentsList = sessionFactory.getCurrentSession().createQuery(FROM_COMMENTS_STRING).list();
		return commentsList;
	}

	@Override
	public List<Comment> getListOfCommentsByNewsId(long newsId) throws DaoException {
		String sql = "SELECT  * FROM Comments WHERE NEWS_ID = :newsId";
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql).addEntity(Comment.class);
		query.setParameter("newsId", newsId);
		@SuppressWarnings("unchecked")
		List<Comment> commentsList = query.list();
		return commentsList;
	}

	@Override
	public Comment getSingleCommentById(long commentId) throws DaoException {
		Comment comment = (Comment) sessionFactory.getCurrentSession().get(Comment.class, commentId);
		return comment;
	}

	@Override
	public void deleteCommentByNewsId(Long newsId) throws DaoException {
		// TODO Auto-generated method stub

	}

	// @Override
	// public void deleteCommentByNewsId(Long newsId) throws DaoException {
	// List<Comment> commentsList = findCommentsByNewsId(newsId);
	// for (Comment comment : commentsList) {
	// sessionFactory.getCurrentSession().delete(comment);
	// }
	//
	// }

	// @SuppressWarnings({ "unchecked" })
	// private List<Comment> findByProperty(String propertyName, Object value) {
	// String queryString = SELECT_WITH_PROPERTY_STRING + propertyName + "= ?";
	// Query queryObject = (Query)
	// sessionFactory.getCurrentSession().createQuery(queryString);
	// queryObject.setParameter(1, value);
	// return queryObject.getResultList();
	// }
	//
	// public List<Comment> findCommentsByNewsId(Object newsId) {
	// return findByProperty(NEWS_ID_STRING, newsId);
	// }
}
