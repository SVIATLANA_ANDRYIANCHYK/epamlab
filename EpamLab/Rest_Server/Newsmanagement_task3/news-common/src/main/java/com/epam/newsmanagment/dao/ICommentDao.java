package com.epam.newsmanagment.dao;

import java.util.List;

import com.epam.newsmanagment.entity.Comment;

public interface ICommentDao extends ICommonDao<Comment> {
	/**
	 * 
	 * @param newsId
	 *            to get
	 * @return List of Comment entities
	 * @throws DaoException
	 */
	public List<Comment> getListOfCommentsByNewsId(long newsId) throws DaoException;

	/**
	 * Gets Comment object by id.
	 * 
	 * @param commentId
	 * @return Comment object of certain id.
	 * @throws DaoException
	 */
	public Comment getSingleCommentById(long commentId) throws DaoException;

	public void deleteCommentByNewsId(Long newsId) throws DaoException;
}
