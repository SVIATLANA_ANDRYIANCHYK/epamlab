package com.epam.newsmanagment.util;

import com.epam.newsmanagment.entity.SearchCriteria;

/**
 * Forms SQL query according to parameters of search criteria parameters
 * 
 * @author Sviatlana_Andryianch
 *
 */
public class QueryBuilder {

	public String buildSearchCriteriaQuery(SearchCriteria searchCriteria) {

		StringBuilder query = new StringBuilder("SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM News ");
		if (searchCriteria != null) {
			// if only author set
			if ((searchCriteria.getAuthorId() != null) && ((searchCriteria.getTagList() == null))) {
				query.append("INNER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID = ");
				query.append(searchCriteria.getAuthorId());
				// if only list of tags set
			} else if ((searchCriteria.getAuthorId() == null) && (searchCriteria.getTagList() != null) && (!searchCriteria.getTagList().isEmpty())) {
				query.append("INNER JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID WHERE NEWS_TAG.TAG_ID IN (");
				for (int i = 0; i < searchCriteria.getTagList().size(); i++) {
					query.append(searchCriteria.getTagList().get(i));
					if (i != (searchCriteria.getTagList().size() - 1)) {
						query.append(", ");
					} else {
						query.append(")");
					}
				}

				// if both list of tags and author set
			} else if ((searchCriteria.getAuthorId() != null) && (searchCriteria.getTagList() != null)) {
				query.append("INNER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID ");
				if (!searchCriteria.getTagList().isEmpty()) {
					query.append("INNER JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID WHERE NEWS_TAG.TAG_ID IN (");
					for (int i = 0; i < searchCriteria.getTagList().size(); i++) {
						query.append(searchCriteria.getTagList().get(i));
						if (i != (searchCriteria.getTagList().size() - 1)) {
							query.append(", ");
						} else {
							query.append(") ");
						}
					}
				}
				query.append("AND NEWS_AUTHOR.AUTHOR_ID = ");
				query.append(searchCriteria.getAuthorId());
			}
		}

		return query.toString();
	}

	public String buildPagedNewsQuery(SearchCriteria sc, int begin, int end) {
		StringBuilder query = new StringBuilder(
				"SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM (SELECT A.NEWS_ID, A.TITLE, A.SHORT_TEXT, A.FULL_TEXT, A.CREATION_DATE, A.MODIFICATION_DATE, ROWNUM RNUM FROM (SELECT N.NEWS_ID, N.TITLE, N.SHORT_TEXT, N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE, COUNT(DISTINCT CO.COMMENT_ID) QTY FROM NEWS N LEFT JOIN COMMENTS CO ON N.NEWS_ID = CO.NEWS_ID LEFT JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID  LEFT JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID");
		if (sc != null) {
			// if only author set
			if ((sc.getAuthorId() != null) && ((sc.getTagList() == null || sc.getTagList().isEmpty()))) {
				query.append(" WHERE NA.AUTHOR_ID = ");
				query.append(sc.getAuthorId());
				// if only list of tags set
			} else if ((sc.getAuthorId() == null) && (sc.getTagList() != null) && (!sc.getTagList().isEmpty())) {
				query.append(" WHERE TAG_ID IN (");
				for (int i = 0; i < sc.getTagList().size(); i++) {
					query.append(sc.getTagList().get(i));
					if (i != (sc.getTagList().size() - 1)) {
						query.append(", ");
					} else {
						query.append(")");
					}
				}
				// if both list of tags and author set
			} else if ((sc.getAuthorId() != null) && (sc.getTagList() != null)) {
				query.append(" WHERE NA.AUTHOR_ID = ");
				query.append(sc.getAuthorId());
				query.append(" AND ");
				query.append("TAG_ID IN (");
				for (int i = 0; i < sc.getTagList().size(); i++) {
					query.append(sc.getTagList().get(i));
					if (i != (sc.getTagList().size() - 1)) {
						query.append(", ");
					} else {
						query.append(")");
					}
				}
			}

		}

		query.append(" GROUP BY N.NEWS_ID, N.TITLE, N.SHORT_TEXT, N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE ORDER BY QTY DESC, N.MODIFICATION_DATE DESC) A ) WHERE RNUM  >=");
		query.append(begin);
		query.append(" AND RNUM <=");
		query.append(end);

		return query.toString();
	}
}
