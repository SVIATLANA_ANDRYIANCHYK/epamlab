package com.epam.newsmanagment.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.INewsDao;
import com.epam.newsmanagment.dao.util.DaoUtil;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.util.QueryBuilder;
import com.epam.newsmanagment.util.TypeConverter;

public class NewsDao implements INewsDao {

	/**
	 * Data source object for taking the connection. Initialized in
	 * application-context.xml using setDataSource method
	 */
	private DataSource dataSource;

	/**
	 * SQL queries
	 */
	private static final String SELECT_ALL_NEWS_QUERY = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, COUNT(COMMENT_ID) AS COMMENT_COUNT FROM NEWS LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE ORDER BY MODIFICATION_DATE DESC, COMMENT_COUNT DESC";
	private static final String INSERT_NEWS_QUERY = "INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (NEWS_SEQ.nextval, ?, ?, ?, ?, ?)";
	private static final String DELETE_NEWS_QUERY = "DELETE FROM NEWS WHERE NEWS_ID = ?";
	private static final String SELECT_SINGLE_NEWS_QUERY = "SELECT n.NEWS_ID, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE FROM NEWS n WHERE n.NEWS_ID = ?";
	private static final String UPDATE_NEWS_QUERY = "UPDATE News SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";
	private static final String SELECT_NEWS_COUNT_QUERY = "SELECT COUNT(*) AS NEWS_COUNT FROM News";
	private static final String INSERT_NEWS_AUTHOR_QUERY = "INSERT INTO News_Author (NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
	private static final String DELETE_NEWS_AUTHOR_QUERY = "DELETE FROM News_Author WHERE NEWS_ID = ?";
	private static final String INSERT_NEWS_TAG_QUERY = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?, ?)";
	private static final String DELETE_NEWS_TAG_QUERY = "DELETE FROM NEWS_TAG WHERE TAG_ID = ?";
	private static final String SELECT_AUTHOR_ID_BY_NEWS_ID = "SELECT na.AUTHOR_ID FROM NEWS_AUTHOR na WHERE NEWS_ID = ?";
	private static final String SELECT_TAG_ID_BY_NEWS_ID = "SELECT tn.TAG_ID FROM NEWS_TAG tn WHERE NEWS_ID = ?";
	private static final String DELETE_NEWS_TAG_BY_NEWS_ID_QUERY = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";

	private static String newsId = "NEWS_ID";
	private static String newsTitle = "TITLE";
	private static String shortText = "SHORT_TEXT";
	private static String fullText = "FULL_TEXT";
	private static String creationDate = "CREATION_DATE";
	private static String modificationDate = "MODIFICATION_DATE";

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#getAll()
	 */
	public List<News> getAll() throws DaoException {

		News news = null; // Create object News
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		List<News> newsList = new ArrayList<News>();

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_ALL_NEWS_QUERY);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				news = new News();
				news.setId(resultSet.getLong(newsId));
				news.setTitle(resultSet.getString(newsTitle));
				news.setShortText(resultSet.getString(shortText));
				news.setFullText(resultSet.getString(fullText));
				news.setCreationDate(resultSet.getTimestamp(creationDate));
				news.setModificationDate(resultSet.getDate(modificationDate));
				newsList.add(news); // Add object News to list
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}

		return newsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#create(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public Long create(News entity) throws DaoException {
		Long id = null;

		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {

			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement((INSERT_NEWS_QUERY), new String[] { newsId });
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			Timestamp ts = new Timestamp(entity.getCreationDate().getTime()); // Type
																				// cast
			preparedStatement.setTimestamp(4, ts);
			TypeConverter converter = new TypeConverter();
			preparedStatement.setDate(5, converter.convertJavaDateToSqlDate(entity.getModificationDate()));
			preparedStatement.executeUpdate();

			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				id = generatedKeys.getLong(1);
			}

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}

		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#update(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public void update(News entity) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(UPDATE_NEWS_QUERY);

			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			Timestamp ts = new Timestamp(entity.getCreationDate().getTime()); // Type
																				// cast
			preparedStatement.setTimestamp(4, ts);
			TypeConverter converter = new TypeConverter();
			preparedStatement.setDate(5, converter.convertJavaDateToSqlDate(entity.getModificationDate()));
			preparedStatement.setLong(6, entity.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#delete(long)
	 */
	public void delete(long id) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_NEWS_QUERY);

			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#getSingleNewsById(long)
	 */
	@Override
	public News getSingleNewsById(long id) throws DaoException {
		News news = new News();
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_SINGLE_NEWS_QUERY);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				news.setId(resultSet.getLong(newsId));
				news.setTitle(resultSet.getString(newsTitle));
				news.setShortText(resultSet.getString(shortText));
				news.setFullText(resultSet.getString(fullText));

				news.setCreationDate(resultSet.getTimestamp(creationDate));

				news.setModificationDate(resultSet.getDate(modificationDate));
			}

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}
		return news;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.INewsDao#search(com.epam.newsmanagment
	 * .model.dao.entity.SearchCriteria)
	 */
	public List<News> search(SearchCriteria searchCriteria) throws DaoException {
		List<News> newsList = new ArrayList<>();
		QueryBuilder operator = new QueryBuilder();
		String query = operator.buildSearchCriteriaQuery(searchCriteria);

		News news = null; // Create object News
		ResultSet resultSet = null;
		Statement statement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				news = new News();
				news.setId(resultSet.getLong(newsId));
				news.setTitle(resultSet.getString(newsTitle));
				news.setShortText(resultSet.getString(shortText));
				news.setFullText(resultSet.getString(fullText));
				news.setCreationDate(resultSet.getTimestamp(creationDate));
				news.setModificationDate(resultSet.getDate(modificationDate));
				newsList.add(news); // Add object News to list
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, statement, resultSet);
		}

		return newsList;
	}

	public List<News> searchBySearchCriteriaAndPagination(SearchCriteria searchCriteria, int start, int end) throws DaoException {
		List<News> newsList = new ArrayList<>();
		QueryBuilder queryBuilder = new QueryBuilder();
		String query = queryBuilder.buildPagedNewsQuery(searchCriteria, start, end);
		News news = null; // Create object News
		ResultSet resultSet = null;
		Statement statement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				news = new News();
				news.setId(resultSet.getLong(newsId));
				news.setTitle(resultSet.getString(newsTitle));
				news.setShortText(resultSet.getString(shortText));
				news.setFullText(resultSet.getString(fullText));
				news.setCreationDate(resultSet.getTimestamp(creationDate));
				news.setModificationDate(resultSet.getDate(modificationDate));
				newsList.add(news); // Add object News to list
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, statement, resultSet);
		}
		return newsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#getNewsCount()
	 */
	public int getNewsCount() throws DaoException {
		int newsCount = 0;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_NEWS_COUNT_QUERY);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				newsCount = resultSet.getInt("NEWS_COUNT");
			}

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}

		return newsCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#addToNewsAuthor(long,
	 * long)
	 */
	public void linkAuthorNews(long newsId, long authorId) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(INSERT_NEWS_AUTHOR_QUERY);

			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#deleteFromNewsAuthor(long)
	 */
	public void unlinkAuthorNews(long newsId) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_NEWS_AUTHOR_QUERY);

			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#addToNewsTag(long,
	 * java.util.List)
	 */
	public void addTagToNews(long newsId, List<Tag> tagList) throws DaoException { // addTagToNews
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(INSERT_NEWS_TAG_QUERY);
			for (int i = 0; i < tagList.size(); i++) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagList.get(i).getTagId());
				preparedStatement.addBatch();
			}

			preparedStatement.executeBatch();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#deleteFromNewsTag(long)
	 */
	public void deleteTagFromNews(long tagId) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_NEWS_TAG_QUERY);

			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}
	}

	public void deleteTagFromNewsByNewsId(long newsId) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_NEWS_TAG_BY_NEWS_ID_QUERY);

			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#getAuthorIdByNewsId(long)
	 */
	public Long getAuthorIdByNewsId(long newsId) throws DaoException {
		Long authorId = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_AUTHOR_ID_BY_NEWS_ID);

			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				authorId = resultSet.getLong("AUTHOR_ID");
			}

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}
		return authorId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.INewsDao#getTagIdListByNewsId(long)
	 */
	public List<Long> getTagIdListByNewsId(long newsId) throws DaoException {
		long tagId = 0;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		List<Long> tagIdList = new ArrayList<>();
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_TAG_ID_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tagId = resultSet.getLong("TAG_ID");
				tagIdList.add(tagId);
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}
		return tagIdList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.INewsDao#getListOfTagsByNewsId(long)
	 */
	public List<Tag> getListOfTagsByNewsId(long newsId) throws DaoException {
		List<Long> tagIdList = getTagIdListByNewsId(newsId);
		List<Tag> tagList = new ArrayList<>();
		ApplicationContext ctx = new ClassPathXmlApplicationContext("application-context.xml");
		TagDao tagDao = (TagDao) ctx.getBean("tagDao");
		tagDao.setDataSource(dataSource);
		for (int i = 0; i < tagIdList.size(); i++) {
			tagList.add(tagDao.getSingleTagById(tagIdList.get(i)));
		}
		((ClassPathXmlApplicationContext) ctx).close();
		return tagList;
	}

	@Override
	public Long getNewsCountBySearchCriteria(SearchCriteria searchCriteria) throws DaoException {
		throw new UnsupportedOperationException();

	}

}
