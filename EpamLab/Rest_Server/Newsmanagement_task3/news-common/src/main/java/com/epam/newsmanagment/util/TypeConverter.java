package com.epam.newsmanagment.util;

public class TypeConverter {
	/**
	 * Converts util.Date object to sql.Date.
	 * 
	 * @param date
	 *            util.Date object
	 * @return sql.Date object
	 */
	public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}
}
