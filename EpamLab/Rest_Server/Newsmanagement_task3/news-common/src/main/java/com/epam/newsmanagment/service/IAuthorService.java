package com.epam.newsmanagment.service;

import com.epam.newsmanagment.entity.Author;

public interface IAuthorService extends IService<Author> {
	/**
	 * 
	 * @param id
	 *            of author to get
	 * @return Author entity
	 * @throws ServiceException
	 */
	public Author getSingleAuthorById(long id) throws ServiceException;

}
