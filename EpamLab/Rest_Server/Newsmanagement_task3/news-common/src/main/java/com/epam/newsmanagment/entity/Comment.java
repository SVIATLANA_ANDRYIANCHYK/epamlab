package com.epam.newsmanagment.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "comments")
@JsonIgnoreProperties
@XmlRootElement
public class Comment implements Serializable, IEntity {

	/**
	 * For deserialization with no exception after modification.
	 */
	private static final long serialVersionUID = 3431305873409011465L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "comment_id", precision = 0)
	private Long commentId; // Primary key

	// @Column(name = "NEWS_ID")
	// private Long newsId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "news_id")
	@com.fasterxml.jackson.annotation.JsonBackReference
	private News news;

	@NotEmpty
	@NotNull
	@Column(name = "comment_text")
	private String commentText;

	@Temporal(TemporalType.DATE)
	@Column(name = "creation_date")
	private Date creationDate;

	/**
	 * Default constructor
	 */
	public Comment() {
		super();
	}

	/**
	 * Parameterized constructor
	 *
	 * @param commentId
	 * @param newsId
	 * @param commentText
	 * @param creationDate
	 */
	public Comment(Long commentId, Long newsId, String commentText, Date creationDate) {
		super();
		this.commentId = commentId;
		this.news.setId(newsId);
		this.commentText = commentText;
		this.creationDate = creationDate;
	}

	/**
	 * @return the comment Id
	 */
	public Long getCommentId() {
		return commentId;
	}

	/**
	 * @param commentId
	 *            the commentId to set
	 */
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	/**
	 * @return the news Id
	 */
	public Long getNewsId() {
		return news.getId();
	}

	/**
	 * @param newsId
	 *            the newsId to set
	 */
	public void setNewsId(Long newsId) {
		this.news.setId(newsId);
	}

	/**
	 * @return the commentText
	 */
	public String getCommentText() {
		return commentText;
	}

	/**
	 * @param commentText
	 *            the commentText to set
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the news
	 */
	public News getNews() {
		return news;
	}

	/**
	 * @param news
	 *            the news to set
	 */
	public void setNews(News news) {
		this.news = news;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
		result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentId == null) {
			if (other.commentId != null)
				return false;
		} else if (!commentId.equals(other.commentId))
			return false;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		return true;
	}

}

// package com.epam.newsmanagment.entity;
//
// import java.io.Serializable;
// import java.util.Date;
//
// import javax.persistence.Column;
// import javax.persistence.Entity;
// import javax.persistence.GeneratedValue;
// import javax.persistence.GenerationType;
// import javax.persistence.Id;
// import javax.persistence.SequenceGenerator;
// import javax.persistence.Table;
// import javax.persistence.Temporal;
// import javax.persistence.TemporalType;
//
// @Entity
// @Table(name = "COMMENTS")
// public class Comment implements Serializable, IEntity {
//
// private static final long serialVersionUID = 1L;
//
// @Id
// @Column(name = "COMMENT_ID")
// @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
// "COMMENTS_SEQ")
// @SequenceGenerator(name = "COMMENTS_SEQ", sequenceName = "COMMENTS_SEQ",
// allocationSize = 1)
// private Long commentId;
//
// @Column(name = "NEWS_ID")
// private Long newsId;
//
// @Column(name = "COMMENT_TEXT", length = 100)
// private String commentText;
//
// @Column(name = "CREATION_DATE")
// @Temporal(TemporalType.DATE)
// private Date creationDate;
//
// public Comment() {
// }
//
// public Long getCommentId() {
// return commentId;
// }
//
// public void setCommentId(Long commentId) {
// this.commentId = commentId;
// }
//
// /**
// * @return the newsId
// */
// public Long getNewsId() {
// return newsId;
// }
//
// /**
// * @param newsId
// * the newsId to set
// */
// public void setNewsId(Long newsId) {
// this.newsId = newsId;
// }
//
// /*
// * (non-Javadoc)
// *
// * @see java.lang.Object#hashCode()
// */
// @Override
// public int hashCode() {
// final int prime = 31;
// int result = 1;
// result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
// result = prime * result + ((commentText == null) ? 0 :
// commentText.hashCode());
// result = prime * result + ((creationDate == null) ? 0 :
// creationDate.hashCode());
// result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
// return result;
// }
//
// /*
// * (non-Javadoc)
// *
// * @see java.lang.Object#equals(java.lang.Object)
// */
// @Override
// public boolean equals(Object obj) {
// if (this == obj)
// return true;
// if (obj == null)
// return false;
// if (getClass() != obj.getClass())
// return false;
// Comment other = (Comment) obj;
// if (commentId == null) {
// if (other.commentId != null)
// return false;
// } else if (!commentId.equals(other.commentId))
// return false;
// if (commentText == null) {
// if (other.commentText != null)
// return false;
// } else if (!commentText.equals(other.commentText))
// return false;
// if (creationDate == null) {
// if (other.creationDate != null)
// return false;
// } else if (!creationDate.equals(other.creationDate))
// return false;
// if (newsId == null) {
// if (other.newsId != null)
// return false;
// } else if (!newsId.equals(other.newsId))
// return false;
// return true;
// }
//
// public String getCommentText() {
// return commentText;
// }
//
// public void setCommentText(String commentText) {
// this.commentText = commentText;
// }
//
// public Date getCreationDate() {
// return creationDate;
// }
//
// public void setCreationDate(Date creationDate) {
// this.creationDate = creationDate;
// }
//
// @Override
// public String toString() {
// return "Comment [commentId=" + commentId + ", commentText=" + commentText +
// ", creationDate=" + creationDate + "]";
// }
//
// }

