package com.epam.newsmanagment.dao.eclipselink;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.INewsDao;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;

@Transactional
@Repository
public class NewsDao implements INewsDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long create(News entity) throws DaoException {
		entityManager.persist(entity);
		entityManager.flush();
		return entity.getNewsId();
	}

	@Override
	public void update(News entity) throws DaoException {
		entityManager.merge(entity);
	}

	@Override
	public void delete(long id) throws DaoException {
		News news = entityManager.find(News.class, id);
		entityManager.remove(news);

	}

	@Override
	public List<News> getAll() throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public News getSingleNewsById(long id) throws DaoException {
		return entityManager.find(News.class, id);
	}

	@Override
	public List<News> search(SearchCriteria searchCriteria) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNewsCount() throws DaoException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void linkAuthorNews(long newsId, long authorId) throws DaoException {
		// TODO Auto-generated method stub

	}

	@Override
	public void unlinkAuthorNews(long newsId) throws DaoException {
		// TODO Auto-generated method stub

	}

	@Override
	public void addTagToNews(long newsId, List<Tag> tagList) throws DaoException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteTagFromNews(long newsId) throws DaoException {
		// TODO Auto-generated method stub

	}

	@Override
	public Long getAuthorIdByNewsId(long newsId) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Long> getTagIdListByNewsId(long newsId) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Tag> getListOfTagsByNewsId(long newsId) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<News> searchBySearchCriteriaAndPagination(SearchCriteria searchCriteria, int pageNumber, int pageSize) throws DaoException {
		Query query = buildSearchCriteria(searchCriteria);
		query.setFirstResult((pageNumber - 1) * pageSize);
		query.setMaxResults(pageSize);
		@SuppressWarnings("unchecked")
		List<News> newsList = query.getResultList();

		return newsList;
	}

	@Override
	public void deleteTagFromNewsByNewsId(long newsId) throws DaoException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Long getNewsCountBySearchCriteria(SearchCriteria searchCriteria) throws DaoException {
		@SuppressWarnings("unchecked")
		List<News> newsList = buildSearchCriteria(searchCriteria).getResultList();
		int newsCount = newsList.size();
		return (long) newsCount;
	}

	private Query buildSearchCriteria(SearchCriteria searchCriteria) {
		StringBuilder queryStringBuilder = new StringBuilder("select distinct n from News n left join n.commentsList as c left join n.tagSet as nt left join n.author as a ");

		if (searchCriteria != null) {

			// if only author set
			if ((searchCriteria.getAuthorId() != null) && ((searchCriteria.getTagList() == null || searchCriteria.getTagList().isEmpty()))) {
				// if only tags set
				queryStringBuilder.append("where a.authorId = ");
				Long authorId = searchCriteria.getAuthorId();
				queryStringBuilder.append(authorId);

			} else if ((searchCriteria.getAuthorId() == null) && (searchCriteria.getTagList() != null) && (!searchCriteria.getTagList().isEmpty())) {
				// if both tags and author set
				queryStringBuilder.append("where nt.tagId in (");
				List<Long> tagIdList = searchCriteria.getTagList();
				for (int i = 0; i < tagIdList.size(); i++) {
					queryStringBuilder.append(tagIdList.get(i));
					if (i != (tagIdList.size() - 1)) {
						queryStringBuilder.append(", ");
					} else {
						queryStringBuilder.append(")");
					}
				}

			} else if ((searchCriteria.getAuthorId() != null) && (searchCriteria.getTagList() != null)) {
				queryStringBuilder.append("where a.authorId = ");
				Long authorId = searchCriteria.getAuthorId();
				queryStringBuilder.append(authorId);
				queryStringBuilder.append(" and ");
				queryStringBuilder.append("nt.tagId in (");
				List<Long> tagIdList = searchCriteria.getTagList();
				for (int i = 0; i < tagIdList.size(); i++) {
					queryStringBuilder.append(tagIdList.get(i));
					if (i != (tagIdList.size() - 1)) {
						queryStringBuilder.append(", ");
					} else {
						queryStringBuilder.append(")");
					}
				}
			}
		}
		queryStringBuilder.append(" order by n.modificationDate desc");
		String queryString = queryStringBuilder.toString();
		Query query = entityManager.createQuery(queryString);
		return query;
	}

}
