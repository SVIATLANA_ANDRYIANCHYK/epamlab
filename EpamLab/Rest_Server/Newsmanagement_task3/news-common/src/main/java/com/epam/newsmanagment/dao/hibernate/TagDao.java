package com.epam.newsmanagment.dao.hibernate;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.ITagDao;
import com.epam.newsmanagment.entity.Tag;

@Repository("tagDao")
@Transactional
public class TagDao implements ITagDao {

	private static final String FROM_TAGS_STRING = "from Tag";

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Long create(Tag entity) throws DaoException {
		Session session = sessionFactory.getCurrentSession();
		Long id = (Long) session.save(entity);
		return id;

	}

	@Override
	public void update(Tag entity) throws DaoException {
		sessionFactory.getCurrentSession().merge(entity);

	}

	@Override
	public void delete(long id) throws DaoException {
		Tag tag = new Tag();
		tag.setTagId(id);
		sessionFactory.getCurrentSession().delete(tag);

	}

	@Override
	public List<Tag> getAll() throws DaoException {
		@SuppressWarnings("unchecked")
		List<Tag> tagList = sessionFactory.getCurrentSession().createQuery(FROM_TAGS_STRING).list();
		return tagList;
	}

	@Override
	public Tag getSingleTagById(long tagId) throws DaoException {
		Tag tag = (Tag) sessionFactory.getCurrentSession().get(Tag.class, tagId);
		return tag;
	}

	@Override
	public void setDataSource(DataSource dataSource) {

	}

}
