package com.epam.newsmanagment.dao.eclipselink;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.ITagDao;
import com.epam.newsmanagment.entity.Tag;

@Transactional
@Repository
public class TagDao implements ITagDao {

	private static final String SELECT_FROM_TAG_QUERY = "select T from Tag T ";

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long create(Tag entity) throws DaoException {
		entityManager.persist(entity);
		entityManager.flush();
		return entity.getTagId();
	}

	@Override
	public void update(Tag entity) throws DaoException {
		entityManager.merge(entity);
	}

	@Override
	public void delete(long id) throws DaoException {
		Tag tag = entityManager.find(Tag.class, id);
		entityManager.remove(tag);

	}

	@Override
	public List<Tag> getAll() throws DaoException {
		return entityManager.createQuery(SELECT_FROM_TAG_QUERY, Tag.class).getResultList();
	}

	@Override
	public Tag getSingleTagById(long tagId) throws DaoException {
		return entityManager.find(Tag.class, tagId);
	}

	@Override
	public void setDataSource(DataSource dataSource) {
		// TODO Auto-generated method stub

	}

}
