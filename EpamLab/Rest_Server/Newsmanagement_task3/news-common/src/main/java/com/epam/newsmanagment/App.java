package com.epam.newsmanagment;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ServiceException;



/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) throws ParseException, ServiceException, DaoException {


		ApplicationContext ctx = new ClassPathXmlApplicationContext("application-context.xml");
		INewsService ser = (INewsService) ctx.getBean("newsService");
		SearchCriteria sc = new SearchCriteria();

		sc.setAuthorId(12345679L);
		List<Long> list = new ArrayList<Long>();
		list.add(24L);
		sc.setTagList(list);
		System.out.println(ser.searchBySearchCriteriaAndPagination(sc, 1, 2));

	}
}
