import com.epam.newsmanagement.controller.impl.AuthorControllerImpl;
import com.epam.newsmanagment.service.ServiceException;

/**
 * Created by Sviatlana_Andryianch on 2/5/2016.
 */
public class Test {

    public static void main(String[] args) throws ServiceException {
        AuthorControllerImpl au = new AuthorControllerImpl();
        System.out.println(au.getAllAuthors());
    }
}
