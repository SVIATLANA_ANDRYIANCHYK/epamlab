package com.epam.newsmanagement.controller.impl;

import com.epam.newsmanagement.controller.NewsController;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ServiceException;
import com.epam.newsmanagment.service.implementation.NewsService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Sviatlana_Andryianch on 2/15/2016.
 */
public class NewsControllerImpl implements NewsController {

    @Autowired
    private INewsService newsService;
    @Override
    public List<News> getAllNews(int page, SearchCriteria searchCriteria) throws ServiceException {
        List<News> newsList = newsService.searchBySearchCriteriaAndPagination(searchCriteria, page, 3);
        return newsList;
    }
}
