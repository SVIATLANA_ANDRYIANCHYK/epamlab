package com.epam.newsmanagement.controller.impl;

import com.epam.newsmanagement.controller.TagController;
import com.epam.newsmanagement.util.GenericResponse;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jws.WebService;
import java.util.List;

/**
 * Created by Sviatlana_Andryianch on 2/1/2016.
 */
@WebService(endpointInterface = "com.epam.newsmanagement.controller.TagController",
        serviceName="tagService")
public class TagControllerImpl implements TagController {

    @Autowired
    private ITagService tagService;


    @Override
    public GenericResponse insertTag(Tag tag) throws ServiceException {
        GenericResponse response = new GenericResponse();
        Long id = tagService.create(tag);
        if(id != null){
            response.setMessage("Tag with id = " + id + " inserted");
            response.setSuccess(true);
        }
        else{
            response.setMessage("Error while creating tag");
            response.setSuccess(false);
        }
        return response;
    }

    @Override
    public List<Tag> getAllTags() throws ServiceException {
        List<Tag> tags = tagService.getAll();
        return tags;
    }



}