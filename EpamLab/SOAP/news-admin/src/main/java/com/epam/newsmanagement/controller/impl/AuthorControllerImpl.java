package com.epam.newsmanagement.controller.impl;

import com.epam.newsmanagement.util.GenericResponse;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.ServiceException;
import com.epam.newsmanagement.controller.AuthorController;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import javax.jws.WebService;
import java.util.List;

/**
 * Created by Sviatlana_Andryianch on 2/1/2016.
 */
@WebService(endpointInterface = "com.epam.newsmanagement.controller.AuthorController",
        serviceName="authorService")
public class AuthorControllerImpl implements AuthorController {

    @Autowired
    private IAuthorService authorService;

    @Override
    public List<Author> getAllAuthors() throws ServiceException {
        List<Author> authorList = authorService.getAll();
        return authorList;
    }

    @Override
    public GenericResponse insertAuthor(Author author) throws ServiceException {
        GenericResponse response = new GenericResponse();

        Long id = authorService.create(author);
        if(id != null) {
            response.setMessage("Author with id = " + id + " created");
            response.setSuccess(true);
        } else{
            response.setMessage("Error while creating author");
            response.setSuccess(false);
        }
        return response;
    }
}
