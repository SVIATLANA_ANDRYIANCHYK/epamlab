package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.util.GenericResponse;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.service.ServiceException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

/**
 * Created by Sviatlana_Andryianch on 2/1/2016.
 */
@WebService
public interface AuthorController {

    @WebMethod
    public List<Author> getAllAuthors() throws ServiceException;

    @WebMethod
    public GenericResponse insertAuthor(Author author) throws ServiceException;


}
