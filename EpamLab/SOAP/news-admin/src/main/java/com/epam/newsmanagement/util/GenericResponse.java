package com.epam.newsmanagement.util;

/**
 * Created by Sviatlana_Andryianch on 2/5/2016.
 */
public class GenericResponse {
    private boolean success;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
