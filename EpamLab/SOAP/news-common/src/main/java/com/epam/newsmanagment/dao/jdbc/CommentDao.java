package com.epam.newsmanagment.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.ICommentDao;
import com.epam.newsmanagment.dao.util.DaoUtil;
import com.epam.newsmanagment.entity.Comment;

public class CommentDao implements ICommentDao {

	private static final String INSERT_COMMENT_QUERY = "INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES (COMMENTS_SEQ.nextval, ?, ?, ?)";
	private static final String UPDATE_COMMENT_QUERY = "UPDATE Comments SET NEWS_ID = ?, COMMENT_TEXT = ?, CREATION_DATE = ? WHERE COMMENT_ID = ?";
	private static final String SELECT_ALL_COMMENTS_QUERY = "SELECT c.COMMENT_ID, c.NEWS_ID, c.COMMENT_TEXT, c.CREATION_DATE FROM COMMENTS c";
	private static final String DELETE_COMMENT_QUERY = "DELETE FROM Comments WHERE COMMENT_ID = ?";
	private static final String SELECT_COMMENTS_OF_CERTAIN_NEWS_QUERY = "SELECT c.COMMENT_ID, c.COMMENT_TEXT, c.CREATION_DATE FROM COMMENTS c WHERE c.NEWS_ID = ?";
	private static final String SELECT_SINGLE_COMMENT_QUERY = "SELECT c.NEWS_ID, c.COMMENT_TEXT, c.CREATION_DATE FROM COMMENTS c WHERE c.COMMENT_ID = ?";
	private static final String DELETE_COMMENT_BY_NEWSID_QUERY = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";

	private static String commentId = "COMMENT_ID";
	private static String newsId = "NEWS_ID";
	private static String commentText = "COMMENT_TEXT";
	private static String creationDate = "CREATION_DATE";
	private DataSource dataSource;

	/**
	 * @param dataSource
	 *            the dataSource to set
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#create(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public Long create(Comment entity) throws DaoException {
		Long id = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement((INSERT_COMMENT_QUERY), new String[] { commentId });
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getCommentText());
			Timestamp ts = new Timestamp(entity.getCreationDate().getTime());
			preparedStatement.setTimestamp(3, ts);
			preparedStatement.executeUpdate();

			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				id = generatedKeys.getLong(1);
			}

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}

		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.IDao#update(com.epam.newsmanagment.model
	 * .dao.entity.IEntity)
	 */
	public void update(Comment entity) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(UPDATE_COMMENT_QUERY);
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getCommentText());
			Timestamp ts = new Timestamp(entity.getCreationDate().getTime());
			preparedStatement.setTimestamp(3, ts);
			preparedStatement.setLong(4, entity.getCommentId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#getAll()
	 */
	public List<Comment> getAll() throws DaoException {
		Comment comment = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		List<Comment> commentList = new ArrayList<>();
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_ALL_COMMENTS_QUERY);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				comment = new Comment();
				comment.setCommentId(resultSet.getLong(commentId));
				comment.setNewsId(resultSet.getLong(newsId));
				comment.setCommentText(resultSet.getString(commentText));
				comment.setCreationDate(resultSet.getTimestamp(creationDate));

				commentList.add(comment); // Add object Comment to list
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}

		return commentList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.dao.IDao#delete(long)
	 */
	@Override
	public void delete(long id) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_COMMENT_QUERY);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.ICommentDao#getListOfCommentsByNewsId
	 * (long)
	 */
	@Override
	public List<Comment> getListOfCommentsByNewsId(long newsId) throws DaoException {

		Comment comment = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		List<Comment> commentList = new ArrayList<>();
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_COMMENTS_OF_CERTAIN_NEWS_QUERY);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				comment = new Comment();
				comment.setCommentId(resultSet.getLong(commentId));
				comment.setNewsId(newsId);
				comment.setCommentText(resultSet.getString(commentText));
				comment.setCreationDate(resultSet.getTimestamp(creationDate));

				commentList.add(comment); // Add object Comment to list
			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}

		return commentList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.dao.ICommentDao#getSingleCommentById(long)
	 */
	public Comment getSingleCommentById(long commentId) throws DaoException {
		Comment comment = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_SINGLE_COMMENT_QUERY);
			preparedStatement.setLong(1, commentId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				comment = new Comment();
				comment.setCommentId(commentId);
				comment.setNewsId(resultSet.getLong(newsId));
				comment.setCommentText(resultSet.getString(commentText));
				comment.setCreationDate(resultSet.getTimestamp(creationDate));

			}
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, resultSet);
		}

		return comment;
	}

	@Override
	public void deleteCommentByNewsId(Long newsId) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_COMMENT_BY_NEWSID_QUERY);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		} finally {
			DaoUtil.closeDaoResources(dataSource, connection, preparedStatement, null);
		}

	}
}
