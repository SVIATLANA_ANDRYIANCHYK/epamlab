package com.epam.newsmanagment.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.INewsDao;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;

@Repository("newsDao")
@Transactional
public class NewsDao implements INewsDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Long create(News entity) throws DaoException {
		Session session = sessionFactory.getCurrentSession();
		Long id = (Long) session.save(entity);
		return id;
	}

	@Override
	public void update(News entity) throws DaoException {
		sessionFactory.getCurrentSession().merge(entity);

	}

	@Override
	public void delete(long id) throws DaoException {
		News news = new News();
		news.setId(id);
		sessionFactory.getCurrentSession().delete(news);

	}

	@Override
	public List<News> getAll() throws DaoException {
		throw new UnsupportedOperationException();
	}

	@Override
	public News getSingleNewsById(long id) throws DaoException {
		News news = (News) sessionFactory.getCurrentSession().get(News.class, id);
		return news;
	}

	@Override
	public List<News> search(SearchCriteria searchCriteria) throws DaoException {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getNewsCount() throws DaoException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void linkAuthorNews(long newsId, long authorId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void unlinkAuthorNews(long newsId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void addTagToNews(long newsId, List<Tag> tagList) throws DaoException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void deleteTagFromNews(long newsId) throws DaoException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Long getAuthorIdByNewsId(long newsId) throws DaoException {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Long> getTagIdListByNewsId(long newsId) throws DaoException {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Tag> getListOfTagsByNewsId(long newsId) throws DaoException {
		throw new UnsupportedOperationException();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> searchBySearchCriteriaAndPagination(SearchCriteria searchCriteria, int pageNumber, int pageSize) throws DaoException {
		Query query = buildSearchCriteria(searchCriteria);
		query.setFirstResult((pageNumber - 1) * pageSize);
		query.setMaxResults(pageSize);
		List<News> newsList = query.list();

		return newsList;
	}

	@Override
	public void deleteTagFromNewsByNewsId(long newsId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	private Query buildSearchCriteria(SearchCriteria searchCriteria) {
		StringBuilder queryStringBuilder = new StringBuilder("select n from News n left join n.commentsList as c left join n.tagSet as nt left join n.author as a ");

		if (searchCriteria != null) {

			// if only author set
			if ((searchCriteria.getAuthorId() != null) && ((searchCriteria.getTagList() == null || searchCriteria.getTagList().isEmpty()))) {
				// if only tags set
				queryStringBuilder.append("where a.authorId = ");
				Long authorId = searchCriteria.getAuthorId();
				queryStringBuilder.append(authorId);

			} else if ((searchCriteria.getAuthorId() == null) && (searchCriteria.getTagList() != null) && (!searchCriteria.getTagList().isEmpty())) {
				// if both tags and author set
				queryStringBuilder.append("where nt.tagId in (");
				List<Long> tagIdList = searchCriteria.getTagList();
				for (int i = 0; i < tagIdList.size(); i++) {
					queryStringBuilder.append(tagIdList.get(i));
					if (i != (tagIdList.size() - 1)) {
						queryStringBuilder.append(", ");
					} else {
						queryStringBuilder.append(")");
					}
				}

			} else if ((searchCriteria.getAuthorId() != null) && (searchCriteria.getTagList() != null)) {
				queryStringBuilder.append("where a.authorId = ");
				Long authorId = searchCriteria.getAuthorId();
				queryStringBuilder.append(authorId);
				queryStringBuilder.append(" and ");
				queryStringBuilder.append("nt.tagId in (");
				List<Long> tagIdList = searchCriteria.getTagList();
				for (int i = 0; i < tagIdList.size(); i++) {
					queryStringBuilder.append(tagIdList.get(i));
					if (i != (tagIdList.size() - 1)) {
						queryStringBuilder.append(", ");
					} else {
						queryStringBuilder.append(")");
					}
				}
			}
		}
		queryStringBuilder.append(" group by n.id, n.title, n.shortText, n.fullText, n.creationDate, n.modificationDate, n.author.authorId order by n.modificationDate desc, count(c.news) desc");
		String queryString = queryStringBuilder.toString();
		Query query = sessionFactory.getCurrentSession().createQuery(queryString);
		return query;
	}

	@Override
	public Long getNewsCountBySearchCriteria(SearchCriteria searchCriteria) throws DaoException {
		@SuppressWarnings("unchecked")
		List<News> newsList = buildSearchCriteria(searchCriteria).list();
		int newsCount = newsList.size();
		return (long) newsCount;
	}
}
