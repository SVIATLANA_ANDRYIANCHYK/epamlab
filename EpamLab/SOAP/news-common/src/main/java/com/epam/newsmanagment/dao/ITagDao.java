package com.epam.newsmanagment.dao;

import javax.sql.DataSource;

import com.epam.newsmanagment.entity.Tag;

public interface ITagDao extends ICommonDao<Tag> {
	/**
	 * Gets Tag object by certain tag id.
	 * 
	 * @param tagId
	 * @return Tag object
	 * @throws DaoException
	 */
	public Tag getSingleTagById(long tagId) throws DaoException;

	public void setDataSource(DataSource dataSource);
}
