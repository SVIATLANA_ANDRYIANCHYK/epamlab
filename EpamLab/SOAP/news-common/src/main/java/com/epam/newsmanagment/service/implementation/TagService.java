package com.epam.newsmanagment.service.implementation;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.ITagDao;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.ServiceException;

@Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
public class TagService implements ITagService {

	@Autowired
	@Qualifier("tagDao")
	private ITagDao tagDao;

	private static final Logger LOG = LoggerFactory.getLogger(TagService.class);

	/**
	 * @return the tagDao
	 */
	public ITagDao getTagDao() {
		return tagDao;
	}

	/**
	 * @param tagDao
	 *            the tagDao to set
	 */
	@Autowired
	public void setTagDao(ITagDao tagDao) {
		this.tagDao = tagDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#create(java.lang.Object)
	 */
	@Override
	public Long create(Tag entity) throws ServiceException {
		try {
			return tagDao.create(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(Tag entity) throws ServiceException {
		try {
			tagDao.update(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#delete(long)
	 */
	@Override
	public void delete(long id) throws ServiceException {
		try {
			tagDao.delete(id);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#getAll()
	 */
	@Override
	public List<Tag> getAll() throws ServiceException {
		try {
			return tagDao.getAll();
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.ITagService#getSingleTagById(long)
	 */
	@Override
	public Tag getSingleTagById(long tagId) throws ServiceException {
		try {
			return tagDao.getSingleTagById(tagId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

}
