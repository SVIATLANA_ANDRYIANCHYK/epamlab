package com.epam.newsmanagment.service.implementation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.ICommentDao;
import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.service.ICommentService;
import com.epam.newsmanagment.service.ServiceException;

@Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
public class CommentService implements ICommentService {

	@Autowired
	@Qualifier("commentDao")
	private ICommentDao commentDao;
	private static final Logger LOG = LoggerFactory.getLogger(CommentService.class);

	/**
	 * @return the commentDao
	 */
	public ICommentDao getCommentDao() {
		return commentDao;
	}

	/**
	 * @param commentDao
	 *            the commentDao to set
	 */
	public void setCommentDao(ICommentDao commentDao) {
		this.commentDao = commentDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#create(java.lang.Object)
	 */
	@Override
	public Long create(Comment entity) throws ServiceException {
		try {
			return commentDao.create(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(Comment entity) throws ServiceException {
		try {
			commentDao.update(entity);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#delete(long)
	 */
	@Override
	public void delete(long id) throws ServiceException {
		try {
			commentDao.delete(id);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.IService#getAll()
	 */
	@Override
	public List<Comment> getAll() throws ServiceException {
		try {
			return commentDao.getAll();
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagment.model.service.ICommentService#
	 * getListOfCommentsByNewsId(long)
	 */
	@Override
	public List<Comment> getListOfCommentsByNewsId(long newsId) throws ServiceException {
		try {
			return commentDao.getListOfCommentsByNewsId(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagment.model.service.ICommentService#getSingleCommentById
	 * (long)
	 */
	@Override
	public Comment getSingleCommentById(long commentId) throws ServiceException {
		try {
			return commentDao.getSingleCommentById(commentId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

	@Override
	public void deleteCommentByNewsId(Long newsId) throws ServiceException {

		try {
			commentDao.deleteCommentByNewsId(newsId);
		} catch (DaoException e) {
			if (LOG.isDebugEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new ServiceException(e.getMessage(), e);
		}
	}

}
