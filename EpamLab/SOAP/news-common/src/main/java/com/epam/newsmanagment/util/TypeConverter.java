package com.epam.newsmanagment.util;

public class TypeConverter {
	/**
	 * Converts com.epam.newsmanagement.util.Date object to sql.Date.
	 * 
	 * @param date
	 *            com.epam.newsmanagement.util.Date object
	 * @return sql.Date object
	 */
	public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}
}
