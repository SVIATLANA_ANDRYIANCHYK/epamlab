package com.epam.newsmanagment.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.dao.IAuthorDao;
import com.epam.newsmanagment.entity.Author;

@Repository("authorDao")
@Transactional
public class AuthorDao implements IAuthorDao {

	private static final String FROM_AUTHOR_STRING = "from Author where expired is null";

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Long create(Author entity) throws DaoException {
		Session session = sessionFactory.getCurrentSession();
		Long id = (Long) session.save(entity);
		return id;

	}

	@Override
	public void update(Author entity) throws DaoException {
		sessionFactory.getCurrentSession().merge(entity);
	}

	@Override
	public void delete(long id) throws DaoException {
		setAuthorExpired(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> getAll() throws DaoException {
		Session session = sessionFactory.getCurrentSession();
		List<Author> authors = session.createQuery(FROM_AUTHOR_STRING).list();
		return authors;
	}

	@Override
	public Author getSingleAuthorById(long id) throws DaoException {

		Author author = (Author) sessionFactory.getCurrentSession().get(Author.class, id);
		return author;
	}

	private void setAuthorExpired(Long id) {
		Author author = (Author) sessionFactory.getCurrentSession().get(Author.class, id);
		author.setExpired(new Date());
		sessionFactory.getCurrentSession().merge(author);

	}
}
