package com.epam.newsmanagment.service;

import java.util.List;

import com.epam.newsmanagment.dao.DaoException;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;

public interface INewsService extends IService<News> {
	/**
	 * 
	 * @param id
	 *            to get
	 * @return News entity
	 * @throws DaoException
	 */
	public News getSingleNewsById(long id) throws ServiceException;

	/**
	 * 
	 * @param searchCriteria
	 *            for searching news in database
	 * @return List of all news appropriate to search criteria
	 * @throws ServiceException
	 */
	public List<News> search(SearchCriteria searchCriteria) throws ServiceException;

	/**
	 * 
	 * @return the count of news in the database.
	 * @throws ServiceException
	 */
	public int getNewsCount() throws ServiceException;

	/**
	 * Adds entity to the linking table News_Author
	 * 
	 * @param newsId
	 *            to add
	 * @param authorId
	 *            to add
	 * @throws ServiceException
	 */
	public void addToNewsAuthor(long newsId, long authorId) throws ServiceException;

	/**
	 * Deletes entity from linking table News_Author
	 * 
	 * @param newsId
	 *            to delete
	 * @throws ServiceException
	 */
	public void deleteFromNewsAuthor(long newsId) throws ServiceException;

	/**
	 * Adds entity to linking table News_Tag
	 * 
	 * @param newsId
	 *            to add
	 * @param tagList
	 *            to add
	 * @throws ServiceException
	 */
	public void addToNewsTag(long newsId, List<Tag> tagList) throws ServiceException;

	/**
	 * Deletes entity from linking table News_Tag
	 * 
	 * @param newsId
	 *            to delete
	 * @throws ServiceException
	 */
	public void deleteFromNewsTag(long newsId) throws ServiceException;

	/**
	 * Gets author id by news id.
	 * 
	 * @param newsId
	 *            to set
	 * @return id of author
	 * @throws ServiceException
	 */
	public Long getAuthorIdByNewsId(long newsId) throws ServiceException;

	/**
	 * 
	 * @param newsId
	 *            to set
	 * @return list of id of tags
	 * @throws DaoException
	 */
	public List<Long> getTagIdListByNewsId(long newsId) throws ServiceException;

	/**
	 * Gets list of tags by certain news id.
	 * 
	 * @param newsId
	 *            to set
	 * @return list of tags
	 * @throws ServiceException
	 */
	public List<Tag> getListOfTagsByNewsId(long newsId) throws ServiceException;

	public void deleteTagFromNewsByNewsId(long newsId) throws ServiceException;

	public List<News> searchBySearchCriteriaAndPagination(SearchCriteria searchCriteria, int start, int end) throws ServiceException;

	public Long getNewsCountBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;
}
