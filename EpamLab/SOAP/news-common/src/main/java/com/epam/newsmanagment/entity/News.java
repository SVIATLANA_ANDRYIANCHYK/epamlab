package com.epam.newsmanagment.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * News entity
 *
 * @author Sviatlana_Andryianch
 *
 */

@Entity
@Table(name = "News")
@JsonIgnoreProperties
@XmlRootElement
public final class News implements Serializable, IEntity {

	/**
	 * For deserialization with no exception after modification.
	 */
	private static final long serialVersionUID = 3773281197317274020L;

	@Id
	@SequenceGenerator(name = "NEWS_SEQ_GEN", sequenceName = "NEWS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEWS_SEQ_GEN")
	@Column(name = "news_id", precision = 0)
	private Long newsId; // Primary key

	@Column(name = "title")
	private String title;

	@Column(name = "short_text")
	private String shortText;

	@Column(name = "full_text")
	private String fullText;

	@Temporal(TemporalType.DATE)
	@Column(name = "creation_date")
	private Date creationDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "modification_date")
	private Date modificationDate;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "news")
	@JsonManagedReference
	private List<Comment> commentsList;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "news_tag", joinColumns = { @JoinColumn(name = "news_id") }, inverseJoinColumns = { @JoinColumn(name = "tag_id") })
	@JsonManagedReference
	private Set<Tag> tagSet;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinTable(name = "news_author", joinColumns = { @JoinColumn(name = "news_id") }, inverseJoinColumns = { @JoinColumn(name = "author_id") })
	@JsonManagedReference
	private Author author;

	/**
	 * @return the author
	 */
	public Author getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(Author author) {
		this.author = author;
	}

	/**
	 * Default constructor
	 */
	public News() {
	}

	/**
	 * Parameterized constructor
	 * @param title
	 * @param shortText
	 * @param fullText
	 * @param creationDate
	 * @param modificationDate
	 */
	public News(String title, String shortText, String fullText, Date creationDate, Date modificationDate) {
		super();

		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	/**
	 * @param newsId
	 * @param title
	 * @param shortText
	 * @param fullText
	 * @param creationDate
	 * @param modificationDate
	 */
	public News(Long newsId, String title, String shortText, String fullText, Date creationDate, Date modificationDate) {
		super();
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return newsId;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.newsId = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the shortText
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * @param shortText
	 *            the shortText to set
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * @return the fullText
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * @param fullText
	 *            the fullText to set
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the newsId
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * @param newsId
	 *            the newsId to set
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * @return the commentsList
	 */

	/**
	 * @return the tagSet
	 */
	public Set<Tag> getTagSet() {
		return tagSet;
	}

	/**
	 * @return the commentsList
	 */
	public List<Comment> getCommentsList() {
		return commentsList;
	}

	/**
	 * @param commentsList
	 *            the commentsList to set
	 */
	public void setCommentsList(List<Comment> commentsList) {
		this.commentsList = commentsList;
	}

	/**
	 * @param tagSet
	 *            the tagSet to set
	 */
	public void setTagSet(Set<Tag> tagSet) {
		this.tagSet = tagSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		// result = prime * result + ((commentsList == null) ? 0 :
		// commentsList.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((tagSet == null) ? 0 : tagSet.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", title=" + title + ", shortText=" + shortText + ", fullText=" + fullText + ", creationDate=" + creationDate + ", modificationDate=" + modificationDate
		/* + ", commentsList=" *//* + commentsList * /*", tagSet=" + tagSet + ", author=" + author + "]"*/;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}

// package com.epam.newsmanagment.entity;
//
// import java.io.Serializable;
// import java.com.epam.newsmanagement.util.Date;
// import java.com.epam.newsmanagement.util.List;
// import java.com.epam.newsmanagement.util.Set;
//
// import javax.persistence.Column;
// import javax.persistence.Entity;
// import javax.persistence.FetchType;
// import javax.persistence.GeneratedValue;
// import javax.persistence.GenerationType;
// import javax.persistence.Id;
// import javax.persistence.JoinColumn;
// import javax.persistence.JoinTable;
// import javax.persistence.ManyToMany;
// import javax.persistence.ManyToOne;
// import javax.persistence.OneToMany;
// import javax.persistence.SequenceGenerator;
// import javax.persistence.Table;
// import javax.persistence.Temporal;
// import javax.persistence.TemporalType;
//
// /**
// * News entity
// *
// * @author Sviatlana_Andryianch
// *
// */
//
// @Entity
// @Table(name = "News")
// public final class News implements Serializable, IEntity {
//
// /**
// * For deserialization with no exception after modification.
// */
// private static final long serialVersionUID = 3773281197317274020L;
//
// @Id
// @SequenceGenerator(name = "NEWS_SEQ_GEN", sequenceName = "NEWS_SEQ")
// @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
// "NEWS_SEQ_GEN")
// @Column(name = "NEWS_ID", precision = 0)
// private Long newsId; // Primary key
//
// @Column(name = "TITLE")
// private String title;
//
// @Column(name = "SHORT_TEXT")
// private String shortText;
//
// @Column(name = "FULL_TEXT")
// private String fullText;
//
// @Temporal(TemporalType.DATE)
// @Column(name = "CREATION_DATE")
// private Date creationDate;
//
// @Temporal(TemporalType.DATE)
// @Column(name = "MODIFICATION_DATE")
// private Date modificationDate;
//
// @OneToMany(fetch = FetchType.EAGER)
// private List<Comment> commentsList;
//
// @ManyToMany(fetch = FetchType.EAGER)
// @JoinTable(name = "NEWS_TAG", joinColumns = { @JoinColumn(name = "NEWS_ID")
// }, inverseJoinColumns = { @JoinColumn(name = "TAG_ID") })
// private Set<Tag> tagSet;
//
// @ManyToOne(fetch = FetchType.EAGER)
// @JoinTable(name = "NEWS_AUTHOR", joinColumns = { @JoinColumn(name =
// "NEWS_ID") }, inverseJoinColumns = { @JoinColumn(name = "AUTHOR_ID") })
// private Author author;
//
// /**
// * @return the author
// */
// public Author getAuthor() {
// return author;
// }
//
// /**
// * @param author
// * the author to set
// */
// public void setAuthor(Author author) {
// this.author = author;
// }
//
// /**
// * Default constructor
// */
// public News() {
// }
//
// /**
// * Parameterized constructor
// *
// * @param id
// * @param title
// * @param shortText
// * @param fullText
// * @param creationDate
// * @param modificationDate
// */
// public News(String title, String shortText, String fullText, Date
// creationDate, Date modificationDate) {
// super();
//
// this.title = title;
// this.shortText = shortText;
// this.fullText = fullText;
// this.creationDate = creationDate;
// this.modificationDate = modificationDate;
// }
//
// /**
// * @param newsId
// * @param title
// * @param shortText
// * @param fullText
// * @param creationDate
// * @param modificationDate
// */
// public News(Long newsId, String title, String shortText, String fullText,
// Date creationDate, Date modificationDate) {
// super();
// this.newsId = newsId;
// this.title = title;
// this.shortText = shortText;
// this.fullText = fullText;
// this.creationDate = creationDate;
// this.modificationDate = modificationDate;
// }
//
// /**
// * @return the id
// */
// public Long getId() {
// return newsId;
// }
//
// /**
// * @param id
// * the id to set
// */
// public void setId(Long id) {
// this.newsId = id;
// }
//
// /**
// * @return the title
// */
// public String getTitle() {
// return title;
// }
//
// /**
// * @param title
// * the title to set
// */
// public void setTitle(String title) {
// this.title = title;
// }
//
// /**
// * @return the shortText
// */
// public String getShortText() {
// return shortText;
// }
//
// /**
// * @param shortText
// * the shortText to set
// */
// public void setShortText(String shortText) {
// this.shortText = shortText;
// }
//
// /**
// * @return the fullText
// */
// public String getFullText() {
// return fullText;
// }
//
// /**
// * @param fullText
// * the fullText to set
// */
// public void setFullText(String fullText) {
// this.fullText = fullText;
// }
//
// /**
// * @return the creationDate
// */
// public Date getCreationDate() {
// return creationDate;
// }
//
// /**
// * @param creationDate
// * the creationDate to set
// */
// public void setCreationDate(Date creationDate) {
// this.creationDate = creationDate;
// }
//
// /**
// * @return the modificationDate
// */
// public Date getModificationDate() {
// return modificationDate;
// }
//
// /**
// * @param modificationDate
// * the modificationDate to set
// */
// public void setModificationDate(Date modificationDate) {
// this.modificationDate = modificationDate;
// }
//
// /**
// * @return the newsId
// */
// public Long getNewsId() {
// return newsId;
// }
//
// /**
// * @param newsId
// * the newsId to set
// */
// public void setNewsId(Long newsId) {
// this.newsId = newsId;
// }
//
// /**
// * @return the commentsList
// */
//
// /**
// * @return the tagSet
// */
// public Set<Tag> getTagSet() {
// return tagSet;
// }
//
// /**
// * @return the commentsList
// */
// public List<Comment> getCommentsList() {
// return commentsList;
// }
//
// /**
// * @param commentsList
// * the commentsList to set
// */
// public void setCommentsList(List<Comment> commentsList) {
// this.commentsList = commentsList;
// }
//
// /**
// * @param tagSet
// * the tagSet to set
// */
// public void setTagSet(Set<Tag> tagSet) {
// this.tagSet = tagSet;
// }
//
// /*
// * (non-Javadoc)
// *
// * @see java.lang.Object#hashCode()
// */
//
// /*
// * (non-Javadoc)
// *
// * @see java.lang.Object#hashCode()
// */
// @Override
// public int hashCode() {
// final int prime = 31;
// int result = 1;
// result = prime * result + ((commentsList == null) ? 0 :
// commentsList.hashCode());
// result = prime * result + ((creationDate == null) ? 0 :
// creationDate.hashCode());
// result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
// result = prime * result + ((modificationDate == null) ? 0 :
// modificationDate.hashCode());
// result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
// result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
// result = prime * result + ((tagSet == null) ? 0 : tagSet.hashCode());
// result = prime * result + ((title == null) ? 0 : title.hashCode());
// return result;
// }
//
// /*
// * (non-Javadoc)
// *
// * @see java.lang.Object#toString()
// */
// @Override
// public String toString() {
// return "News [newsId=" + newsId + ", title=" + title + ", shortText=" +
// shortText + ", fullText=" + fullText + ", creationDate=" + creationDate +
// ", modificationDate=" + modificationDate
// + ", commentsList=" + commentsList + ", tagSet=" + tagSet + ", author=" +
// author + "]";
// }
//
// /*
// * (non-Javadoc)
// *
// * @see java.lang.Object#equals(java.lang.Object)
// */
// @Override
// public boolean equals(Object obj) {
// if (this == obj)
// return true;
// if (obj == null)
// return false;
// if (getClass() != obj.getClass())
// return false;
// News other = (News) obj;
// if (commentsList == null) {
// if (other.commentsList != null)
// return false;
// } else if (!commentsList.equals(other.commentsList))
// return false;
// if (creationDate == null) {
// if (other.creationDate != null)
// return false;
// } else if (!creationDate.equals(other.creationDate))
// return false;
// if (fullText == null) {
// if (other.fullText != null)
// return false;
// } else if (!fullText.equals(other.fullText))
// return false;
// if (modificationDate == null) {
// if (other.modificationDate != null)
// return false;
// } else if (!modificationDate.equals(other.modificationDate))
// return false;
// if (newsId == null) {
// if (other.newsId != null)
// return false;
// } else if (!newsId.equals(other.newsId))
// return false;
// if (shortText == null) {
// if (other.shortText != null)
// return false;
// } else if (!shortText.equals(other.shortText))
// return false;
// if (tagSet == null) {
// if (other.tagSet != null)
// return false;
// } else if (!tagSet.equals(other.tagSet))
// return false;
// if (title == null) {
// if (other.title != null)
// return false;
// } else if (!title.equals(other.title))
// return false;
// return true;
// }
//
// }
