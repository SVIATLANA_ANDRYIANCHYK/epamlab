package com.epam.newsmanagment.service;

import java.util.List;

public interface IService<T> {
	/**
	 * Adds entity to the database.
	 * 
	 * @param entity
	 *            entity for adding to a database.
	 */
	public Long create(T entity) throws ServiceException;

	/**
	 * 
	 * @param entity
	 *            adding to db
	 * @return true if operation ended successfully, false - if not
	 * @throws ServiceException
	 */
	public void update(T entity) throws ServiceException;

	/**
	 * 
	 * @param deliting
	 *            entity
	 * @return true if operation ended successfully, false - if not
	 * @throws ServiceException
	 */
	public void delete(long id) throws ServiceException;

	/**
	 * Shows all items in the database
	 * 
	 * @return list of items
	 * @throws ServiceException
	 */
	List<T> getAll() throws ServiceException;
}
