package controller;

import java.util.Date;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.service.ICommentService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ServiceException;

@Controller
public class SingleNewsController {

	private ICommentService commentService;
	private INewsService newsService;

	@Inject
	public SingleNewsController(INewsService newsService, ICommentService commentService) {
		this.newsService = newsService;
		this.commentService = commentService;

	}

	@RequestMapping(value = { "/view/{newsId}" }, method = RequestMethod.GET)
	public String viewSingleNews(@PathVariable("newsId") Long newsId, Model model, HttpSession session) throws ServiceException {
		News news = newsService.getSingleNewsById(newsId);
		news.setCommentsList(commentService.getListOfCommentsByNewsId(newsId));
		model.addAttribute("news", news);

		return "single-news";
	}

	@RequestMapping(value = { "/postComment" }, method = RequestMethod.POST)
	public String postComment(@RequestParam(value = "newsId") Long newsId, @RequestParam(value = "commentText") String commentText) throws ServiceException {
		String text = commentText.replaceAll("\\s+", "");
		if (text.length() > 1 && commentText != null && commentText.length() < 100) {
			Comment comment = new Comment();
			Date today = new Date();
			comment.setCreationDate(today);
			comment.setCommentText(commentText);
			News news = newsService.getSingleNewsById(newsId);
			comment.setNewsId(newsId);
			commentService.create(comment);

		}
		return "redirect:/view/" + newsId;
	}

	@RequestMapping(value = { "/next" }, method = RequestMethod.GET)
	public String showNextNews(Long newsId, HttpSession session) throws ServiceException {
		// Navigator navigator = new Navigator();
		// SearchCriteria sc = (SearchCriteria)
		// session.getAttribute("searchCtiteria");
		// int newsCount = newsService.getNewsCount();
		// NewsVO nextNews = navigator.getNextNewsVO(serviceManager, sc, newsId,
		// newsCount);
		// Long nextNewsId = nextNews.getNews().getId();
		return "redirect:/view/"/* + nextNewsId; */;
	}

	@RequestMapping(value = { "/previous" }, method = RequestMethod.GET)
	public String showPreviousNews(Long newsId, HttpSession session) throws ServiceException {
		// Navigator navigator = new Navigator();
		// SearchCriteria sc = (SearchCriteria)
		// session.getAttribute("searchCtiteria");
		// int newsCount = newsService.getNewsCount();
		// NewsVO previousNews = navigator.getPreviousNewsVO(serviceManager, sc,
		// newsId, newsCount);
		// Long previousNewsId = previousNews.getNews().getId();
		return "redirect:/view/"/* + previousNewsId */;
	}
}
