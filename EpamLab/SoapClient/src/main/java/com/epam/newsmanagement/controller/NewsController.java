package com.epam.newsmanagement.controller;



import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.ServiceException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

/**
 * Created by Sviatlana_Andryianch on 2/15/2016.
 */
@WebService
public interface NewsController {

    @WebMethod
    public List<News> getAllNews(int page, SearchCriteria searchCriteria) throws ServiceException;

}
