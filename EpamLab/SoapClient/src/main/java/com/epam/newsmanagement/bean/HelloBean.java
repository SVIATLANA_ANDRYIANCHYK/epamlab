package com.epam.newsmanagement.bean;

import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

/**
 * Created by Sviatlana_Andryianch on 2/8/2016.
 */
@Component
@ManagedBean(name = "helloBean")
@SessionScoped
public class HelloBean implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;

    public String getName() {
        return "hi";
    }
    public void setName(String name) {
        this.name = name;
    }
}
