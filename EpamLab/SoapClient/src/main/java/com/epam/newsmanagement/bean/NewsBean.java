package com.epam.newsmanagement.bean;

import com.epam.newsmanagement.controller.NewsController;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.ServiceException;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

/**
 * Created by Sviatlana_Andryianch on 2/9/2016.
 */
@ManagedBean(name="newsBean")
@SessionScoped
public class NewsBean {

    private Long selectedAuthorId;
    private List<Long> selectedTagIdList;
    private List<News> newsList;
    private NewsController newsController;
    private int page;
    private final static Logger LOG = Logger.getLogger(AuthorBean.class);


    @PostConstruct
    public void init() {
        String serviceUrl = "http://localhost:8099/news-admin/newsService"; /*loadServiceUrl();*/
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(NewsController.class);
        factory.setAddress(serviceUrl);
        newsController = (NewsController) factory.create();
        SearchCriteria searchCriteria = new SearchCriteria(/*selectedAuthorId, selectedTagIdList*/);
//        try {
//            newsList = newsController.getAllNews(1, null);
//        } catch (ServiceException e) {
//            LOG.error(e.getMessage());
//        }
    }


    public Long getSelectedAuthorId() {
        return selectedAuthorId;
    }

    public void setSelectedAuthorId(Long selectedAuthorId) {
        this.selectedAuthorId = selectedAuthorId;
    }

    public List<Long> getSelectedTagIdList() {
        return selectedTagIdList;
    }

    public void setSelectedTagIdList(List<Long> selectedTagIdList) {
        this.selectedTagIdList = selectedTagIdList;
    }

    public List<News> getNewsList() {
        return newsList;
    }

    public void setNewsList(List<News> newsList) {
        this.newsList = newsList;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
