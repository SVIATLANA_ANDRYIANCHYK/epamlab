package com.epam.newsmanagement.bean;

import com.epam.newsmanagement.controller.TagController;
import com.epam.newsmanagement.entity.ServiceException;
import com.epam.newsmanagement.entity.Tag;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

/**
 * Created by Sviatlana_Andryianch on 2/11/2016.
 */
@ManagedBean(name = "tagBean")
@SessionScoped
public class TagBean {

    private TagController tagController;

    private List<Tag> tagList;
    private final static Logger LOG = Logger.getLogger(TagBean.class);

    @PostConstruct
    public void init() {
        String serviceUrl = "http://localhost:8099/news-admin/tagservice"; /*loadServiceUrl();*/
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(TagController.class);
        factory.setAddress(serviceUrl);
        tagController = (TagController) factory.create();
        try {
            tagList = tagController.getAllTags();
        } catch (ServiceException e) {
            LOG.error(e.getMessage());
        }
    }


    public TagController getTagController() {
        return tagController;
    }

    public void setTagController(TagController tagController) {
        this.tagController = tagController;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }
}
