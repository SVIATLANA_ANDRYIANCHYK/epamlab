package com.epam.newsmanagement.bean;

import com.epam.newsmanagement.controller.AuthorController;
import com.epam.newsmanagement.controller.TagController;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.ServiceException;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

/**
 * Created by Sviatlana_Andryianch on 2/9/2016.
 */
@ManagedBean(name = "authorBean")
@SessionScoped
public class AuthorBean {

    private AuthorController authorController;
    private List<Author> authorList;
    private String newAuthorName;
    private final static Logger LOG = Logger.getLogger(AuthorBean.class);

    @PostConstruct
    public void init(){
        String serviceUrl = "http://localhost:8099/news-admin/authorService"; /*loadServiceUrl();*/
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(AuthorController.class);
        factory.setAddress(serviceUrl);
        authorController = (AuthorController) factory.create();
        try {
            authorList = authorController.getAllAuthors();
        } catch (ServiceException e) {
            LOG.error(e.getMessage());
        }
    }


    public List<Author> getAuthors() throws ServiceException {
        List<Author> authors = authorController.getAllAuthors();
        return authors;
    }

    public void save(){
        Author author = new Author();
        author.setAuthorName(newAuthorName);
        try {
            authorController.insertAuthor(author);
            newAuthorName = null;
        } catch (ServiceException e) {
            LOG.error(e.getMessage());
        }
    }

    private String loadServiceUrl() {
        Properties prop = new Properties();
        InputStream input = null;
        String serviceUrl = null;
        try {
            input = new FileInputStream("url.properties");
            // load a properties file
            prop.load(input);
            // get the property value and print it out
            serviceUrl = prop.getProperty("author-service");
        } catch (IOException ex) {
            LOG.error(ex.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    LOG.error(e.getMessage());
                }
            }
        }
        return serviceUrl;
    }


    public List<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    public String getNewAuthorName() {
        return newAuthorName;
    }

    public void setNewAuthorName(String newAuthorName) {
        this.newAuthorName = newAuthorName;
    }
}
