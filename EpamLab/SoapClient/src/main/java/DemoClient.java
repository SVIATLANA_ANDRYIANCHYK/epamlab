import com.epam.newsmanagement.controller.AuthorController;
import com.epam.newsmanagement.controller.TagController;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.ServiceException;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.util.GenericResponse;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import java.util.List;

/**
 * Created by Sviatlana_Andryianch on 1/29/2016.
 */
public class DemoClient {
    public static void main(String[] args) throws ServiceException {
        String serviceUrl = "http://localhost:8099/news-admin/tagservice";
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(TagController.class);
        factory.setAddress(serviceUrl);
        TagController tagController = (TagController)factory.create();
        System.out.println(tagController.getAllTags());
        Tag tag = new Tag(1000L, "Test");
        GenericResponse response = tagController.insertTag(tag);
        System.out.println(response.getMessage());
//        AuthorController authorController = (AuthorController) factory.create();
//
//
//        Author author = new Author(66L, "Test", null);
//        GenericResponse response = authorController.insertAuthor(author);
//        System.out.println(response.getMessage());
//        List<Author> authors = authorController.getAllAuthors();
//        System.out.println(authors);


    }
}
